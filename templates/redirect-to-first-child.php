<?php

/**
 * Template Name: Redirect to first child
 */

$pages = get_pages([
    'child_of' => stratum()->page->id(),
    'sort_column' => 'menu_order',
]);

if (count($pages) > 0) {
    wp_redirect(get_permalink($pages[0]));
    exit;
}

wp_redirect(get_home_url());
exit;
