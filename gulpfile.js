import gulp from 'gulp';
import browsersync from './_src/build/browserSync.js';
import buildTask from './_src/build/build.js';
import buildDevTask from './_src/build/build-dev.js';
import watchTask from './_src/build/watch.js';
import fix from './_src/build/fix.js';
import lint from './_src/build/lint.js';

const watch = gulp.series(buildDevTask, browsersync, watchTask);
const build = buildTask;

// Find out what task was called
const taskName = process.argv[2];

// If it was watch, lets load the environment variables
if (taskName === 'watch') {
    let env = await import('./.env.js');

    for (const [key, value] of Object.entries(env.default)) {
        process.env[key] = value;
    }
}

export {
    watch,
    fix,
    lint
}

export default build;
