<?php

get_header();

echo \Stratum\Template::render();

$content = '';

if (stratum()->page->injectHeading()) {
    $content .= \Stratum\Component::render('wysiwyg', [
        'heading' => stratum()->page->title(),
    ]);
}

if (!stratum()->page->blocks()->hasQueryLoop()) {
    $content .= \Stratum\Component::render('card-grid', [
        'items' => stratum()->query()->posts,
    ]);

    // -------------------------------------------------------------------------
    // Render the primary content into a main
    // -------------------------------------------------------------------------
    $content = \Stratum\Component::render('main', $content);

    // -----------------------------------------------------------------------------
    // Add content after the main
    // -----------------------------------------------------------------------------
    // This can be removed when we have the search-and-filter pagination sorted
    $content .= \Stratum\Component::render('pagination/archive');
}

// -----------------------------------------------------------------------------
// Output everything
// -----------------------------------------------------------------------------
echo $content;

get_footer();
