<?php

get_header();

$content = '';

if (stratum()->page->injectHeading()) {
    $content .= \Stratum\Component::render('wysiwyg', [
        'heading' => stratum()->page->title(),
        'classes' => ['has-text-align-center'],
    ]);
}

$content .= stratum()->page->content();

echo \Stratum\Component::render('main', [
    'content' => $content,
]);

get_footer();
