<?php

namespace Stratum\Plugin;

class ACF
{
    public static function init(): void
    {
        add_action('acf/init', [__CLASS__, 'google']);
        add_action('acf/init', [__CLASS__, 'optionPages']);
        add_action('acf/init', [__CLASS__, 'disableShortcode']);
        add_filter('acf/load_field/name=background_color', [__CLASS__, 'loadBackgroundColors']);
        add_filter('acf/load_field/name=background_colour', [__CLASS__, 'loadBackgroundColors']);
        add_filter('acf/load_field/name=social_network', [__CLASS__, 'loadSocialNetworks']);
        add_filter('acf/load_field/name=icon', [__CLASS__, 'loadIcons']);
        add_filter('acf/load_field/type=select', [__CLASS__, 'fancyUI']);
        add_filter('acf/load_field/type=true_false', [__CLASS__, 'fancyUI']);
        add_filter('acf/blocks/wrap_frontend_innerblocks', '__return_false');
    }

    public static function disableShortcode(): void
    {
        acf_update_setting('enable_shortcode', false);
    }

    public static function fancyUI($field): array
    {
        $field['ui'] = true;
        return $field;
    }

    public static function loadSocialNetworks($field): array
    {
        // Unify the presentation of the background color selector
        $field['type'] = 'select';

        // Empty out the choices if there are any
        $field['choices'] = [];

        $images = \Stratum\Asset::files('images/networks/*.svg');

        if (!empty($images)) {
            foreach ($images as $image) {
                $filename = pathinfo($image, PATHINFO_FILENAME);
                $field['choices'][$filename] = str_replace('-', ' ', ucfirst($filename));
            }
        }

        return $field;
    }

    public static function loadIcons($field): array
    {
        // Unify the presentation of the background color selector
        $field['type'] = 'select';

        // Empty out the choices if there are any
        $field['choices'] = [];

        $images = \Stratum\Asset::files('images/icons/*.svg');

        if (!empty($images)) {
            foreach ($images as $image) {
                $filename = pathinfo($image, PATHINFO_FILENAME);
                $field['choices'][$filename] = str_replace('-', ' ', ucfirst($filename));
            }
        }

        return $field;
    }

    public static function loadBackgroundColors($field): array
    {
        // Unify the presentation of the background color selector
        $field['type'] = 'button_group';

        // Empty out the choices if there are any
        $field['choices'] = [
            'default' => __('Default', 'stratum-text-domain')
        ];

        $colors = \Stratum\Colors::secondary();

        if (!empty($colors)) {
            foreach ($colors as $color) {
                $field['choices'][$color['background']] = ucfirst($color['background']);
            }
        }

        return $field;
    }

    public static function google(): void
    {
        $key = \Stratum\Setting::get('google-maps-key', false);

        // Register a google maps api key
        if (!empty($key)) {
            acf_update_setting('google_api_key', $key);
        }
    }

    /**
     * Provide ACF with a Google maps API key if it has been configured
     * @return void
     */
    public static function optionPages(): void
    {
        $globalPages = \Stratum\Setting::get('acf/global-option-pages', []);
        $postPages = \Stratum\Setting::get('acf/post-option-pages', []);

        // Global Option pages
        if (!empty($globalPages)) {
            acf_add_options_page();

            foreach ($globalPages as $page) {
                acf_add_options_sub_page($page);
            }
        }

        // Post type option pages
        if (!empty($postPages)) {
            foreach ($postPages as $postTypeSlug) {
                $postType = get_post_type_object($postTypeSlug);

                acf_add_options_sub_page([
                    'title' => sprintf(__("%s Options", 'stratum-text-domain'), $postType->label),
                    'parent' => $postTypeSlug !== 'post'
                        ? 'edit.php?post_type=' . $postTypeSlug
                        : 'edit.php',
                    'capability' => 'manage_options'
                ]);
            }
        }
    }
}
