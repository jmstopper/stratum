<?php

namespace Stratum\Plugin;

class YoastSEO
{
    public static function init(): void
    {
        add_filter('wpseo_metabox_prio', [__CLASS__, 'wpseoMetaboxPriority']);
        add_filter('wpseo_debug_markers', '__return_false');
        add_action('wp_dashboard_setup', [__CLASS__, 'removeDashboardWidget']);
        add_action('wp_before_admin_bar_render', [__CLASS__, 'disableMenuBar']);
    }

    public static function disableMenuBar(): void
    {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('wpseo-menu');
    }

    public static function removeDashboardWidget(): void
    {
        // In some cases, you may need to replace 'side' with 'normal' or 'advanced'.
        remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'side');
    }

    public static function wpseoMetaboxPriority(string $priority): string
    {
        return 'low';
    }

    public static function breadcrumbsAsArray(): array
    {
        if (!function_exists('yoast_breadcrumb')) {
            return [];
        }

        global $wp;
        $crumbs = [];
        $dom = new \DOMDocument();
        // @phpstan-ignore-next-line
        $dom->loadHTML(\yoast_breadcrumb('', '', false));
        $items = $dom->getElementsByTagName('a');

        foreach ($items as $tag) {
            $crumbs[] =  [
                'title' => $tag->nodeValue,
                'url' => $tag->getAttribute('href'),
            ];
        }

        //Get the current page text and href
        $items = new \DOMXpath($dom);
        $dom = $items->query('//*[contains(@class, "breadcrumb_last")]');

        $crumbs[] = [
            'title' => $dom->item(0)->nodeValue,
            'url' => trailingslashit(home_url($wp->request)),
            'wrapper' => [
                'aria-current' => 'page',
            ]
        ];

        return $crumbs;
    }
}
