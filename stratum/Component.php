<?php

namespace Stratum;

class Component
{
    use \Stratum\ComponentRegisterFieldsTrait;

    public static function render(string $component, $args = [], $options = [], $adjunct = []): string
    {
        return \Stratum\Render::render('assets/components/' . $component, $args, $options, $adjunct);
    }

    public static function name(string $component): string
    {
        $json = self::json($component);
        return $json->title;
    }

    public static function shouldLoadComponentByTemplate(string $component): bool
    {
        $json = self::json($component);

        if (isset($json->stratum->templateDependency) && !empty($json->stratum->templateDependency)) {
            $templates = $json->stratum->templateDependency;

            foreach ($templates as $template) {
                $parts = explode('/', $template);

                if (isset($parts[2]) && $parts[0] === 'post') {
                    if ($parts[1] === 'single' && is_singular($parts[2])) {
                        return true;
                    } elseif ($parts[1] === 'archive') {
                        if ($parts[2] === 'post' && is_home()) {
                            return true;
                        } elseif (is_post_type_archive($parts[2])) {
                            return true;
                        }
                    }
                } elseif (isset($parts[1]) && $parts[0] === 'tax') {
                    if ($parts[1] === 'category' && is_category()) {
                        return true;
                    } elseif ($parts[1] === 'post_tag' && is_tag()) {
                        return true;
                    } elseif (is_tax($parts[1])) {
                        return true;
                    }
                } elseif (isset($parts[1]) && $parts[0] === 'template') {
                    global $post;

                    $templateParts = $parts;
                    unset($templateParts[0]);

                    if (implode('/', $templateParts) === get_page_template_slug($post)) {
                        return true;
                    }
                } elseif ($parts[0] === 'search' && is_search()) {
                    return true;
                } elseif ($parts[0] === 'author' && is_author()) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function loadTemplateDependencies(string $component, string $type): void
    {
        if (self::shouldLoadComponentByTemplate($component)) {
            $method = 'load' . ucFirst($type);
            self::$method($component);
        }
    }

    public static function loadAssets(string $component): void
    {
        self::loadStyles($component);
        self::loadScripts($component);
    }

    public static function loadStyles(string $component): void
    {
        $json = self::json($component);

        if (!empty($json->stratum->componentDependencies)) {
            foreach ($json->stratum->componentDependencies as $dependency) {
                self::loadStyles($dependency);
            }
        }

        if ($url = self::css($component)) {
            wp_enqueue_style('stratum-component-' . $component, $url, [], null);
        }
    }

    public static function loadScripts(string $component): void
    {
        $json = self::json($component);

        if (!empty($json->stratum->componentDependencies)) {
            foreach ($json->stratum->componentDependencies as $dependency) {
                self::loadScripts($dependency);
            }
        }

        if ($url = self::js($component)) {
            wp_enqueue_script(
                'stratum-component-' . $component,
                $url,
                [],
                false,
                [
                    'in_footer' => true,
                    'strategy' => 'defer'
                ],
            );
        }
    }

    public static function files(string $component): array
    {
        return \Stratum\Asset::files('components/' . $component . '/*', 0, true);
    }

    public static function directory(string $component): string
    {
        return \Stratum\Asset::path('components/' . $component);
    }

    public static function srcDirectory(string $component): ?string
    {
        $component = basename($component);
        $directories = \Stratum\FS::files('_src/components/*/*', GLOB_ONLYDIR);

        // The component we are more than likely looking for is in the 3-blocks
        // directory. These components are closer to the end of the array rather
        // than the start, therefore save a little time in the search by working
        // from the end
        $directories = array_reverse($directories);

        foreach ($directories as $directory) {
            if (basename($directory) == $component) {
                return $directory;
            }
        }

        return null;
    }

    public static function supportsPosts(string $component): bool
    {
        $json = self::json($component);

        return $json->stratum->supportsPosts ?? false;
    }

    public static function resourceHints(string $component): array
    {
        $json = self::json($component);

        return $json->stratum->resourceHints ?? [];
    }

    public static function json(string $component): object
    {
        $asset = 'components/' . basename($component) . '/block.json';

        if (\Stratum\Asset::exists($asset)) {
            $json = \Stratum\Asset::json($asset);

            if (is_object($json)) {
                return $json;
            }
        }

        return new \stdClass();
    }

    public static function css(string $component): string
    {
        $asset = 'components/' . $component . '/block.css';

        return \Stratum\Asset::exists($asset)
            ? \Stratum\Asset::url($asset)
            : '';
    }

    public static function js(string $component): string
    {
        $asset = 'components/' . $component . '/block.js';
        return \Stratum\Asset::exists($asset)
            ? \Stratum\Asset::url($asset)
            : '';
    }

    public static function default(string $component, string $attribute): string
    {
        $json = self::json($component);

        return $json->attributes->{$attribute}->default ?? '';
    }

    public static function acfRenderCallback(
        array $block,
        string $content,
        bool $isPreview,
        int $postId,
        $wpBlock,
        $context
    ): void {
        // Pull the component name from the absolute path
        $component = basename($block['path']);

        echo self::render(
            $component,
            // args that render the component
            \Stratum\WordPress\Gutenberg::args(\get_fields(), $block),
            [],
            // Other data that is not used to render the component but could be
            // used to modify behaviour
            [
                'component' => $component,
                'acf' => [
                    'content' => $content,
                    'isPreview' => $isPreview,
                    'postId' => $postId,
                    'block' => $wpBlock,
                    'context' => $context,
                ],
            ]
        );
    }
}
