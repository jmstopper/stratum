<?php

namespace Stratum;

class FS
{
    final public static function resolve(string $file): string
    {
        // Add the theme path to the file
        $file = static::path($file);

        if (is_dir($file)) {
            $file = $file . '/index.php';
        }

        // Check if there is an extension, if not add one.
        if (empty(pathinfo($file, PATHINFO_EXTENSION))) {
            $file .= '.php';
        }

        return $file;
    }

    final public static function files(string $path, int $flags = 0, bool $relative = false): array
    {
        $files = glob(static::path($path), $flags);

        if (!is_array($files)) {
            return [];
        }

        return $relative === true
            ? array_map(function ($file) {
                return basename($file);
            }, $files)
            : $files;
    }

    /**
    * @return array<mixed>
    */
    final public static function json(string $file, bool $asArray = false): object|array
    {
        if (empty(pathinfo($file, PATHINFO_EXTENSION))) {
            $file .= '.json';
        }

        return json_decode(static::content($file), $asArray);
    }

    final public static function content(string $file): string
    {
        return \trim(\file_get_contents(
            static::path($file)
        ));
    }

    final public static function exists(string $file): bool
    {
        return \file_exists(
            static::path($file)
        );
    }

    final public static function require(string $file): mixed
    {
        return require static::path($file);
    }

    final public static function url(string $file): string
    {
        return \Stratum\Theme::url(static::relPath($file));
    }

    final public static function csv(string $file, bool $headers = false): array
    {
        if (empty(pathinfo($file, PATHINFO_EXTENSION))) {
            $file .= '.csv';
        }

        $contents = file(static::path($file), FILE_SKIP_EMPTY_LINES);

        if ($contents === false) {
            return [];
        }

        $csv = array_map("str_getcsv", $contents);

        if ($headers === true) {
            $keys = array_shift($csv);

            foreach ($csv as $i => $row) {
                $csv[$i] = array_combine($keys, $row);
            }
        }

        return $csv;
    }

    final public static function path(string $file): string
    {
        return \Stratum\Theme::path(
            static::relPath($file)
        );
    }

    // This exists so it can be overwritten in a class that extends file
    public static function relPath(string $file): string
    {
        return $file;
    }
}
