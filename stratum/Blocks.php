<?php

namespace Stratum;

class Blocks
{
    private array $headerBlocks = [];
    private array $contentBlocks = [];
    private array $footerBlocks = [];
    private bool $seperateFirstLast = false;

    public function __construct(mixed $thing = null, bool $seperateFirstLast = false)
    {
        // If $thing is empty and we are on the 404 page, pull the 404 template post
        if ($thing === null && is_404()) {
            $thing = \Stratum\Template::post('404');
        }

        // If we now have an instance of a \WP_Post in thing, lets pull the blocks
        if ($thing instanceof \WP_Post) {
            $this->contentBlocks = self::removeEmptyBlocks(\parse_blocks($thing->post_content));
        }

        // Do we need to seperate the top and bottom of the page when searching blocks?
        $this->seperateFirstLast = $seperateFirstLast;

        // We should rewrite the Template system so that
        // 1. The logic for retrieving a template is not magic
        // 2. There is not a header and footer, there is just one post with a
        // template loop component
        if (!is_404()) {
            $before = \Stratum\Template::post(['suffix' => 'before']);
            $after = \Stratum\Template::post(['suffix' => 'after']);

            $this->headerBlocks = $before instanceof \WP_Post
                ? self::removeEmptyBlocks(parse_blocks($before->post_content))
                : [];

            $this->footerBlocks = $after instanceof \WP_Post
                ? self::removeEmptyBlocks(parse_blocks($after->post_content))
                : [];

            if (!is_singular()) {
                $content = \Stratum\Template::post();

                $this->contentBlocks = $content instanceof \WP_Post
                    ? self::removeEmptyBlocks(parse_blocks($content->post_content))
                    : [];
            }
        }
    }

    public function first(): ?\Stratum\Block
    {
        $blocks = $this->seperateFirstLast
            ? $this->headerBlocks()
            : $this->get();

        return !empty($blocks[0])
            ? \Stratum\Block::seed($blocks[0])
            : null;
    }

    public function last(): ?\Stratum\Block
    {
        $blocks = $this->seperateFirstLast
            ? $this->footerBlocks()
            : $this->get();

        return !empty($blocks)
            ? \Stratum\Block::seed(end($blocks))
            : null;
    }

    /**
     * @return array<array<mixed>>
     */
    public function get(?int $count = null): array
    {
        $blocks = array_merge(
            $this->headerBlocks,
            $this->contentBlocks,
            $this->footerBlocks,
        );

        return is_int($count)
            ? array_splice($blocks, 0, $count)
            : $blocks;
    }

    public function hasQueryLoop(): bool
    {
        return $this->hasBlock('acf/query-loop');
    }

    public function hasBlock(string $name, ?array $blocks = null): bool
    {
        // On the first call $blocks should be null.
        if ($blocks === null) {
            $blocks = $this->get();
        }

        foreach ($blocks as $block) {
            // The conditionals below might need adding to to check specifically
            // for the acf/query-loop component as it loads components dynamically

            $sBlock = \Stratum\Block::seed($block);

            if ($sBlock->name() === $name) {
                return true;
            }

            // If the block is a reusable block
            if ($sBlock->reusable()) {
                $reusableBlocks = parse_blocks(get_the_content(null, false, $block['attrs']['ref']));

                if ($this->hasBlock($name, $reusableBlocks)) {
                    return true;
                }
            }

            // If the block supports <InnerBlocks>
            if ($sBlock->hasInnerBlocks()) {
                if ($this->hasBlock($name, $block['innerBlocks'])) {
                    return true;
                }
            }
        }

        return false;
    }

    public function coreBlocksUsed(): bool
    {
        $nonStyledBlocks = self::nonStyledBlocks();

        $blocks = array_filter($this->get(), function ($block) use ($nonStyledBlocks) {
            if (
                empty($block['blockName']) ||
                str_starts_with($block['blockName'], 'acf/') ||
                in_array($block['blockName'], $nonStyledBlocks)
            ) {
                return false;
            }

            return true;
        });

        return !empty($blocks);
    }

    public static function seed(mixed $thing = '', bool $seperateFirstLast = false): self
    {
        return new self($thing, $seperateFirstLast);
    }

    /**
     * @return array<array<mixed>>
     */
    private function headerBlocks(): array
    {
        return $this->headerBlocks;
    }

    /**
     * @return array<array<mixed>>
     */
    private function footerBlocks(): array
    {
        return $this->footerBlocks;
    }

    /**
     * @return array<array<mixed>>
     */
    private function postBlocks(): array
    {
        return $this->contentBlocks;
    }

    private static function nonStyledBlocks(): array
    {
        return [
            'core/paragraph',
            'core/image',
            'core/heading',
            'core/buttons',
            'core/gallery',
            'core/list',
            'core/quote',
            'core/shortcode',
            'core/audio',
            'core/code',
            'core/embed',
            'core/freeform',
            'core/html',
            'core/missing',
            'core/preformatted',
            'core/separator',
            'core/block',
            'core/table',
            'core/video',
            'core/details',
            'core/footnotes'
        ];
    }

    /**
     * @param array<array<mixed>> $blocks
     * @return array<array<mixed>>
     */
    private static function removeEmptyBlocks(array $blocks): array
    {
        return array_values(array_filter($blocks, [__CLASS__, 'isNonEmptyBlock']));
    }

    /**
     * @param array<mixed> $block
     */
    private static function isNonEmptyBlock(array $block): bool
    {
        return !($block['blockName'] === null && strlen(trim($block['innerHTML'])) === 0);
    }
}
