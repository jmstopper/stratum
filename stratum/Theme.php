<?php

namespace Stratum;

class Theme
{
    public static function url(string $path = ''): string
    {
        // https://developer.wordpress.org/reference/functions/get_stylesheet_directory_uri/
        // Supports child theme overriding
        return \trailingslashit(\get_stylesheet_directory_uri()) . $path;
    }

    public static function path(string $path = ''): string
    {
        // Should probably review how this function is used to see if we can
        // remove the ternary and always return the version with a trailing slash
        // https://developer.wordpress.org/reference/functions/get_theme_file_path/
        // Supports child theme overriding
        return $path !== ''
            ? \trailingslashit(\get_theme_file_path()) . $path
            : \get_theme_file_path();
    }

    public static function json()
    {
        return \Stratum\FS::json('theme');
    }
}
