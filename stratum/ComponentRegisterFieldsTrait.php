<?php

namespace Stratum;

trait ComponentRegisterFieldsTrait
{
    public static function registerFields($component): void
    {
        $json = self::json($component);

        if (!empty($json->stratum->registerFields) && $json->stratum->registerFields === true) {
            if (self::hasLocalACF($component) === false) {
                acf_add_local_field_group(
                    [
                    'key' => 'block_' . $json->name,
                    'title' => 'Block - ' . $json->title,
                    'fields' => self::generateACFFields($json->stratum->fields),
                    'location' => [
                        [
                            [
                                'param' => 'block',
                                'operator' => '==',
                                'value' => 'acf/' . $json->name,
                            ],
                        ],
                    ],
                    ]
                );
            }
        }
    }

    public static function hasLocalACF(string $component): bool
    {
        $files = self::files($component);

        foreach ($files as $file) {
            if (\str_starts_with($file, 'group_') && pathinfo($file, PATHINFO_EXTENSION) === 'json') {
                return true;
            }
        }

        return false;
    }

    public static function generateACFFields($fields): array
    {
        $acfFields = [];

        foreach ($fields as $name => $fieldObj) {
            // Skip this field if it _register is false
            if (isset($fieldObj->_register) && $fieldObj->_register === false) {
                continue;
            }

            // If there is no type
            if (empty($fieldObj->type)) {
                continue;
            }

            // acf_add_local_field_group expects an array of arrays for fields
            $acfFields[] = self::generateACFField((array) $fieldObj, $name);
        }

        return $acfFields;
    }

    public static function generateACFField(array $field, string $name): array
    {
        // ---------------------------------------------------------------------
        // Pull the first or only type as it could be one of many
        // ---------------------------------------------------------------------
        $type = \is_array($field['type'])
            ? $field['type'][0]
            : $field['type'];

        if ($type === 'bool') {
            $type = 'true_false';
        }

        // ---------------------------------------------------------------------
        // Required fields for registering a field
        // - key, label, name, type
        // ---------------------------------------------------------------------
        if (empty($field['key'])) {
            $field['key'] = 'field_' . $name;
        }

        if (empty($field['label'])) {
            $field['label'] = ucfirst($name);
        }

        if (empty($field['name'])) {
            $field['name'] = $name;
        }

        if (empty($field['type'])) {
            $field['type'] = $type;
        }

        // ---------------------------------------------------------------------
        // Remove fields starting with an underscore
        // ---------------------------------------------------------------------
        foreach ($field as $key => $value) {
            if (\str_starts_with($key, '_')) {
                unset($field[$key]);
            }
        }

        // ---------------------------------------------------------------------
        // Ship it
        // ---------------------------------------------------------------------
        return $field;
    }
}
