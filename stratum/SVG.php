<?php

namespace Stratum;

class SVG
{
    /**
    * @param array<mixed> $args
    */
    public static function render(string $name, array $args = []): string
    {
        // How to edit an SVG in PHP
        // https://stackoverflow.com/questions/41264017/php-svg-editing
        // https://stackoverflow.com/questions/18758101/domdocument-add-attribute-to-root-tag

        // Merge attributes
        $args = wp_parse_args($args, [
            'id' => uniqid($name . '-'),
            'title' => '',
            'description' => '',
            'width' => '',
            'height' => '',
        ]);

        if (!empty($args['alt']) && empty($args['title'])) {
            $args['title'] = $args['alt'];
            unset($args['alt']);
        }

        // Create a new instance of DOMDocument
        $doc = new \DOMDocument();

        if (empty(pathinfo($name, PATHINFO_EXTENSION))) {
            $name .= '.svg';
        }

        // Load in the SVG
        $doc->loadXML(\Stratum\Asset::content('svgs/' . $name));

        // Is there a title
        if ($args['title'] !== '') {
            $doc->documentElement->setAttribute('role', 'img');

            $titleID = 'title-' . $args['id'];

            $labelled = [];
            $labelled[] = $titleID;

            // Add a title
            $title = $doc->createElement('title', esc_attr($args['title']));
            $title->setAttribute('id', 'title-' . $args['id']);

            // Append it to the SVG
            $doc->firstChild->appendChild($title);

            // Is there a description
            if ($args['description'] !== '') {
                $descriptionID = 'description-' . $args['id'];
                $labelled[] = $descriptionID;

                // Add a description
                $description = $doc->createElement('description', esc_attr($args['description']));
                $description->setAttribute('id', $descriptionID);

                // Append it to the SVG
                $doc->firstChild->appendChild($description);
            }

            // Add the attributes to the SVG
            $doc->documentElement->setAttribute('aria-labelledby', implode(' ', $labelled));
        } else {
            $doc->documentElement->setAttribute('aria-hidden', 'true');
        }

        if ($args['width'] !== '') {
            $doc->documentElement->setAttribute('width', $args['width']);
        }

        if ($args['height'] !== '') {
            $doc->documentElement->setAttribute('height', $args['height']);
        }

        if (!empty($args['wrapper'])) {
            foreach ($args['wrapper'] as $key => $value) {
                $doc->documentElement->setAttribute($key, $value);
            }
        }

        // output the svg markup and strip the xml doctype declaration
        // https://stackoverflow.com/questions/5706086/php-domdocument-output-without-xml-version-1-0-encoding-utf-8/17362447
        $svg = $doc->saveXML($doc->documentElement);

        return $svg;
    }

    /**
     * Build the path to the SVG asset in the theme
     *
     * @param  string $name
     * @return string
     */
    public static function path(string $name): string
    {
        return \Stratum\Asset::path('svgs/' . $name . '.svg');
    }

    /**
     * Build the URL for the SVG
     *
     * @param  string $name
     * @return string
     */
    public static function url(string $name): string
    {
        return \Stratum\Asset::url('svgs/' . $name . '.svg');
    }
}
