<?php

namespace Stratum\WordPress;

class Cleanup
{
    public static function init(): void
    {
        self::headTags();
        self::emoji();

        // Remove events and news widget
        add_action('wp_network_dashboard_setup', [__CLASS__, 'removeEventsAndNews'], 20);
        add_action('wp_user_dashboard_setup', [__CLASS__, 'removeEventsAndNews'], 20);
        add_action('wp_dashboard_setup', [__CLASS__, 'removeEventsAndNews'], 20);

        if (!is_admin()) {
            // Remove extra attributes from script and style tags
            add_filter('style_loader_tag', [__CLASS__, 'cleanStylesheetLinks']);
            add_filter('script_loader_tag', [__CLASS__, 'cleanScriptTags']);

            // Remove unneeded classes from body and post tag
            add_filter('body_class', [__CLASS__, 'bodyClass']);
            add_filter('post_class', [__CLASS__, 'postClass']);
        }
    }

    public static function removeEventsAndNews(): void
    {
        remove_meta_box('dashboard_primary', get_current_screen(), 'side');
    }

    private static function emoji(): void
    {
        // Remove all actions related to emojis
        add_action('init', [__CLASS__, 'disableWPEmoji']);

        // Remove DNS prefetch of emoji code
        add_filter('wp_resource_hints', [__CLASS__, 'resourceHints']);

        // Filter to remove TinyMCE emojis
        add_filter('tiny_mce_plugins', [__CLASS__, 'disableEmojiTinymce']);
    }

    private static function headTags(): void
    {
        // remove_action('wp_head', 'wp_oembed_add_host_js');

        // Remove the next and previous rel tags from the head
        // https://developer.wordpress.org/reference/functions/adjacent_posts_rel_link_wp_head/
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);

        // Remove default gallery css
        // https://developer.wordpress.org/reference/hooks/use_default_gallery_style/
        add_filter('use_default_gallery_style', '__return_false');

        // Disable recent comments CSS.
        // https://developer.wordpress.org/reference/hooks/show_recent_comments_widget_style/
        add_filter('show_recent_comments_widget_style', '__return_false');

        // Remove the link tags to the rss for comments
        // https://developer.wordpress.org/reference/hooks/feed_links_show_comments_feed/
        add_filter('feed_links_show_comments_feed', '__return_false');

        // Remove oembed link tags
        // https://developer.wordpress.org/reference/functions/wp_oembed_add_discovery_links/
        remove_action('wp_head', 'wp_oembed_add_discovery_links');

        // Remove rest api links for the current post
        // https://developer.wordpress.org/reference/functions/rest_output_link_wp_head/
        remove_action('wp_head', 'rest_output_link_wp_head', 10);

        // Disable XML-RPC RSD link
        // https://developer.wordpress.org/reference/functions/rsd_link/
        remove_action('wp_head', 'rsd_link');

        // Remove WordPress version number
        // https://developer.wordpress.org/reference/functions/wp_generator/
        remove_action('wp_head', 'wp_generator');

        // Remove wlwmanifest link
        // https://developer.wordpress.org/reference/functions/wlwmanifest_link/
        remove_action('wp_head', 'wlwmanifest_link');

        // Remove shortlink
        // https://developer.wordpress.org/reference/functions/wp_shortlink_wp_head/
        remove_action('wp_head', 'wp_shortlink_wp_head');

        // Disable wp-embeds
        add_action('wp_footer', [__CLASS__, 'footer']);
    }

    /**
     * Attempt to disable wp-embed script from being output
     *
     * @return void
     */
    public static function footer(): void
    {
        wp_dequeue_script('wp-embed');
    }

    public static function resourceHints(array $urls): array
    {
        for ($i = 0; $i < count($urls); $i++) {
            // Removes <link rel="dns-prefetch" href="//s.w.org"> which is
            // preloaded for emojis...
            if (strpos($urls[$i], 'emoji') !== false) {
                unset($urls[$i]);
                break;
            }
        }

        return $urls;
    }

    /**
     * Remove emoji support from WordPress front end
     *
     * @return void
     */
    public static function disableWPEmoji(): void
    {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
        add_filter('emoji_svg_url', '__return_false');
    }

    /**
     * Remove emoji support from WordPress back end
     *
     * @param  array $plugins
     * @return array
     */
    public static function disableEmojiTinymce(array $plugins = []): array
    {
        return array_diff(
            $plugins,
            ['wpemoji']
        );
    }

    public static function bodyClass(array $classes): array
    {
        $remove = [
            'page-template-default',
            'post-template-default',
            'single-format-standard',
            'page',
            'single',
            'home',
            'wp-embed-responsive',
            'archive',
            'post-type-archive',
        ];

        // Add post/page slug if not present
        if (is_singular()) {
            global $post;

            $classes[] = 'slug-' . $post->post_name;

            $remove[] = 'page-id-' . get_the_id();
            $remove[] = 'postid-' . get_the_id();
        }

        return array_values(array_diff($classes, $remove));
    }

    public static function postClass(array $classes): array
    {
        $remove = [
            'status-publish',
            'hentry',
            'has-post-thumbnail',
            'page',
        ];

        return array_values(array_diff($classes, $remove));
    }

    public static function cleanStylesheetLinks(string $html): string
    {
        $doc = new \DOMDocument();
        // The extra parameters mean that when the saveHTML method is called the
        // html and doctype are omitted
        $doc->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $node = $doc->getElementsByTagName('link')[0];
        $node->removeAttribute('id');

        if ($node->getAttribute('media') === 'all') {
            $node->removeAttribute('media');
        }

        return $doc->saveHTML();
    }

    public static function cleanScriptTags(string $html): string
    {
        $doc = new \DOMDocument();
        // The extra parameters mean that when the saveHTML method is called the
        // html and doctype are omitted
        $doc->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $node = $doc->getElementsByTagName('script')[0];

        // This will crash in the admin area on the media page
        if (empty($node)) {
            return $html;
        }

        $node->removeAttribute('id');
        $node->removeAttribute('type');

        return $doc->saveHTML();
    }
}
