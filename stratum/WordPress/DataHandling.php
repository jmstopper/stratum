<?php

namespace Stratum\WordPress;

class DataHandling
{
    public static function init(): void
    {
        add_filter('wp_kses_allowed_html', [__CLASS__, 'allowedHTML'], 10, 2);
    }

    public static function allowedHTML(array $tags, string $context): array
    {
        if ('post' === $context) {
            $tags['iframe'] = [
                'src' => true,
                'data-src' => true,
                'height' => true,
                'width' => true,
                'frameborder' => true,
                'allowfullscreen' => true,
                'loading' => true,
            ];

            // This is needed if a component with a tabindex is passed through
            // wp_kses_post
            $tags['div']['tabindex'] = true;

            // Allow inner blocks in the admin area
            $tags['innerblocks'] = [
                'template' => true,
                'allowedblocks' => true,
            ];

            $tags['img']['srcset'] = true;
            $tags['img']['sizes'] = true;
            $tags['img']['loading'] = true;
            $tags['img']['decoding'] = true;

            $tags['input']['id'] = true;
            $tags['input']['type'] = true;
            $tags['input']['name'] = true;
            $tags['input']['value'] = true;
            $tags['input']['class'] = true;
            $tags['input']['checked'] = true;

            $tags['select']['class'] = true;
            $tags['option']['value'] = true;

            $tags['svg'] = [
                'xmlns' => true,
                'fill' => true,
                'viewbox' => true,
                'role' => true,
                'aria-hidden' => true,
                'focusable' => true,
                'width' => true,
                'height' => true,
            ];

            $tags['path'] = [
                'd' => true,
                'fill' => true,
                'stroke' => true,
                'stroke-width' => true,
            ];

            $tags['source'] = [
                'src' => true,
                'type' => true,
            ];
        }

        return $tags;
    }
}
