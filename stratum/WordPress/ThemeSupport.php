<?php

namespace Stratum\WordPress;

class ThemeSupport
{
    public static function init(): void
    {
        $GLOBALS['content_width'] = \Stratum\Setting::get('content-width', 800);

        add_action('after_setup_theme', [__CLASS__, 'core']);
        add_action('wp_head', [__CLASS__, 'themeColorMeta']);
        add_filter('body_class', [__CLASS__, 'blockColors']);
        add_post_type_support('page', 'excerpt');
    }

    public static function blockColors(array $classes): array
    {
        $firstBlock = stratum()->page->blocks()->first();
        $lastBlock = stratum()->page->blocks()->last();

        if ($firstBlock instanceof \Stratum\Block) {
            $color = $firstBlock->color();
            if ($color !== '') {
                $classes[] = 'first-block-' . $color;
            }
        }

        if ($lastBlock instanceof \Stratum\Block) {
            $color = $lastBlock->color();
            if ($color !== '') {
                $classes[] = 'last-block-' . $color;
            }
        }

        return $classes;
    }

    public static function themeColorMeta(): void
    {
        $hex = \Stratum\Asset::json('general/site.webmanifest')->theme_color ?? '';

        $firstBlock = stratum()->page->blocks()->first();

        if ($firstBlock instanceof \Stratum\Block) {
            $color = $firstBlock->color();

            if (!empty($color)) {
                $hex = \Stratum\Colors::get($color);
            }
        }

        if ($hex !== '') {
            echo \Stratum\Component::render('el', [
                'el' => 'meta',
                'wrapper' => [
                    'name' => 'theme-color',
                    'content' => esc_attr($hex)
                ]
            ]);
        }
    }

    public static function core(): void
    {
        add_theme_support('title-tag');

        add_theme_support('html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'script',
            'style'
        ]);

        $formats = \Stratum\Setting::get('post-formats', []);

        if (!empty($formats)) {
            add_theme_support('post-formats', $formats);
        }
    }
}
