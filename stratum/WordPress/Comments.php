<?php

namespace Stratum\WordPress;

class Comments
{
    public static function init(): void
    {
        if (!self::enabled()) {
            // Disable support for comments and trackbacks in post types.
            add_action('admin_init', [__CLASS__, 'disableCommentsPostTypesSupport']);

            // Redirect any user trying to access comments page.
            add_action('admin_init', [__CLASS__, 'disableCommentsAdminMenuRedirect']);

            // Remove comments page in menu.
            add_action('admin_menu', [__CLASS__, 'disableCommentsAdminMenu']);

            // Remove comments links from admin bar.
            add_action('wp_before_admin_bar_render', [__CLASS__, 'adminBarRender']);

            // Remove comments links from admin bar.
            add_action('init', [__CLASS__, 'disableCommentsAdminBar']);

            // // Close comments on the front-end.
            add_filter('comments_open', '__return_false', 20);
            add_filter('pings_open', '__return_false', 20);

            // Hide existing comments from the frontend
            add_filter('comments_array', '__return_empty_array');

            add_filter('wp_count_comments', [__CLASS__, 'disableCommentCounts'], 9999, 2);
            add_action('wp_dashboard_setup', [__CLASS__, 'disableWidgets']);
        }
    }

    public static function enabled(): bool
    {
        return \Stratum\Setting::get('enable-comments', false);
    }

    public static function disableWidgets(): void
    {
        // Remove comments metabox from dashboard.
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

        // "At a Glance" widget shows the number of comments on the site.
        remove_meta_box('dashboard_right_now', 'dashboard', 'normal');

        // "Activity" widget queries for recent comments.
        remove_meta_box('dashboard_activity', 'dashboard', 'normal');

        // "WooCommerce Status" widget. Not comment related, but can save near 20 seconds of load time on dashboard.
        remove_meta_box('woocommerce_dashboard_status', 'dashboard', 'normal');
    }

    public static function disableCommentCounts($count, int $postID)
    {
        if ($postID !== 0) {
            return $count;
        }

        return (object) [
            'approved' => 0,
            'moderated' => 0,
            'spam' => 0,
            'trash' => 0,
            'post-trashed' => 0,
            'total_comments' => 0,
            'all' => 0,
        ];
    }

    public static function disableCommentsPostTypesSupport(): void
    {
        $post_types = get_post_types();

        foreach ($post_types as $post_type) {
            if (post_type_supports($post_type, 'comments')) {
                remove_post_type_support($post_type, 'comments');
                remove_post_type_support($post_type, 'trackbacks');
            }
        }
    }

    public static function disableCommentsAdminMenu(): void
    {
        remove_menu_page('edit-comments.php');
    }

    public static function disableCommentsAdminMenuRedirect(): void
    {
        global $pagenow;

        if ('edit-comments.php' === $pagenow) {
            wp_safe_redirect(admin_url());
            exit;
        }
    }

    public static function disableCommentsAdminBar(): void
    {
        if (is_admin_bar_showing()) {
            remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
        }
    }

    public static function adminBarRender(): void
    {
        global $wp_admin_bar;

        $wp_admin_bar->remove_menu('comments');
    }
}
