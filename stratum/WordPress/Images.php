<?php

namespace Stratum\WordPress;

class Images
{
    public static function init(): void
    {
        add_action('init', [__CLASS__, 'removeCoreSizes']);
        add_action('after_setup_theme', [__CLASS__, 'register']);
        add_filter('image_size_names_choose', [__CLASS__, 'imageSizeNamesChoose']);
        add_filter('big_image_size_threshold', '__return_false');
        add_filter('wp_get_attachment_image_attributes', [__CLASS__, 'lazyLoad']);
        add_filter('jpeg_quality', [__CLASS__, 'quality']);
    }

    public static function quality(int $quality): int
    {
        return \Stratum\Setting::get('jpeg-quality', $quality);
    }

    public static function removeCoreSizes(): void
    {
        remove_image_size('1536x1536'); // 2 x Medium Large (1536 x 1536)
        remove_image_size('2048x2048'); // 2 x Large (2048 x 2048)
    }

    public static function lazyLoad(array $attr): array
    {
        // Return if we are in the admin or there is a loading strategy already
        // defined for this image
        if (is_admin() || !empty($attr['loading'])) {
            return $attr;
        }

        $stack = \Stratum\Render::stack();

        // Check if the current root block is the first or second block on the
        // page. If so, eagerly load the image, otherwise load it lazily.
        if (isset($stack[0]['args']['_index'])) {
            if (in_array($stack[0]['args']['_index'], [0, 1])) {
                $attr['loading'] = 'eager';
            } else {
                $attr['loading'] = 'lazy';
            }
        } else {
            $attr['loading'] = 'lazy';
        }

        return $attr;
    }

    /**
     * Register the image sizes we want to use in the theme
     *
     * @return void
     */
    public static function register(): void
    {
        $images = \Stratum\Setting::get('images', []);

        if (is_array($images)) {
            add_theme_support('post-thumbnails');

            foreach ($images as $size) {
                add_image_size(
                    $size->name,
                    $size->width,
                    $size->height,
                    $size->crop ?? false
                );
            }
        }
    }

    public static function imageSizeNamesChoose(array $sizes): array
    {
        $images = \Stratum\Setting::get('images', []);

        if (count($images) > 0) {
            foreach ($images as $size) {
                if (property_exists($size, 'show') && $size->show === true) {
                    $sizes[$size->name] = ucfirst(
                        str_replace(
                            '_',
                            ' ',
                            $size->name
                        )
                    );
                }
            }
        }

        return $sizes;
    }
}
