<?php

namespace Stratum\WordPress;

class Assets
{
    public static function init(): void
    {
        // enqueue styles and scripts
        add_action('wp_enqueue_scripts', [__CLASS__, 'assets']);
        add_action('get_footer', [__CLASS__, 'assetsInFooter'], 11);

        // jQuery
        add_action('wp_default_scripts', [__CLASS__, 'removejQueryMigrate']);
        add_action('wp_default_scripts', [__CLASS__, 'movejQueryToFooter']);

        add_action('admin_enqueue_scripts', [__CLASS__, 'loadAdminStyles']);
    }

    public static function loadAdminStyles(): void
    {
        wp_enqueue_style(
            'stratum-admin',
            \Stratum\Asset::url('styles/admin.css'),
            [],
            null
        );
    }

    /**
     * Registers styles and scripts required by our theme
     *
     * @return void
     */
    public static function assets(): void
    {
        /*
        * If we are in wp-admin, load the default stylesheet, otherwise load
        * up our custom stylesheet. This probs has to change with Gutenberg on
        * the horizon
        */
        wp_register_style(
            'stratum-core',
            is_admin() ?
                get_stylesheet_uri()
                : \Stratum\Asset::url('styles/styles.css'),
            apply_filters('stratum/assets/styles', []),
            null
        );

        // Register the main script file
        wp_register_script(
            'stratum-core',
            \Stratum\Asset::url('scripts/scripts.js'),
            apply_filters('stratum/assets/scripts', []),
            null,
            [
                'in_footer' => true, // Always load Javascript in the footer, we never want it blocking css
                'strategy' => 'defer'
            ]
        );


        if (\Stratum\Setting::get('ajax', false)) {
            wp_localize_script(
                'stratum-core',
                'wp_vars',
                [
                    'ajax_url' => esc_url(admin_url('admin-ajax.php')),
                    'home_url' => esc_url(home_url()),
                    'nonce' => wp_create_nonce('stratum'),
                ]
            );
        }

        wp_enqueue_style('stratum-core');
        do_action('stratum/assets/styles/after');
        wp_enqueue_script('stratum-core');
        do_action('stratum/assets/scripts/after');
    }

    public static function assetsInFooter(): void
    {
        \Stratum\Component::loadAssets('footer');
    }

    public static function movejQueryToFooter($wp_scripts): void
    {
        if (!is_admin()) {
            $wp_scripts->add_data('jquery', 'group', 1);
            $wp_scripts->add_data('jquery-core', 'group', 1);
        }
    }

    public static function removejQueryMigrate($scripts): void
    {
        if (!is_admin() && isset($scripts->registered['jquery'])) {
            $script = $scripts->registered['jquery'];

            if ($script->deps) {
                $script->deps = array_diff($script->deps, array('jquery-migrate'));
            }
        }
    }
}
