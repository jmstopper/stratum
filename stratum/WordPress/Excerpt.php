<?php

namespace Stratum\WordPress;

class Excerpt
{
    public static function init(): void
    {
        add_filter('excerpt_more', [__CLASS__, 'more']);
        add_filter('excerpt_length', [__CLASS__, 'length']);
    }

    public static function more(string $more): string
    {
        return \Stratum\Setting::get('excerpt/more', $more);
    }

    public static function length(int $length): string
    {
        return \Stratum\Setting::get('excerpt/length', $length);
    }
}
