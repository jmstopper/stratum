<?php

namespace Stratum\WordPress;

class ResourceHints
{
    public static function init(): void
    {
        add_action('wp_head', [__CLASS__, 'resourceHints'], 999);
        add_filter('stratum/resource-hints', [__CLASS__, 'header']);
        add_filter('stratum/resource-hints', [__CLASS__, 'earlyBlocks']);
    }

    public static function resourceHints(): void
    {
        $assets = apply_filters('stratum/resource-hints', []);

        foreach ($assets as $asset) {
            // Allow for providing a string representing a path/url, or an
            // object with more options
            $asset = is_string($asset)
                ? ['href' => $asset]
                : (array) $asset;

            if (empty($asset['href'])) {
                continue;
            }

            // Set a default structure for the hint
            $asset = wp_parse_args($asset, [
                'rel' => 'preload',
                'as' => self::as($asset['href']),
            ]);

            // Is this a local asset
            if (!self::isAbsolute($asset['href'])) {
                // If there is no mimetype try and extract the mimetype from the system
                if (empty($asset['type'])) {
                    // If the system could determine a mimetype
                    if ($type = self::mimeType($asset['href'])) {
                        $asset['type'] = $type;
                    }
                }

                // Finally make the href absolute
                $asset['href'] = \Stratum\Asset::url($asset['href']);
            }

            // If this is a font or fetch type, add crossorigin. This isn't the
            // only times we need crossorigin, but these are explicit times we
            // need crossorigin
            if (in_array($asset['as'], ['font', 'fetch'])) {
                $asset['crossorigin'] = true;
            }

            echo \Stratum\Component::render('el', [
                'el' => 'link',
                'wrapper' => $asset,
            ]);
        }
    }

    public static function header(array $resourceHints): array
    {
        return array_merge(
            $resourceHints,
            \Stratum\Component::resourceHints('header')
        );
    }

    public static function earlyBlocks(array $resourceHints): array
    {
        $blocksToLoad = \Stratum\Setting::get('blocks-to-load-above-the-fold', 2);

        if ($blocksToLoad < 1) {
            return $resourceHints;
        }

        $blocks = stratum()->page->blocks()->get($blocksToLoad);

        if (empty($blocks)) {
            return $resourceHints;
        }

        return array_reduce($blocks, function (array $hints, array $block): array {
            return array_merge(
                $hints,
                \Stratum\Component::resourceHints($block['blockName'])
            );
        }, $resourceHints);
    }

    private static function as(string $path): ?string
    {
        // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link#as
        $types = apply_filters('stratum/resource-hints/as', [
            // Fonts
            'woff2' => 'font',
            'woff' => 'font',
            'ttf' => 'font',
            'otf' => 'font',

            // Images
            'svg' => 'image',
            'jpg' => 'image',
            'png' => 'image',

            // Images
            'css' => 'style',

            // Images
            'js' => 'script',
        ]);

        $ext = \pathinfo($path, PATHINFO_EXTENSION);

        return \array_key_exists($ext, $types)
            ? $types[$ext]
            : null;
    }

    private static function mimeType(string $relPath): ?string
    {
        // https://www.php.net/manual/en/function.mime-content-type.php
        $mime = \mime_content_type(\Stratum\Asset::path($relPath));

        return $mime !== false
            ? $mime
            : null;
    }

    private static function isAbsolute($url): bool
    {
        return isset(parse_url($url)['host']);
    }
}
