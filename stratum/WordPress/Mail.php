<?php

namespace Stratum\WordPress;

class Mail
{
    public static function init(): void
    {
        if (\Stratum\isLocal() && \Stratum\Setting::get('mailpit', false) !== false) {
            add_action('phpmailer_init', [__CLASS__, 'configMailPitSMTP']);
        }
    }

    public static function configMailPitSMTP(\PHPMailer\PHPMailer\PHPMailer $phpmailer): void
    {
        // https://mailpit.axllent.org/
        // https://github.com/axllent/mailpit
        // https://mailpit.axllent.org/docs/configuration/smtp/
        // https://github.com/PHPMailer/PHPMailer

        $ip = \Stratum\Setting::get('mailpit/ip', false);
        $port = \Stratum\Setting::get('mailpit/port', false);
        $connection = @fsockopen($ip, $port);

        if ($connection) {
            $phpmailer->IsSMTP();
            $phpmailer->Host = $ip;
            $phpmailer->Port = $port;
        }
    }
}
