<?php

namespace Stratum\WordPress;

class Menus
{
    public static function init(): void
    {
        \add_action('after_setup_theme', [__CLASS__, 'register'], 10);
        \add_filter('nav_menu_css_class', [__CLASS__, 'removeClasses']);
        \add_filter('nav_menu_item_id', '__return_empty_string');
        \add_filter('walker_nav_menu_start_el', [__CLASS__, 'fixMenuItemSemantics'], 1, 2);
    }

    public static function fixMenuItemSemantics($output, $item)
    {
        if ($item->url === '#0' && !in_array('menu-item-has-children', $item->classes)) {
            return \Stratum\Component::render('el', [
                'el' => 'span',
                'content' => esc_html($item->title)
            ]);
        }

        return $output;
    }

    /**
     * Register menus for use in WordPress
     *
     * Loops over each element defined in STRATUM_MENUS and registers a menu for it.
     * We use this to shortcut the overhead each time.
     *
     * @return void
     */
    public static function register(): void
    {
        $menus = \Stratum\Setting::get('menus', []);

        if (!empty($menus)) {
            $menuArgs = [];

            foreach ($menus as $slug => $menu) {
                $menuArgs[$menu->slug] = $menu->label;
            }

            register_nav_menus($menuArgs);
        }
    }

    public static function removeClasses(array $classes): array
    {
        // https://developer.wordpress.org/reference/functions/wp_list_pages/#more-information
        if (!is_array($classes)) {
            return $classes;
        }

        $classes = array_diff(
            $classes,
            [
                'page_item',
                'menu-item-home',
                'menu-item-type-post_type',
                'menu-item-type-custom',
                'menu-item-type-taxonomy',
                'menu-item-object-page',
                'menu-item-object-custom',
                'menu-item-object-category',
                'current_page_ancestor',
                'current_page_parent',
                'current_page_item',
            ]
        );

        foreach ($classes as $i => $class) {
            // Remove class with menu item id
            $menuID = strtok($class, 'menu-item-');
            $pageID = strtok($class, 'page-item-');

            if (0 < intval($menuID) || 0 < intval($pageID)) {
                unset($classes[$i]);
            }
        }

        return $classes;
    }
}
