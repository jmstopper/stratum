<?php

namespace Stratum\WordPress;

class Gutenberg
{
    public static function init(): void
    {
        add_action('init', [__CLASS__, 'dequeueInlineCss'], 99999);
        add_action('after_setup_theme', [__CLASS__, 'setup']);
        add_action('wp_enqueue_scripts', [__CLASS__, 'dequeueGutenbergCSS'], 100);
        add_action('enqueue_block_editor_assets', [__CLASS__, 'editorAssets']);
        add_filter('block_categories_all', [__CLASS__, 'customBlockCategories']);
        add_theme_support('wp-block-styles');
        add_theme_support('responsive-embeds');
        remove_theme_support('block-templates');
        remove_theme_support('core-block-patterns'); // These are not needed for now

        // Add the default button class to gutenberg buttons
        add_filter('the_content', function ($content) {
            return str_replace('wp-block-button__link', 'btn wp-block-button__link', $content);
        });
    }

    public static function dequeueInlineCss(): void
    {
        // If alignment options are selected for blocks like the buttons block,
        // Gutenberg will output inlined CSS. Not good if we want to implement
        // a CSP. This does mean that the CSS needs to be bundled in to the main
        // stylesheet though.
        remove_action('wp_footer', 'wp_enqueue_stored_styles', 1);
    }

    public static function editorAssets(): void
    {
        wp_enqueue_script(
            'stratum-editor',
            \Stratum\Asset::url('scripts/editor.js'),
            ['wp-blocks', 'wp-dom'],
            '',
            true
        );
    }

    public static function customBlockCategories(array $categories): array
    {
        $name = \Stratum\Setting::get(
            'theme/theme-name',
            _x('Custom', 'Prefix for custom block category', 'stratum-text-domain')
        );

        return array_merge([[
            'slug' => 'stratum-blocks',
            'title' => sprintf(__('%s Blocks', 'stratum-text-domain'), $name),
        ]], $categories);
    }

    public static function dequeueGutenbergCSS(): void
    {
        wp_dequeue_style('global-styles'); // Inline CSS variables
        wp_dequeue_style('wp-block-library-theme');
        wp_dequeue_style('wc-block-style'); // Remove WooCommerce block CSS
        wp_dequeue_style('classic-theme-styles'); // A new custom stylesheet from Gutenberg...

        if (!stratum()->page->blocks()->coreBlocksUsed()) {
            wp_dequeue_style('wp-block-library');
        }
    }

    public static function setup(): void
    {
        add_theme_support('editor-styles');
        add_editor_style(\Stratum\Asset::relPath('styles/editor.css'));

        $components = \Stratum\Components::withAsset('block{.css,*.css}');

        if (count($components) > 0) {
            foreach ($components as $component) {
                add_editor_style(\Stratum\Asset::relPath('components/' . $component . '/block.css'));
            }
        }
    }

    public static function args($args, $block = []): array
    {
        if (!is_array($args)) {
            $args = [
                'classes' => [],
            ];
        }

        if (empty($args['_index'])) {
            $args['_index'] = self::blockIndex($block);
        }

        if (!empty($block['align'])) {
            $args['classes'][] = 'align' . $block['align'];
        }

        if (!empty($block['align_text'])) {
            $args['classes'][] = 'has-text-align-' . $block['align_text'];
        }

        // American english from custom field
        if (!empty($args['background_color']) && !in_array($args['background_color'], ['default'])) {
            $args['classes'][] = 'has-' . $args['background_color'] . '-background-color';
            $args['classes'][] = 'has-background';
        }

        // British english from custom field
        if (!empty($args['background_colour']) && !in_array($args['background_colour'], ['default'])) {
            $args['classes'][] = 'has-' . $args['background_colour'] . '-background-color';
            $args['classes'][] = 'has-background';
        }

        // Gutenberg setting
        if (!empty($block['backgroundColor'])) {
            $args['classes'][] = 'has-' . $block['backgroundColor'] . '-background-color';
            $args['classes'][] = 'has-background';
        }

        if (!empty($block['className'])) {
            $args['classes'][] = $block['className'];
        }

        if (!empty($block['anchor'])) {
            $args['id'] = $block['anchor'];
        }

        return $args;
    }

    public static function blockIndex(array $block): int
    {
        $hash = md5(json_encode($block['data']));
        $blocks = stratum()->page->blocks()->get();

        foreach ($blocks as $key => $block) {
            if (!empty($block['attrs']['data']) && $hash === md5(json_encode($block['attrs']['data']))) {
                return $key;
            }
        }

        return -1;
    }
}
