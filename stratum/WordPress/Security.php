<?php

namespace Stratum\WordPress;

class Security
{
    public static function init(): void
    {
        add_filter('login_errors', [__CLASS__, 'loginErrors']);
        add_filter('wp_headers', [__CLASS__, 'headers']);
    }

    /**
     * Obscure the login failure message. Should probs be moved to a plugin
     *
     * @return string
     */
    public static function loginErrors(): string
    {
        return __(
            "Sorry: We don't recognise that user name and password.",
            'stratum-text-domain'
        );
    }

    public static function headers(array $headers): array
    {
        // https://securityheaders.com/?q=https%3A%2F%2Fbeleaf.agency&followRedirects=on
        $headers['Strict-Transport-Security'] = 'max-age=31536000; includeSubDomains';
        // https://scotthelme.co.uk/hardening-your-http-response-headers/#x-frame-options
        $headers['X-Frame-Options'] = 'SAMEORIGIN';
        // https://scotthelme.co.uk/a-new-security-header-referrer-policy/
        $headers['Referrer-Policy'] = 'no-referrer-when-downgrade';
        // https://www.permissionspolicy.com/
        $headers['Permissions-Policy'] = 'accelerometer=(),ambient-light-sensor=(),autoplay=(), battery=(),camera=(),cross-origin-isolated=(),display-capture=(),document-domain=(),encrypted-media=(),execution-while-not-rendered=(),execution-while-out-of-viewport=(),fullscreen=(),geolocation=(),gyroscope=(),keyboard-map=(),magnetometer=(),microphone=(),midi=(),navigation-override=(),payment=(),picture-in-picture=(),publickey-credentials-get=(),screen-wake-lock=(),sync-xhr=(),usb=(),web-share=(),xr-spatial-tracking=()';

        // https://scotthelme.co.uk/content-security-policy-an-introduction/
        // $scriptHashes = [
        //     // No JS
        //     "'sha256-0laFFXpl+7fg/sWHEWvUVDQcKiDTvJPYxGCENzRL7EU='",
        // ];
        // $scriptHashes = implode(' ', $scriptHashes);
        // $headers['Content-Security-Policy'] = "default-src 'self' $scriptHashes";

        return $headers;
    }
}
