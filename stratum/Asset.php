<?php

namespace Stratum;

class Asset extends \Stratum\FS
{
    public static function relPath(string $asset): string
    {
        // manifest.json needs to be forced not to go through the extract process
        return 'assets/' . ($asset !== 'manifest.json' ? self::extract($asset) : $asset);
    }

    private static function extract(string $asset): string
    {
        return static::json('manifest')->$asset ?? $asset;
    }
}
