<?php

namespace Stratum;

class Stratum
{
    public ?\Stratum\Page\AbstractPage $page = null;
    public array $stack = [];
    public bool $h1Used = false;
    public array $ids = [];
    public bool $mainRendered = false;

    public function __construct()
    {
        $this->initPage();
    }

    public static function init(): void
    {
        require_once 'helpers/require-dir.php';

        \Stratum\requireDir(\get_theme_file_path('stratum/helpers'));
        \Stratum\requireDir(\get_theme_file_path('includes'));

        // Core things that need to be setup
        \Stratum\Setting::init();
        \Stratum\Components::init();
        \Stratum\Template::init();
        \Stratum\Render::init();

        // WordPress optimisations
        \Stratum\WordPress\Assets::init();
        \Stratum\WordPress\Cleanup::init();
        \Stratum\WordPress\Comments::init();
        \Stratum\WordPress\DataHandling::init();
        \Stratum\WordPress\Excerpt::init();
        \Stratum\WordPress\Gutenberg::init();
        \Stratum\WordPress\Images::init();
        \Stratum\WordPress\Mail::init();
        \Stratum\WordPress\Menus::init();
        \Stratum\WordPress\ResourceHints::init();
        \Stratum\WordPress\Search::init();
        \Stratum\WordPress\Security::init();
        \Stratum\WordPress\ThemeSupport::init();

        // Plugin optimisations
        \Stratum\Plugin\ACF::init();
        \Stratum\Plugin\YoastSEO::init();
    }

    public function query(): \WP_Query
    {
        global $wp_query;
        return $wp_query;
    }

    private function initPage(): void
    {
        if (is_singular()) {
            $this->page = \Stratum\Page\Post::seed(get_queried_object());
        } elseif (is_home()) {
            $this->page = \Stratum\Page\PostType::seed(get_post_type_object('post'));
        } elseif (is_post_type_archive()) {
            $this->page = \Stratum\Page\PostType::seed(get_queried_object());
        } elseif (is_category() || is_tag() || is_tax()) {
            $this->page = \Stratum\Page\Term::seed(get_queried_object());
        } elseif (is_author()) {
            $this->page = \Stratum\Page\User::seed(get_queried_object());
        } elseif (is_404()) {
            $this->page = \Stratum\Page\Error404::seed();
        } elseif (is_search()) {
            $this->page = \Stratum\Page\Search::seed();
        } elseif (!empty($_GET['post'])) {
            // If we are in the admin area accessing the Gutenberg edit screen,
            // the post id will be available in the $_GET['post'] array index.
            $this->page = \Stratum\Page\Post::seed(\get_post($_GET['post']));
        } elseif (get_post() instanceof \WP_Post) {
            // If we are on the rest API, and submitting a post, the post will
            // be available under get_post();
            $this->page = \Stratum\Page\Post::seed(\get_post());
        } elseif (!wp_doing_ajax() && !wp_is_rest_endpoint()) {
            echo "Stratum\Stratum->initPage(): error - cant find matching post";
            exit;
        }
    }
}
