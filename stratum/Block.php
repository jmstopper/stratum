<?php

namespace Stratum;

class Block
{
    /**
     * @param array<mixed> $block
     */
    public function __construct(
        public array $block
    ) {
    }

    public function name(): string
    {
        return is_string($this->block['blockName'])
            ? $this->block['blockName']
            : '';
    }

    public function subName(): string
    {
        return basename($this->name());
    }

    public function align(): string
    {
        return $this->block['attrs']['align'] ?? (!empty($this->block['blockName'])
            ? \Stratum\Component::default($this->block['blockName'], 'align')
            : ''
        );
    }

    public function hasInnerBlocks()
    {
        return is_array($this->block['innerBlocks']) && count($this->block['innerBlocks']) > 0;
    }

    public function color(): string
    {
        return $this->block['attrs']['backgroundColor'] ?? (!empty($this->block['blockName'])
            ? \Stratum\Component::default($this->block['blockName'], 'background')
            : ''
        );
    }

    public function isPrimaryHeading(): bool
    {
        return $this->isH1() || $this->hasHeading();
    }

    public function isH1(): bool
    {
        if (!empty($this->block['blockName']) && $this->block['blockName'] === 'core/heading') {
            if (!empty($this->block['attrs']['level']) && $this->block['attrs']['level'] === 1) {
                return true;
            }
        }

        return false;
    }

    public function isCore(): bool
    {
        return str_starts_with($this->name(), 'core');
    }

    public function hasHeading(): bool
    {
        return !empty($this->block['attrs']['data']['heading']) || $this->firstInnerBlockIsACFHeading();
    }

    public function firstInnerBlockIsACFHeading()
    {
        return !empty($this->block['innerBlocks'][0])
            && $this->block['innerBlocks'][0]['blockName'] === 'acf/innerblocks-heading'
            && !empty($this->block['innerBlocks'][0]['innerBlocks'][0]['innerHTML'])
            && trim(wp_strip_all_tags($this->block['innerBlocks'][0]['innerBlocks'][0]['innerHTML'])) !== '';
    }

    public function reusable(): bool
    {
        return !empty($this->block['attrs']['ref']);
    }

    public static function seed(array $block): self
    {
        return new self($block);
    }
}
