<?php

namespace Stratum;

class Setting
{
    public static function init(): void
    {
        self::addHooks();
    }

    public static function get(string $key, mixed $default): mixed
    {
        return apply_filters("stratum/setting/$key", $default);
    }

    private static function addHooks(): void
    {
        $settings = flattenObject(\Stratum\FS::json('stratum'), '', '/');

        foreach ($settings as $name => $value) {
            // https://www.php.net/manual/en/functions.arrow.php
            // We use 1 so that we hook in as early as possible
            add_action(
                'stratum/setting/' . strtolower(str_replace(' ', '-', $name)),
                fn() => $value,
                1
            );
        }
    }
}
