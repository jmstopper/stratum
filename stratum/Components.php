<?php

namespace Stratum;

class Components
{
    public static function init(): void
    {
        // Get the blocks loaded
        add_action('init', [__CLASS__, 'loadPHP']);
        add_action('init', [__CLASS__, 'loadBlocks']);

        // Stratum supports defining the fields for a block in the block.json
        // file. This is overhead that might not be needed so lets disable it
        // by default.
        if (\Stratum\Setting::get('acf/declarative-fields', false)) {
            add_action('acf/init', [__CLASS__, 'registerFields']);
        }

        // Load the page styles and javascript
        add_action('stratum/assets/styles/after', [__CLASS__, 'loadComponentStyles']);
        add_action('wp_head', [__CLASS__, 'injectHeadEarly'], 9);
        add_action('wp_head', [__CLASS__, 'injectHeadLate'], 999);
        add_action('get_footer', [__CLASS__, 'loadComponentsInFooter']);

        // Load acf fields from the components
        add_filter('acf/settings/load_json', [__CLASS__, 'updateLoadPath']);
        add_filter('acf/json/save_paths', [__CLASS__, 'modifyAcfJsonSave'], 10, 2);

        // This allows for defining where blocks should be loaded in
        // the block.json file. Most useful for hardcoded blocks that
        // dont appear in post_content.
        add_action('stratum/assets/styles/after', [__CLASS__, 'loadTemplateStyles']); // Load in the header
        add_action('stratum/assets/scripts/after', [__CLASS__, 'loadTemplateScripts']); // Load in the footer
    }

    public static function injectHeadEarly(): void
    {
        $css = self::withAsset('inject-head-early{.css,*.css}');
        $js = self::withAsset('inject-head-early{.js,*.js}');

        if (count($css) > 0) {
            foreach ($css as $component) {
                echo \Stratum\Component::render('el', [
                    'el' => 'style',
                    'content' => \Stratum\Asset::content('components/' . $component . '/inject-head-early.css'),
                ]);
            }
        }

        if (count($js) > 0) {
            foreach ($js as $component) {
                echo \Stratum\Component::render('el', [
                    'el' => 'script',
                    'content' => \Stratum\Asset::content('components/' . $component . '/inject-head-early.js'),
                ]);
            }
        }
    }

    public static function injectHeadLate(): void
    {
        $css = self::withAsset('inject-head-late{.css,*.css}');
        $js = self::withAsset('inject-head-late{.js,*.js}');

        if (count($css) > 0) {
            foreach ($css as $component) {
                echo \Stratum\Component::render('el', [
                    'el' => 'style',
                    'content' => \Stratum\Asset::content('components/' . $component . '/inject-head-late.css'),
                ]);
            }
        }

        if (count($js) > 0) {
            foreach ($js as $component) {
                echo \Stratum\Component::render('el', [
                    'el' => 'script',
                    'content' => \Stratum\Asset::content('components/' . $component . '/inject-head-late.js'),
                ]);
            }
        }
    }

    public static function withAsset(string $globString): array
    {
        $components = array_unique(glob(\Stratum\Asset::path('components/*/' . $globString), GLOB_BRACE));

        return array_values(array_map(function ($component) {
            // First strip the file, and strip everything before the component name
            return basename(dirname($component));
        }, $components));
    }

    public static function modifyAcfJsonSave(array $paths, array $post): array
    {
        if (!empty($post['location'][0][0]['param']) && $post['location'][0][0]['param'] === 'block') {
            // Build a path to the block source code
            $componentPath = \Stratum\Component::srcDirectory($post['location'][0][0]['value']);

            // Check the path exists
            if (!is_null($componentPath) && is_dir($componentPath)) {
                return [$componentPath];
            }
        }

        return $paths;
    }

    public static function registerFields(): void
    {
        if (function_exists('acf_add_local_field_group')) {
            $components = self::get();

            foreach ($components as $component) {
                \Stratum\Component::registerFields($component);
            }
        }
    }

    public static function get(): array
    {
        return \Stratum\Asset::files('components/*', 0, true);
    }

    public static function loadComponentsInFooter(): void
    {
        $blocks = stratum()->page->blocks()->get();

        foreach ($blocks as $block) {
            if (!empty($block['blockName'])) {
                \Stratum\Component::loadAssets(basename($block['blockName']));
            }
        }
    }

    public static function loadTemplateStyles(): void
    {
        $components = \Stratum\Asset::files('components/*');

        foreach ($components as $component) {
            \Stratum\Component::loadTemplateDependencies(basename($component), 'styles');
        }
    }

    public static function loadTemplateScripts(): void
    {
        $components = \Stratum\Asset::files('components/*');

        foreach ($components as $component) {
            \Stratum\Component::loadTemplateDependencies(basename($component), 'scripts');
        }
    }

    public static function loadComponentStyles(): void
    {
        $blocks = stratum()->page->blocks()->get();

        foreach ($blocks as $block) {
            self::loadComponentStylesRecursive($block);
        }
    }

    public static function loadComponentStylesRecursive(array $block, int $loaded = 0): int
    {
        $sBlock = \Stratum\Block::seed($block);

        if (!$sBlock->isCore()) {
            $blockName = $sBlock->subName();

            if ($loaded <= \Stratum\Setting::get('blocks-to-load-above-the-fold', 2)) {
                \Stratum\Component::loadStyles($blockName);
                $loaded++;
            }
        }

        // If the block is a reusable block
        if ($sBlock->reusable()) {
            $blocks = parse_blocks(get_the_content(null, false, $block['attrs']['ref']));

            foreach ($blocks as $refBlock) {
                $loaded = self::loadComponentStylesRecursive($refBlock, $loaded);
            }
        }

        // If the block supports <InnerBlocks>
        if ($sBlock->hasInnerBlocks()) {
            foreach ($block['innerBlocks'] as $innerBlock) {
                $loaded = self::loadComponentStylesRecursive($innerBlock, $loaded);
            }
        }

        return $loaded;
    }

    public static function loadBlocks(): void
    {
        $blocks = \Stratum\Asset::files('components/*/block.json');

        foreach ($blocks as $block) {
            register_block_type($block);
        }
    }

    public static function loadPHP(): void
    {
        $files = \Stratum\Asset::files(
            'components/*/{gutenberg,functions,args,functions/*,includes/*}.php',
            GLOB_BRACE
        );

        foreach ($files as $file) {
            require_once $file;
        }
    }

    public static function updateLoadPath(array $paths): array
    {
        $components = \Stratum\Asset::files('components/*');

        return array_merge($paths, $components);
    }

    public static function supportPosts(): array
    {
        $components = \Stratum\Components::get();

        $components = array_filter($components, function ($component) {
            return \Stratum\Component::supportsPosts($component);
        });

        return array_values($components);
    }
}
