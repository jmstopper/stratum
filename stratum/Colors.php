<?php

namespace Stratum;

class Colors
{
    /**
     * @return object{'slug': string, "name": string, "color": string, "foreground": string, "secondary": string}[]
     */
    public static function colors(): array
    {
        return \Stratum\Setting::get('colors', []);
    }

    public static function get(string $slug): string
    {
        $colors = self::colors();

        foreach ($colors as $color) {
            if ($color->slug === $slug) {
                return $color->color;
            }
        }

        return '';
    }

    public static function map(): array
    {
        return self::parseColors(
            array_filter(self::colors(), fn ($color) => !empty($color->foreground)),
            'foreground'
        );
    }

    public static function secondary(): array
    {
        return self::parseColors(
            array_filter(self::colors(), fn ($color) => !empty($color->secondary)),
            'secondary'
        );
    }

    private static function parseColors(array $colors, string $key): array
    {
        return array_map(fn ($color) => [
            'background' => $color->slug,
            'foreground' => $color->$key,
        ], $colors);
    }
}
