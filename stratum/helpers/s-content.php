<?php

global $wp_embed;
add_filter('s_content', array( $wp_embed, 'run_shortcode' ), 8);
add_filter('s_content', array( $wp_embed, 'autoembed' ), 8);
add_filter('s_content', 'wptexturize');
add_filter('s_content', 'convert_chars');
add_filter('s_content', 'wpautop');
add_filter('s_content', 'shortcode_unautop');
add_filter('s_content', 'do_shortcode');
