<?php

namespace Stratum;

function minifyJavascript(string $output): string
{
    return preg_replace(array("/\s+\n/", "/\n\s+/", "/ +/"), array("\n", "\n ", " "), $output);
}
