<?php

namespace Stratum;

function uniqueID(string $id): string
{
    if (array_key_exists($id, stratum()->ids)) {
        stratum()->ids[$id] = stratum()->ids[$id] + 1;
        $id = $id . '-' . stratum()->ids[$id];
    } else {
        stratum()->ids[$id] = 0;
    }

    return $id;
}
