<?php

namespace Stratum;

function htmlAttributes(): string
{
    $attributes = [
        'class' => [],
    ];

    $langAttributes = explode(' ', get_language_attributes());

    foreach ($langAttributes as $attribute) {
        $attribute = explode('=', $attribute);
        $attributes[$attribute[0]] = str_replace('"', '', $attribute[1]);
    }

    return attrToString(apply_filters('stratum/html-attributes', $attributes));
}
