<?php

namespace Stratum;

function bodyAttributes(): string
{
    $attributes = [
        'class' => get_body_class(),
    ];

    return attrToString(apply_filters('stratum/body-attributes', $attributes));
}
