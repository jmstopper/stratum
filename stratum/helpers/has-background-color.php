<?php

namespace Stratum;

function hasBackgroundColor(array $classes): bool
{
    return \Stratum\stringPartialInArray($classes, '-background-color');
}
