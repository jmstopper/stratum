<?php

namespace Stratum;

function attrToString(array $attributes = []): string
{
    if (empty($attributes)) {
        return '';
    }

    $myAttributes = [];

    foreach ($attributes as $key => $value) {
        if ($value === true) {
            $myAttributes[] = esc_attr($key);
        } elseif (is_array($value)) {
            if (count($value) > 0) {
                $myAttributes[] = esc_attr($key) . '="' . esc_attr(implode(' ', array_unique($value))) . '"';
            }
        } elseif ($value === '') {
            $myAttributes[] = esc_attr($key) . '=""';
        } elseif ($value !== '') {
            $myAttributes[] = esc_attr($key) . '="' . esc_attr($value) . '"';
        }
    }

    return implode(' ', $myAttributes);
}
