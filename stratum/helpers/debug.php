<?php

namespace Stratum;

function dd($debug, bool $dump = false): void
{
    debugdie($debug, $dump);
}


function debug($debug, bool $dump = false): void
{
    echo '<pre>';

    if ($dump) {
        // ob_start();
        var_dump($debug);
        // echo ob_get_clean();
    } else {
        print_r($debug);
    }

    echo '</pre>';
}

function debugdie($debug, bool $dump = false): void
{
    \Stratum\debug($debug, $dump);
    exit;
}
