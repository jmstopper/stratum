<?php

namespace Stratum;

function vimeoThumbnail(string $url, string $size = '1280'): string
{
    // https://developer.vimeo.com/api/oembed/videos
    $response = wp_remote_get('https://vimeo.com/api/oembed.json?width=' . $size . '&url=' . $url);
    $body = json_decode(wp_remote_retrieve_body($response), true);

    return $body['thumbnail_url'];
}
