<?php

namespace Stratum;

function stringPartialInArray(array $array, string $needle): bool
{
    foreach ($array as $haystack) {
        if (str_contains($haystack, $needle)) {
            return true;
        }
    }

    return false;
}
