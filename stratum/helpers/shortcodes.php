<?php

add_shortcode('page-title', function () {
    return stratum()->page->title();
});

add_shortcode('page-description', function () {
    return method_exists(stratum()->page, 'description')
        ? stratum()->page->description()
        : '';
});
