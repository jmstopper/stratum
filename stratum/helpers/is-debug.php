<?php

namespace Stratum;

function isDebug(): bool
{
    return defined('WP_DEBUG') && WP_DEBUG === true;
}
