<?php

namespace Stratum;

function stripWhitespace(string $string): string
{
    return preg_replace('/\xc2\xa0/', '', $string);
}
