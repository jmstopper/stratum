<?php

namespace Stratum;

function isLocal(): bool
{
    $isLocal = false;

    if (\wp_get_environment_type() === 'local') {
        $isLocal = true;
    } elseif (\str_ends_with($_SERVER['SERVER_NAME'], '.test')) {
        $isLocal = true;
    }

    return \apply_filters('stratum/is-local', $isLocal);
}
