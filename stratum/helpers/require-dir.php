<?php

namespace Stratum;

function requireDir(string $directory): void
{
    $files = glob(trailingslashit($directory) . '*');

    foreach ($files as $file) {
        if (is_dir($file)) {
            requireDir($file);
        } else {
            require_once $file;
        }
    }
}
