<?php

namespace Stratum;

function svg(string $name, array $args = [])
{
    return \Stratum\SVG::render($name, $args);
}
