<?php

namespace Stratum;

function decimalToPercent(float $decimal, int $precision = 0): string
{
    return round($decimal * 100, $precision) . '%';
}
