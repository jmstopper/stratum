<?php

namespace Stratum;

function log($log): void
{
    error_log(is_array($log) || is_object($log)
        ? print_r($log, true)
        : $log);
}
