<?php

namespace Stratum;

function render(string $component, $args = [], $options = [], $adjunct = [])
{
    return \Stratum\Render::render($component, $args, $options, $adjunct);
}
