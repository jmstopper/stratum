<?php

function flattenObject(object $object, string $prefix = '', string $seperator = '.'): array
{
    $result = [];

    foreach ($object as $key => $value) {
        if (is_object($value)) {
            $result = $result + flattenObject($value, $prefix . $key . $seperator, $seperator);
        } else {
            $result[$prefix . $key] = $value;
        }
    }

    return $result;
}
