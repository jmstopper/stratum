<?php

namespace Stratum;

function cleanPhone(string $tel): string
{
    return preg_replace("/[^0-9+-]/", '', $tel);
}
