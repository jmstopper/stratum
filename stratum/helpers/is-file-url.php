<?php

namespace Stratum;

function isFileURL(string $url): bool
{
    return in_array(pathinfo($url, PATHINFO_EXTENSION), [
        'pdf', 'doc', 'docx', 'jpg', 'png'
    ]);
}
