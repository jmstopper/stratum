<?php

namespace Stratum;

function isPageAdmin(): bool
{
    global $pagenow;

    return $pagenow === 'post.php'
        && isset($_GET['post'])
        && get_post_type($_GET['post']) === 'page';
}
