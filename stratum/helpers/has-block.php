<?php

namespace Stratum;

function hasBlock(string $block): bool
{
    return has_block($block)
        || has_block($block, \Stratum\Template::post(['suffix' => 'before']))
        || has_block($block, \Stratum\Template::post(['suffix' => 'after']))
        || \Stratum\Component::shouldLoadComponentByTemplate($block);
}
