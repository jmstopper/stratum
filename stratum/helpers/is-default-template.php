<?php

namespace Stratum;

function isDefaultTemplate(): bool
{
    // get_page_template_slug returns
        // false if the post does not exist.
        // an empty string if the post template is the default
    return empty(\get_page_template_slug());
}
