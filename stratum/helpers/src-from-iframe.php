<?php

namespace Stratum;

function srcFromIframe(string $iframe): string
{
    // if(str_starts_with($iframe, 'http')){
    //     return $iframe;
    // }

    preg_match('/src="([^"]+)"/', $iframe, $match);

    return trim($match[1]);
}
