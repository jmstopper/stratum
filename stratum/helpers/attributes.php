<?php

namespace Stratum;

function attributes(array $args): string
{
    $attributes = [];

    if (!empty($args['id'])) {
        $attributes['id'] = \Stratum\uniqueID(sanitize_title($args['id']));
    } elseif (!empty($args['heading']) && empty($args['_bypass_id'])) {
        $attributes['id'] = \Stratum\uniqueID(sanitize_title($args['heading']));
    }

    if (!empty($args['classes'])) {
        // Convert the array to a string
        $attributes['class'] = implode(
            ' ',
            // Remove duplicate classes. Duplicates occur when Gutenberg adds
            // classes and we do to in the render filter
            array_unique($args['classes'])
        );
    }

    if (!empty($args['wrapper'])) {
        $attributes = array_merge($attributes, $args['wrapper']);
    }

    $attributes = array_map(function ($attribute) {
        return is_scalar($attribute)
            ? $attribute
            : json_encode($attribute);
    }, $attributes);

    return \Stratum\attrToString($attributes);
}
