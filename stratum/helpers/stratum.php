<?php

// namespace Stratum;

function stratum(): \Stratum\Stratum
{
    global $stratum;

    if (!$stratum instanceof \Stratum\Stratum) {
        $stratum = new \Stratum\Stratum();
    }

    return $stratum;
}
