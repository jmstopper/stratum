<?php

namespace Stratum;

function pageObject($thing)
{
    if ($thing instanceof \WP_Post) {
        return \Stratum\Page\Post::seed($thing);
    } elseif ($thing instanceof \WP_Post_Type) {
        return \Stratum\Page\PostType::seed($thing);
    } elseif ($thing instanceof \WP_Term) {
        return \Stratum\Page\Term::seed($thing);
    } elseif ($thing instanceof \WP_User) {
        return \Stratum\Page\User::seed($thing);
    }

    throw new \Exception('$thing is of an unknown type');
}
