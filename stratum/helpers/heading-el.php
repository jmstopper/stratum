<?php

namespace Stratum;

function headingEl(): string
{
    if (stratum()->h1Used !== true) {
        stratum()->h1Used = true;
        return 'h1';
    }

    return 'h2';
}
