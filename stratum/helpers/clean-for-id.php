<?php

namespace Stratum;

function cleanForID(string $string): string
{
    return sanitize_title($string);
}
