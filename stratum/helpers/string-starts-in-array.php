<?php

namespace Stratum;

function stringStartsInArray(array $array, string $needle): bool
{
    foreach ($array as $haystack) {
        if (str_starts_with($haystack, $needle)) {
            return true;
        }
    }

    return false;
}
