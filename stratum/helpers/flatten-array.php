<?php

function flattenArray(array $array, string $prefix = '', string $seperator = '.'): array
{
    $result = [];

    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result = $result + flattenArray($value, $prefix . $key . $seperator, $seperator);
        } else {
            $result[$prefix . $key] = $value;
        }
    }

    return $result;
}
