<?php

namespace Stratum;

function hasAlign(array $classes): bool
{
    return \Stratum\stringStartsInArray($classes, 'align');
}
