<?php

if (!function_exists('dd')) {
    function dd($debug, bool $dump = false): void
    {
        \Stratum\dd($debug, $dump);
    }
}
