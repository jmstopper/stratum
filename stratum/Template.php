<?php

namespace Stratum;

class Template
{
    public static function init(): void
    {
        // ---------------------------------------------------------------------
        // Create the post type that stores the templates
        // ---------------------------------------------------------------------
        add_action('init', [__CLASS__, 'registerPT']);

        // ---------------------------------------------------------------------
        // Modify the options available in the template location dropdown
        // ---------------------------------------------------------------------
        add_filter('acf/load_field/name=template_location', [__CLASS__, 'populateACF']);

        // ---------------------------------------------------------------------
        // Add a column in the admin to show where the template is loaded
        // ---------------------------------------------------------------------
        add_filter('manage_stratum-template_posts_columns', [__CLASS__, 'adminColumn']);

        // ---------------------------------------------------------------------
        // Add the locations the template is loaded to the admin column
        // ---------------------------------------------------------------------
        add_action('manage_stratum-template_posts_custom_column', [__CLASS__, 'adminColumnValue'], 10, 2);

        // ---------------------------------------------------------------------
        // There is no direct link for a 404 page so we force a temporary 404
        // page for testing the output
        // ---------------------------------------------------------------------
        add_action('wp', [__CLASS__, 'force404']);

        // ---------------------------------------------------------------------
        // Output information about templates to the body class
        // ---------------------------------------------------------------------
        add_filter('body_class', [__CLASS__, 'bodyClass']);
    }

    public static function force404(): void
    {
        if (
            !empty($_GET['stratum-template'])
            && $_GET['stratum-template'] === '404'
            && (current_user_can('editor') || current_user_can('administrator'))
        ) {
            global $wp_query;
            $wp_query->set_404();
        }
    }

    public static function populateACF(array $field): array
    {
        $choices = [];

        foreach (self::locations() as $key => $location) {
            $choices[$location['slug']] = $location['name'];
        }

        $field['choices'] = $choices;

        return $field;
    }

    public static function adminColumn(array $columns): array
    {
        $columns['location'] = __('Location', 'stratum-text-domain');
        return $columns;
    }

    public static function adminColumnValue(string $column, int $postID): void
    {
        if ($column === 'location') {
            $slugs = get_field('template_location', $postID);

            if (!empty($slugs)) {
                $locations = array_filter(
                    self::locations(),
                    function ($location) use ($slugs) {
                        return in_array($location['slug'], $slugs);
                    }
                );

                echo \Stratum\Component::render('link-list', [
                    'links' => array_map(function ($location) {
                        return [
                            'title' => $location['name'],
                            'url' => $location['url'] ?? '',
                        ];
                    }, $locations)
                ]);
            }
        }
    }

    public static function registerPT(): void
    {
        register_post_type('stratum-template', [
            'public' => false,
            // 'publicly_queryable' => false,
            // 'exclude_from_search' => true,
            'show_ui' => true,
            'label' => __('Template', 'stratum-text-domain'),
            'show_in_rest' => true,
            'supports' => [
                'title', 'editor',
            ],
        ]);
    }

    public static function bodyClass(array $classes): array
    {
        $header = self::post(['suffix' => 'before']);
        $footer = self::post(['suffix' => 'after']);

        if (!empty($header)) {
            $classes[] = 'has-header-template';
        }

        if (!empty($footer)) {
            $classes[] = 'has-footer-template';
        }

        return $classes;
    }

    // -----------------------------------------------------------------------------
    // A helper function for registering new template areas
    // self::register('slug', 'name', 'url');
    // -----------------------------------------------------------------------------
    public static function register(string $slug, string $name, string $url = ''): void
    {
        add_filter(
            'stratum/template/choices',
            function (array $choices) use ($slug, $name, $url) {
                $choices[] = [
                    'slug' => $slug,
                    'name' => $name,
                    'url' => $url,
                ];

                return $choices;
            }
        );
    }

    public static function render($args = []): string
    {
        return apply_filters(
            'the_content',
            self::content($args)
        );
    }

    public static function content($args = []): string
    {
        $template = self::post($args);

        return $template instanceof \WP_Post
            ? $template->post_content
            : '';
    }

    public static function post($args = []): ?\WP_Post
    {
        $queryArgs = self::query(self::resolve($args));

        if ($queryArgs === null) {
            return null;
        }

        $template = get_posts($queryArgs);

        return !empty($template[0]) && $template[0] instanceof \WP_Post
            ? $template[0]
            : null;
    }

    private static function query(string $location): ?array
    {
        if ($location === '') {
            return null;
        }

        return [
            'numberposts' => 1,
            'post_type' => 'stratum-template',
            'meta_query' => [
                [
                    'key' => 'template_location',
                    // A select field that allows for multiple selections will
                    // be stored in the database as an encoded array. This means
                    // that a query must be made using LIKE as the comparison
                    // with quotes around it as that is how the data is stored.
                    // For example:
                    // a:1:{i:0;s:19:"post-archive-before";}
                    'value' => '"' . $location . '"',
                    'compare' => 'LIKE'
                ],
            ],
        ];
    }

    private static function resolve($args): string
    {
        if (is_string($args)) {
            return $args;
        }

        $queryString = '';
        $queriedObject = get_queried_object();

        if (is_singular()) {
            $queryString = get_post_type() . '-single';
        } elseif (is_a($queriedObject, 'WP_Post_Type') || is_home()) {
            $postType = is_a($queriedObject, 'WP_Post_Type') ? $queriedObject->name : 'post';
            $queryString = $postType . '-archive';
        } elseif (is_search()) {
            $queryString = 'search-results';
        } elseif (is_a($queriedObject, 'WP_Term')) {
            $queryString = $queriedObject->taxonomy . '-archive';
        } elseif (is_author()) {
            $queryString = 'user';
        }

        if ($queryString === '') {
            return '';
        }

        if (!empty($args['suffix'])) {
            $queryString .= '-' . $args['suffix'];
        }

        return $queryString;
    }

    // -----------------------------------------------------------------------------
    // Get all the registered template areas
    // -----------------------------------------------------------------------------
    private static function locations(): array
    {
        $locations = [
            [
                'slug' => '404',
                'name' => __('404', 'stratum-text-domain'),
                'url' => get_home_url() . '?stratum-template=404',
            ],
            [
                'slug' => 'search-results',
                'name' => __('Search results', 'stratum-text-domain'),
                'url' => home_url() . '?s=',
            ],
            [
                'slug' => 'user',
                'name' => __('User archive', 'stratum-text-domain'),
                // 'url' => home_url() . '?s=',
            ],
        ];

        $postTypes = get_post_types([
            'public' => true,
        ], 'objects');

        foreach ($postTypes as $postType) {
            $locations[] = [
                'slug' => $postType->name . '-single-before',
                'name' => $postType->label . ' single header',
            ];

            $locations[] = [
                'slug' => $postType->name . '-single-after',
                'name' => $postType->label . ' single footer',
            ];

            // See if the post type has archive pages enabled
            if ($postType->has_archive === true || $postType->name === 'post') {
                $locations[] = [
                    'slug' => $postType->name . '-archive',
                    'name' => $postType->label . ' archive',
                    'url' => get_post_type_archive_link($postType->name)
                ];
            }
        }

        $taxonomies = get_taxonomies([
            'public' => true,
        ], 'objects');

        foreach ($taxonomies as $taxonomy) {
            $locations[] = [
                'slug' => $taxonomy->name . '-archive',
                'name' => $taxonomy->label . ' archive',
            ];
        }

        return apply_filters('stratum/template/choices', $locations);
    }
}
