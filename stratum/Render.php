<?php

namespace Stratum;

class Render
{
    public static function init(): void
    {
        add_filter('stratum/render', [__CLASS__, 'renderArgs'], 10, 4);
        add_filter('stratum/render/output', [__CLASS__, 'renderOutput'], 10, 2);
    }

    /**
     * Retrieve a partial from the theme and pass arguments to it.
     *
     * Like https://developer.wordpress.org/reference/functions/get_template_part/
     * but allows for arguments to be passed in the form or an array. Each partial
     * defines its own array arguments so view each partial to see what you
     * can/should pass
     *
     * @param  string $component
     * @param  array  $args
     * @param  array  $options
     * @param  array  $adjunct
     * @return string
     */
    public static function render(string $component, $args = [], $options = [], $adjunct = []): string
    {
        // We push the component and args pre filtering so that any hooks used
        // while filtering will know what component is being rendered. Down side
        // is the args wont be the final args.
        self::push($component, $args);

        // Modify the arguments
        $args = apply_filters('stratum/render/' . $component, $args, $options, $adjunct);
        $args = apply_filters('stratum/render', $args, $component, $options, $adjunct);

        // If the args are returned as null after the filters are applied, then
        // terminate the output of this block.
        if ($args === null) {
            self::pop();
            return '';
        }

        // Buffer the output
        ob_start();

        // Immediately Invoked Function Expression (IIFE)
        // We do this so prevent variables used for the render function leaking
        // into the render process of the component. We cant prevent the
        // $_component leaking though
        (function ($_component, $args) {
            require \Stratum\FS::resolve($_component);
        })($component, $args);

        // Retrieve the buffer
        $html = ob_get_clean();

        // Filter it
        $html = apply_filters('stratum/render/output/' . $component, $html, $args);
        $html = apply_filters('stratum/render/output', $html, $component, $args);

        self::pop();

        // return it
        return $html;
    }

    public static function stack(): array
    {
        return stratum()->stack;
    }

    public static function push(string $component, mixed $args = []): void
    {
        stratum()->stack[] = [
            'component' => $component,
            'args' => $args,
        ];
    }

    public static function pop(): void
    {
        array_pop(stratum()->stack);
    }

    public static function renderOutput(string $output, string $path): string
    {
        if (!\Stratum\isDebug()) {
            $json = \Stratum\Component::json($path);

            if (!empty($json->stratum->minifyHTML)) {
                $output = \Stratum\minifyHTML($output);
            }

            if (!empty($json->stratum->minifyJavascript)) {
                $output = \Stratum\minifyJavascript($output);
            }
        }

        return $output;
    }

    public static function renderArgs($args = [], $component = '', $options = [], $raw = [])
    {
        if (!is_array($args)) {
            return $args;
        }

        if (!empty($args['classes'])) {
            if (!\Stratum\hasAlign($args['classes'])) {
                $align = \Stratum\Component::default(basename($component), 'align');

                if (!empty($align)) {
                    $args['classes'][] = 'align' . $align;
                }
            }

            if (!\Stratum\hasBackgroundColor($args['classes'])) {
                $background = \Stratum\Component::default(basename($component), 'background');

                if (!empty($background)) {
                    $args['classes'][] = 'has-' . $background . '-background-color';
                }
            }
        }

        if (!empty($args['media'])) {
            $args['classes'][] = 'has-media';

            if (str_contains($args['media'], '<iframe')) {
                $args['classes'][] = 'has-embed';
            }
        }

        if (!empty($args['icon'])) {
            $args['icon'] = \Stratum\svg('icons/' . $args['icon'], [
                'width' => '64',
                'height' => '64'
            ]);
        }

        if (!empty($args['reverse'])) {
            $args['classes'][] = 'is-reverse';
        }

        if (!empty($args['visibility']) && $args['visibility'] !== 'both') {
            $args['classes'][] = $args['visibility'];
        }

        if (!empty($args['heading']) && empty($args['_bypass_heading_generation'])) {
            if (empty($args['headingEl'])) {
                $args['headingEl'] = \Stratum\headingEl();
            }

            if (empty($args['secondaryHeadingEl'])) {
                $args['secondaryHeadingEl'] = $args['headingEl'] === 'h1'
                    ? 'h2'
                    : 'h3';
            }
        }

        // When a block is using InnerBlocks to render content, the <InnerBlocks />
        // tag must be passed as an argument to the template. Unfortunately, this
        // prevents conditionals from working correctly in the template as the real
        // content does not exist in the argument until after the conditional has
        // run. To address this, we search for the value that contains the
        // <InnerBlocks /> element and replace it before ACF does. This does not
        // work in the admin area so there will be extra DOM elements output, but
        // whatever...
        if (!is_admin()) {
            foreach ($args as $key => $value) {
                if ($value === '<InnerBlocks />') {
                    if (!empty($raw['acf']['content'])) {
                        $args[$key] = $raw['acf']['content'];
                    } elseif ($args['content'] === '<InnerBlocks />') {
                        $args[$key] = '';
                    }
                }
            }
        }

        $optionsToMerge = [];

        foreach ($options as $key => $option) {
            if (str_starts_with($key, '+')) {
                $optionsToMerge[substr($key, 1)] = $option;

                // Remove + prefixed entries so they don't end up in the final args.
                unset($options[$key]);
            } elseif (str_starts_with($key, '_')) {
                // Remove _ prefixed entries so they don't end up in the final args.
                unset($options[$key]);
            }
        }

        // Overwrite the args with options
        $args = array_replace_recursive($args, $options);

        // Merge the options with the args. The value in $optionstToMerge must be
        // merged into an array in $args and not a scalar.
        $args = array_merge_recursive($args, $optionsToMerge);

        // If the background class is added inside a component filter, it wont have
        // the has-background class added inside the gutenberg filter. Lets add it.
        // The attributes function will remove duplicates for us so no need to
        // remove duplicate classes
        if (!empty($args['classes'])) {
            foreach ($args['classes'] as $key => $class) {
                if (str_contains($class, '-background-color') && !str_starts_with($class, 'last-block')) {
                    $args['classes'][] = 'has-background';
                    break;
                }
            }
        }

        return $args;
    }
}
