<?php

namespace Stratum\Page;

class Search extends \Stratum\Page\AbstractListing
{
    public function label(): string
    {
        return __('Search results', 'stratum-text-domain');
        // return sprintf(__('Results for "%s"', 'stratum-text-domain'), get_search_query());
    }

    public function description(): string
    {
        return sprintf(__('%s results found', 'stratum-text-domain'), $this->found());
    }

    public function url(): string
    {
        return home_url('?=');
    }
}
