<?php

namespace Stratum\Page;

class Error404 extends \Stratum\Page\AbstractPage
{
    public function __construct()
    {
        $post = \Stratum\Template::post('404');

        if ($post instanceof \WP_Post) {
            $this->set($post);
        }
    }

    public function title(): string
    {
        return $this->get() instanceof \WP_Post
            ? get_the_title($this->get())
            : '404';
    }

    public function content(bool $format = true): string
    {
        $content = \get_the_content('', false, $this->get());

        if (empty($content)) {
            $content = \Stratum\Component::render('no-content');
        }

        return $format === true
            ? \apply_filters('the_content', $content)
            : $content;
    }

    public function url(): string
    {
        return home_url('?stratum-template=404');
    }
}
