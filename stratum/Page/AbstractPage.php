<?php

namespace Stratum\Page;

abstract class AbstractPage
{
    protected $object = null;
    protected ?\Stratum\Blocks $blocks = null;

    abstract public function title(): string;
    abstract public function url(): string;
    // Post type archives don't have content at this point
    // public function content(bool $format = true): string;

    final public function get()
    {
        return $this->object;
    }

    final protected function set($object)
    {
        $this->object = $object;
    }

    final public function blocks(): \Stratum\Blocks
    {
        if (!$this->blocks instanceof \Stratum\Blocks) {
            $this->blocks = \Stratum\Blocks::seed($this->get());
        }

        return $this->blocks;
    }

    public function injectHeading(): bool
    {
        $firstBlock = $this->blocks()->first();

        return \apply_filters(
            'stratum/page-object/inject-heading',
            !$firstBlock instanceof \Stratum\Block || !$firstBlock->isPrimaryHeading(),
            $this
        );
    }

    // This isnt available across all of the page objects but at this point the
    // definition is exactly the same so no point repeating...
    public function acf(string $key, bool $format = true, bool $escape = false): mixed
    {
        return \get_field($key, $this->get(), $format, $escape);
    }

    public static function seed($object = null): static
    {
        return new static($object);
    }
}
