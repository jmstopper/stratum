<?php

namespace Stratum\Page;

class User extends \Stratum\Page\AbstractListing
{
    public function __construct(\WP_User|int $user)
    {
        if (!$user instanceof \WP_User) {
            $user = \get_userdata($user);
        }

        $this->set($user);
    }

    public function label(): string
    {
        return $this->displayName();
    }

    public function content(bool $format = true): string
    {
        return $this->description($format);
    }

    public function url(): string
    {
        return \get_author_posts_url($this->id());
    }

    public function description(bool $format = true): string
    {
        return $format === true
            ? \apply_filters('s_content', $this->get()->description)
            : $this->get()->description;
    }

    public function id(): int
    {
        return $this->get()->ID;
    }

    public function email(): string
    {
        return $this->get()->user_email;
    }

    public function firstName(): string
    {
        return $this->get()->user_firstname;
    }

    public function lastName(): string
    {
        return $this->get()->user_lastname;
    }

    public function displayName(): string
    {
        return $this->get()->display_name;
    }

    public function username(): string
    {
        return $this->get()->user_login;
    }

    public function nickname(): string
    {
        return $this->meta('nickname');
    }

    public function meta(string $key, bool $single = true): mixed
    {
        return \get_user_meta($this->id(), $key, $single);
    }

    public function posts(array $args = []): array
    {
        $args['author'] = $this->id();

        return array_map(function (\WP_Post $post): \Stratum\Page\Post {
            return \Stratum\Page\Post::seed($post);
        }, get_posts($args));
    }
}
