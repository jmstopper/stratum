<?php

namespace Stratum\Page;

class Post extends \Stratum\Page\AbstractPage
{
    public function __construct(\WP_Post|int|null $myPost)
    {
        if ($myPost === null) {
            // null, false, 0 and other PHP falsey values return the current global post inside the loop
            // https://developer.wordpress.org/reference/functions/get_post/#parameters
            $myPost = get_post();
        }

        if (!$myPost instanceof \WP_Post) {
            $myPost = \get_post($myPost);
        }

        $this->set($myPost);
    }

    public function title(): string
    {
        return get_the_title($this->get());
    }

    public function content(bool $format = true): string
    {
        $content = \get_the_content('', false, $this->get());

        return $format === true
            ? \apply_filters('the_content', $content)
            : $content;
    }

    public function url(): string
    {
        return \get_the_permalink($this->get());
    }

    public function id(): int
    {
        return $this->get()->ID;
    }

    public function publishedDTI(): \DateTimeImmutable|false
    {
        return \get_post_datetime($this->get());
    }

    public function publishedDate(string $format = ''): string
    {
        return \get_the_date($format, $this->get());
    }

    public function publishedTime(string $format = ''): string
    {
        return \get_the_time($format, $this->get());
    }

    public function modifiedDTI()
    {
        return \get_post_datetime($this->get(), 'modified');
    }

    public function modifiedDate(string $format = ''): string
    {
        return \get_the_modified_date($format, $this->get());
    }

    public function modifiedTime(string $format = ''): string
    {
        return \get_the_modified_time($format, $this->get());
    }

    public function timestamp(bool $translate = true): string
    {
        return \get_post_time('U', false, $this->get(), $translate);
    }

    public function author(): \Stratum\Page\User
    {
        return new \Stratum\Page\User($this->get()->post_author);
    }

    public function template(): string
    {
        return \get_page_template_slug($this->get());
    }

    public function status(): string
    {
        return \get_post_status($this->get());
    }

    public function postRequired(): bool
    {
        return \post_password_required($this->get());
    }

    public function password(): string
    {
        return $this->get()->post_password;
    }

    public function sticky(): bool
    {
        return \is_sticky($this->id());
    }

    public function format(): string
    {
        return \get_post_format($this->get());
    }

    public function slug(): string
    {
        return $this->get()->post_name;
    }

    public function parent(): ?self
    {
        return $this->get()->post_parent !== 0
            ? self::seed($this->get()->post_parent)
            : null;
    }

    public function hasImage(): bool
    {
        return has_post_thumbnail($this->get());
    }

    public function image(string $size = 'post-thumbnail', array $attr = []): string
    {
        $id = $this->imageID();

        return is_int($id)
            ? wp_get_attachment_image($id, $size, false, $attr)
            : '';
    }

    public function imageURL(string $size = 'post-thumbnail'): string
    {
        $id = $this->imageID();

        return is_int($id)
            ? wp_get_attachment_image_src($id, $size, false)[0]
            : '';
    }

    public function imageID(): ?int
    {
        return $this->hasImage()
            ? get_post_thumbnail_id($this->get())
            : null;
    }

    public function excerpt(): string
    {
        // If get_the_excerpt function is used outside "the Loop" and the post
        // doesn’t have a custom excerpt, the excerpt will not be generated
        // correctly due to the internal logic of wp_trim_excerpt and
        // get_the_content not being provided the post object.
        // https://developer.wordpress.org/reference/functions/get_the_excerpt/#comment-2457

        // In order to avoid this issue, we bypass get_the_excerpt and use
        // wp_trim_excerpt directly
        $excerpt = !empty($this->get()->post_excerpt)
            ? $this->get()->post_excerpt
            : \wp_trim_excerpt('', $this->get());

        return \apply_filters('the_excerpt', $excerpt);
    }

    public function categories(): array|\WP_Error
    {
        return $this->terms('category');
    }

    public function tags(): array|\WP_Error
    {
        return $this->terms('post_tag');
    }

    public function taxonomies(bool $objects = false): array
    {
        return \get_object_taxonomies(
            $this->type(),
            $objects === true
                ? 'objects'
                : 'names'
        );
    }

    public function terms(string $taxonomy): array|\WP_Error
    {
        $terms = \get_the_terms($this->get(), $taxonomy);

        return !is_bool($terms)
            ? $terms
            : [];
    }

    public function meta(string $key, bool $single = true): mixed
    {
        return \get_post_meta($this->id(), $key, $single);
    }

    public function type(): string
    {
        return $this->get()->post_type;
    }

    public function yoast(string $key): mixed
    {
        // This is the YoastSEO Surfaces API.
        // A reference for the keys that can be provided is available at:
        // https://developer.yoast.com/customization/apis/surfaces-api/
        // @phpstan-ignore-next-line
        return \YoastSEO()->meta->for_post($this->id())->{$key};
    }
}
