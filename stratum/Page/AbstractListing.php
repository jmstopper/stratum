<?php

namespace Stratum\Page;

abstract class AbstractListing extends \Stratum\Page\AbstractPage
{
    abstract public function label(): string;

    public function title(): string
    {
        return $this->page() > 1
            ? $this->label() . ' - ' . $this->paginationString()
            : $this->label();
    }

    public function paginationString(): string
    {
        return sprintf(
            __('Page %d of %d', 'stratum-text-domain'),
            $this->page(),
            $this->pages()
        );
    }

    public function page(): int
    {
        return stratum()->query()->query_vars['paged'];
    }

    public function pages(): int
    {
        return stratum()->query()->max_num_pages;
    }

    public function found(): int
    {
        return stratum()->query()->found_posts;
    }
}
