<?php

namespace Stratum\Page;

class PostType extends \Stratum\Page\AbstractListing
{
    public function __construct(\WP_Post_Type|string $postType)
    {
        if (!$postType instanceof \WP_Post_Type) {
            $postType = \get_post_type_object($postType);
        }

        $this->set($postType);
    }

    public function label(): string
    {
        return $this->get()->label;
    }

    public function url(): string
    {
        return get_post_type_archive_link($this->get()->name);
    }

    public function type(): string
    {
        return $this->get()->name;
    }
}
