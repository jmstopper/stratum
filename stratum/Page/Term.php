<?php

namespace Stratum\Page;

class Term extends \Stratum\Page\AbstractListing
{
    public function __construct(\WP_Term|int $term)
    {
        if (!$term instanceof \WP_Term) {
            $term = \get_term($term);
        }

        $this->set($term);
    }

    public function label(): string
    {
        return $this->get()->name;
    }

    public function content(bool $format = true): string
    {
        return $this->description($format);
    }

    public function url(): string
    {
        return \get_term_link($this->get());
    }

    public function id(): int
    {
        return $this->get()->term_id;
    }

    public function name(): string
    {
        return $this->get()->name;
    }

    public function slug(): string
    {
        return $this->get()->slug;
    }

    public function description(bool $format = true): string
    {
        return $format === true
            ? \apply_filters('s_content', $this->get()->description)
            : $this->get()->description;
    }

    public function count(): int
    {
        return $this->get()->count;
    }

    public function taxonomy(bool $object = false): string
    {
        return $object === true
            ? get_taxonomy($this->get()->taxonomy)
            : $this->get()->taxonomy;
    }

    public function parent(): ?self
    {
        return $this->get()->parent !== 0
            ? self::seed($this->get()->parent)
            : null;
    }

    public function posts(array $args = [], bool $children = true): array
    {
        if (!is_array($args['tax_query'])) {
            $args['tax_query'] = [];
        }

        $args['tax_query'][] = [
            'field' => 'term_id',
            'taxonomy' => $this->taxonomy(),
            'terms' => $this->id(),
            'include_children' => $children,
        ];

        return array_map(function (\WP_Post $post): \Stratum\Page\Post {
            return \Stratum\Page\Post::seed($post);
        }, get_posts($args));
    }

    public function children(): array|\WP_Error
    {
        return \get_term_children($this->get()->term_id, $this->get()->taxonomy);
    }

    public function meta(string $key, bool $single = true): mixed
    {
        return \get_term_meta($this->id(), $key, $single);
    }
}
