import ESLintPlugin from 'eslint-webpack-plugin';
import DependencyExtractionWebpackPlugin from '@wordpress/dependency-extraction-webpack-plugin';

export default (options) => {
    // -------------------------------------------------------------------------
    // Lets build the filename of the scripts we are creating
    // -------------------------------------------------------------------------
    const filename = options.production === true
        ? '[name].[chunkhash].js'
        : '[name].js';

    const mode = options.production === true
        ? 'production'
        : 'development';

    // -------------------------------------------------------------------------
    // Begin the config
    // -------------------------------------------------------------------------
    const config = {
        mode,
        output: {
            filename,
        },
        plugins: [
            new ESLintPlugin({
                fix: true,
            }),
            new DependencyExtractionWebpackPlugin(),
        ],
        module: {
            rules: [],
        },
        // ---------------------------------------------------------------------
        // If you need to use jQuery from the global namespace, bring these
        // lines back in
        // ---------------------------------------------------------------------
        // externals: {
        //     jquery: 'jQuery',
        //     $: 'jQuery',
        //     jQuery: 'jQuery'
        // },
        // ---------------------------------------------------------------------
        // Don't output webpack statistics
        // ---------------------------------------------------------------------
        stats: 'errors-warnings',
    };

    // -------------------------------------------------------------------------
    // Only run through babel on a production build.
    // In testing on an empty project, a build without babel tool under 500ms
    // and nearly 2 seconds with babel.
    // -------------------------------------------------------------------------
    if (options.production === true) {
        config.module.rules.push({
            test: /\.m?js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
        });
    }

    // -------------------------------------------------------------------------
    // Ship it
    // -------------------------------------------------------------------------
    return config;
};
