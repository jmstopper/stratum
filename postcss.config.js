import cssNano from 'cssnano';

export default {
    plugins: [
        cssNano({
            preset: ['default', {
                discardComments: {
                    removeAll: true,
                },
                discardUnused: true,
                mergeIdents: true,
                reduceIdents: true,
            }],
        }),
    ],
};
