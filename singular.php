<?php

get_header();

$content = '';

if (have_posts()) {
    while (have_posts()) {
        the_post();

        if (stratum()->page->injectHeading()) {
            $content .= \Stratum\Component::render('wysiwyg', [
                'heading' => stratum()->page->title(),
                // 'media' => stratum()->page->image('large'),
                'classes' => ['has-text-align-center'],
            ]);
        }

        // Add the content
        $content .= stratum()->page->content();

        // Semantically, comments belong inside the article tag
        if (\Stratum\WordPress\Comments::enabled()) {
            $content .= \Stratum\Component::render('comments');
        }

        // Wrap it all in an article
        $content = \Stratum\Component::render('article', $content);
    }
}

// Add the pre main tag as the contents of main needs to be unique
$content = \Stratum\Component::render('main', $content);

// -----------------------------------------------------------------------------
// Pagination belongs outside the main tag
// -----------------------------------------------------------------------------
// $content .= \Stratum\Component::render('pagination/paged'); // Paginated posts
// $content .= \Stratum\Component::render('pagination/single'); // Previous next post

echo $content;

get_footer();
