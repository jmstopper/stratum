wp.domReady(() => {
    const bannedBlockPrefixes = [
        'core/query',
        'core/post-',
        'core/site-',
        'core/term-',
        'core/comment',
        'core/navigation',
        'yoast',
    ];

    const bannedBlocks = [
        'core/archives',
        'core/avatar',
        'core/calendar',
        'core/categories',
        'core/columns',
        'core/column',
        'core/cover',
        'core/file',
        'core/group',
        'core/media-text',
        'core/latest-comments',
        'core/latest-posts',
        'core/more',
        'core/nextpage',
        'core/page-list',
        'core/pullquote',
        'core/rss',
        'core/read-more',
        'core/search',
        'core/social-links',
        'core/social-link',
        'core/spacer',
        'core/tag-cloud',
        'core/text-columns',
        'core/verse',
        'core/query',
        'core/loginout',
    ];

    const allBlocks = wp.blocks.getBlockTypes();
    const allowedBlocks = [];

    // -------------------------------------------------------------------------
    // Remove blocks
    // -------------------------------------------------------------------------
    for (const block of allBlocks) {
        let unregisterBlock = false;

        if (bannedBlocks.includes(block.name)) {
            // Check against the flat array
            unregisterBlock = true;
        } else {
            // Check against the prefix
            for (const prefix of bannedBlockPrefixes) {
                if (block.name.startsWith(prefix)) {
                    unregisterBlock = true;
                    break;
                }
            }
        }

        // Remove the block
        if (unregisterBlock === true) {
            wp.blocks.unregisterBlockType(block.name);
        } else {
            allowedBlocks.push(block);
        }
    }

    // -------------------------------------------------------------------------
    // Loop the allowed blocks and remove the block styles.
    // Maybe allow styles from custom blocks here. Prefix of ACF/
    // -------------------------------------------------------------------------
    for (const block of allowedBlocks) {
        for (const style of block.styles) {
            wp.blocks.unregisterBlockStyle(block.name, style.name);
        }
    }
});
