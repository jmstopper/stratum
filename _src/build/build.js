import gulp from 'gulp';
import componentsGeneral from './componentsGeneral.js';
import { componentsStylesCore, componentsStylesAncillary } from './componentsStyles.js';
import { componentsScriptsCore, componentsScriptsAncillary } from './componentsScripts.js';
import colors from './colors.js';
import clean from './clean.js';
import create from './create.js';
import report from './report.js';
import fonts from './fonts.js';
import images from './images.js';
import general from './general.js';
import svg from './svg.js';
import { styles } from './styles.js';
import { scripts } from './scripts.js';
import pot from './pot.js';
import manifest from './manifest.js';
import wordpressStylesheet from './wordpressStylesheet.js';
import themeJson from './theme-json.js';
import deleteEmpty from './delete-empty.js';

export default gulp.series(
    clean,
    create,
    gulp.parallel(
        colors,
        componentsStylesCore,
        componentsScriptsCore,
    ),
    gulp.parallel(
        fonts,
        general,
        images,
        svg,
        pot,
        wordpressStylesheet,
        themeJson,
        styles,
        scripts,
        componentsGeneral,
        componentsStylesAncillary,
        componentsScriptsAncillary,
    ),
    deleteEmpty,
    gulp.parallel(
        manifest,
        report,
    ),
);
