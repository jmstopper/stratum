/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import clearImport from 'clear';

export default function clear(cb) {
    clearImport();

    cb();
}
