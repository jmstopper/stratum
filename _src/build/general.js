/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import gulp from 'gulp';
import plumber from 'gulp-plumber';
import config from '../config/config.js';

export default function general() {
    return gulp.src(`${config.paths.base.src + config.paths.general.src}**/*`)
        .pipe(plumber())
        .pipe(gulp.dest(config.paths.base.dest + config.paths.general.dest));
}
