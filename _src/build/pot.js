/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import gulp from 'gulp';
import plumber from 'gulp-plumber';
import wpPot from 'gulp-wp-pot';
import config from '../config/config.js';

export default function pot() {
    const destination = `./${config.stratum.theme['Text Domain']}.pot`;

    return gulp.src(config.paths.php, { cwd: './' })
        .pipe(plumber())
        .pipe(wpPot({
            domain: config.stratum.theme['Text Domain'],
            package: config.stratum.theme['Text Domain'],
        }))
        .pipe(gulp.dest(destination));
}
