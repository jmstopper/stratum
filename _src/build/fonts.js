/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import gulp from 'gulp';
import plumber from 'gulp-plumber';
import config from '../config/config.js';

export default function fonts() {
    const destination = config.paths.base.dest + config.paths.fonts.dest;

    return gulp.src(`${config.paths.base.src + config.paths.fonts.src}**/*`)
        .pipe(plumber())
        .pipe(gulp.dest(destination));
}
