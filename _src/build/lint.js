import { stylesLint } from './styles.js';
import { componentsStylesLint } from './componentsStyles.js';
import { php } from './php.js';

export default function lint(cb) {
    stylesLint();
    componentsStylesLint();
    php();

    cb();
}
