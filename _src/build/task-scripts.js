/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import gulp from 'gulp';
import plumber from 'gulp-plumber';
import browserSync from 'browser-sync';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';
import named from 'vinyl-named-with-path';
import gulpif from 'gulp-if';
import rename from 'gulp-rename';
import webpackConfig from '../../webpack.config.js';
import rewriteComponents from './rewrite-components.js';

export default function taskScripts(src, dest, production = true) {
    return gulp.src(src)
        .pipe(gulpif(production !== true, plumber({
            errorHandler(err) {
                console.log(err.message);
                this.emit('end');
            },
        })))
        .pipe(named())
        .pipe(webpackStream(webpackConfig({
            production,
        }), webpack))
        .pipe(rename((mypath) => {
            // eslint-disable-next-line no-param-reassign
            mypath.dirname = rewriteComponents(mypath.dirname);
        }))
        .pipe(gulp.dest(dest))
        .pipe(browserSync.stream());
}
