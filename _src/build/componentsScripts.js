/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
import { globSync } from 'glob';
import { writeFile } from 'fs';
import config from '../config/config.js';
import taskScripts from './task-scripts.js';

const newline = '\r\n'; // New line code
const ancillarySrc = [
    `${config.paths.base.src + config.paths.components.src + config.paths.components.entry}*/*/*.js`,
    '!**/scripts.js',
    '!**/editor.js',
    '!**/_*.js',
    '!**/_**/*.*',
];

const ancillaryDest = config.paths.base.dest + config.paths.components.dest;

function componentsScriptsCore(cb) {
    // Build a base path for the glob
    const src = `${config.paths.base.src + config.paths.components.src}*/*/scripts.js`;

    // Build the ignores
    const ignore = [
        // Ignore the template directory
        `${config.paths.base.src + config.paths.components.src}_**/**.*`,

        // Ignore folders starting with an underscore
        `${config.paths.base.src + config.paths.components.src}**/_**/**.*`,
    ];

    // Get the files
    const files = globSync(src, {
        ignore,
    });

    // Placeholder for output content
    let content = '';

    if (files.length > 0) {
        const imports = files.map((file) => `import '${file.replace(config.paths.base.src + config.paths.components.src, './')}';${newline}`);

        content = imports.reduce((carry, line) => carry + line);
    }

    writeFile(
        `${config.paths.base.src + config.paths.components.src}_components.js`, // File to write to
        content, // Contents of file
        {}, // Options for the file
        (err) => { // Callback
            if (err) {
                throw err;
            }
        },
    );

    cb();
}

function componentsScriptsAncillary() {
    return taskScripts(ancillarySrc, ancillaryDest);
}

function componentsScriptsAncillaryDev() {
    return taskScripts(ancillarySrc, ancillaryDest, false);
}

export {
    componentsScriptsCore,
    componentsScriptsAncillary,
    componentsScriptsAncillaryDev,
};
