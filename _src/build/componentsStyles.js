/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import { writeFile } from 'fs';
import { globSync } from 'glob';
import config from '../config/config.js';
import { taskStyles, taskStylesLint, taskStylesFix } from './task-styles.js';

const newline = '\r\n'; // New line code

const ancillarySrc = [
    `${config.paths.base.src + config.paths.components.src + config.paths.components.entry}*/*/*.scss`,
    '!**/styles.scss',
    '!**/editor.scss',
    '!**/_*.scss',
    '!**/_**/*.*',
];

const ancillaryDest = config.paths.base.dest + config.paths.components.dest;

function componentsStylesCore(cb) {
    // Build the ignores
    const ignore = [
        // Ignore the template directory
        `${config.paths.base.src + config.paths.components.src}_**/**.*`,

        // Ignore folders starting with an underscore
        `${config.paths.base.src + config.paths.components.src}**/_**/**.*`,

        // Ignores scss files in a component directory
        '**/_*.*',
    ];

    // Get the files
    const files = [
        // Load scss files that are not contained in a folder
        ...globSync(`${config.paths.base.src + config.paths.components.src}*/*.scss`, {
            ignore,
        }),
        // Load styles.scss files that are in a folder
        ...globSync(`${config.paths.base.src + config.paths.components.src}*/*/styles.scss`, {
            ignore,
        }),
    ].sort();

    const editorFiles = [
        // Load scss files that are not contained in a folder
        ...globSync(`${config.paths.base.src + config.paths.components.src}*/editor.scss`, {
            ignore,
        }),
        // Load styles.scss files that are in a folder
        ...globSync(`${config.paths.base.src + config.paths.components.src}*/*/editor.scss`, {
            ignore,
        }),
    ].sort();

    // Placeholder for output content
    let content = '';
    let editorContent = '';

    // If there are files
    if (files.length > 0) {
        // Loop the files and generate the import statements
        // Find and replace the path to the components and make it relative
        // We could strip .scss here but thats just overhead on compile time
        const imports = files.map((file) => `@import '${file.replace(config.paths.base.src + config.paths.components.src, './')}';${newline}`);

        const editorImports = files.map((file) => {
            // Find and replace the path to the components and make it relative
            // We could strip .scss here but thats just overhead on compile time
            const filePath = `${file.replace(config.paths.base.src + config.paths.components.src, './')}';${newline}`;

            if (filePath.startsWith('./1-elements/')) {
                return '';
            }

            if (filePath.startsWith('./3-blocks/0-wordpress')) {
                return '';
            }

            return `@import '${file.replace(config.paths.base.src + config.paths.components.src, './')}';${newline}`;
        });

        // Reduce the array to a string
        content = imports.reduce((carry, line) => carry + line);

        editorContent = editorImports.reduce((carry, line) => carry + line);
    }

    if (editorFiles.length > 0) {
        // Loop the files and generate the import statements
        // Find and replace the path to the components and make it relative
        // We could strip .scss here but thats just overhead on compile time
        const editorImports = editorFiles.map((file) => `@import '${file.replace(config.paths.base.src + config.paths.components.src, './')}';${newline}`);

        editorContent += editorImports.reduce((carry, line) => carry + line);
    }

    // Write the output
    writeFile(
        `${config.paths.base.src + config.paths.components.src}_components.scss`, // File to write to
        content, // Contents of file
        {}, // Options for the file
        (err) => { // Callback
            if (err) {
                throw err;
            }
        },
    );

    writeFile(
        `${config.paths.base.src + config.paths.components.src}_components-editor.scss`, // File to write to
        editorContent, // Contents of file
        {}, // Options for the file
        (err) => { // Callback
            if (err) {
                throw err;
            }
        },
    );

    cb();
}

function componentsStylesAncillary() {
    return taskStyles(ancillarySrc, ancillaryDest);
}

function componentsStylesAncillaryDev() {
    return taskStyles(ancillarySrc, ancillaryDest, false);
}

function componentsStylesLint() {
    const src = `${config.paths.base.src + config.paths.components.src}[^_template]**/**/*.scss`;

    return taskStylesLint(src);
}

function componentsStylesFix() {
    const src = `${config.paths.base.src + config.paths.components.src}[^_template]**/**/*.scss`;
    const dest = config.paths.base.src + config.paths.components.src;

    return taskStylesFix(src, dest);
}

export {
    componentsStylesCore,
    componentsStylesAncillary,
    componentsStylesAncillaryDev,
    componentsStylesLint,
    componentsStylesFix,
};
