/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import gulp from 'gulp';
import plumber from 'gulp-plumber';
import browsersync from 'browser-sync';
import rename from 'gulp-rename';
import rewriteComponents from './rewrite-components.js';
import config from '../config/config.js';

export default function componentsGeneral() {
    return gulp.src([
        `${config.paths.base.src + config.paths.components.src + config.paths.components.entry}[^_]**/[^_]**/**`,
        '!**/*.scss',
        '!**/*.js',
        '!**/readme.md',
    ])
        .pipe(plumber())
        .pipe(rename((mypath) => {
            // eslint-disable-next-line no-param-reassign
            mypath.dirname = rewriteComponents(mypath.dirname);
        }))
        .pipe(gulp.dest(config.paths.base.dest + config.paths.components.dest))
        .pipe(browsersync.stream());
}
