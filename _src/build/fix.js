import { stylesFix } from './styles.js';
import { componentsStylesFix } from './componentsStyles.js';
import { phpFix } from './php.js';

export default function fix(cb) {
    stylesFix();
    componentsStylesFix();
    phpFix();

    cb();
}
