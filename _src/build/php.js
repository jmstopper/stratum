/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import gulp from 'gulp';
import phpcs from 'gulp-phpcs';
import phpcbf from 'gulp-phpcbf';
import config from '../config/config.js';

function php() {
    return gulp.src(config.paths.php)
        .pipe(phpcs({
            bin: 'vendor/bin/phpcbf',
        }))
        // Log all problems that was found
        .pipe(phpcs.reporter('log'));
}

function phpFix() {
    return gulp.src(config.paths.php)
        .pipe(phpcbf({
            bin: 'vendor/bin/phpcbf',
        }))
        // Outputs all files
        .pipe(gulp.dest('./'));
}

export {
    php,
    phpFix,
};

export default php;
