/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import { deleteAsync } from 'del';
import config from '../config/config.js';

export default function clean() {
    return deleteAsync([
        config.paths.base.dest,
    ]);
}
