/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import gulp from 'gulp';
import browsersync from 'browser-sync';
import config from '../config/config.js';
import componentsGeneral from './componentsGeneral.js';
import { componentsStylesCore, componentsStylesAncillaryDev, componentsStylesLint } from './componentsStyles.js';
import { componentsScriptsCore, componentsScriptsAncillaryDev } from './componentsScripts.js';
import colors from './colors.js';
import general from './general.js';
import manifest from './manifest.js';
import wordpressStylesheet from './wordpressStylesheet.js';
import themeJson from './theme-json.js';
import fonts from './fonts.js';
import images from './images.js';
import svg from './svg.js';
import { stylesDev, stylesLint } from './styles.js';
import { scriptsDev } from './scripts.js';
import clear from './clear.js';

export default function watch(cb) {
    // -------------------------------------------------------------------------
    // Styles
    // _src/styles/*.scss
    // -------------------------------------------------------------------------
    gulp.watch(
        `${config.paths.base.src + config.paths.styles.src}*.scss`,
        gulp.series(
            clear,
            gulp.series(
                stylesDev,
                manifest,
            ),
            stylesLint,
            componentsStylesLint,
        ),
    );

    // -------------------------------------------------------------------------
    // Scripts
    // _src/scripts/**/*.js
    // -------------------------------------------------------------------------
    gulp.watch(
        `${config.paths.base.src + config.paths.scripts.src}**/*.js`,
        gulp.series(
            clear,
            scriptsDev,
            manifest,
        ),
    );

    // -------------------------------------------------------------------------
    // General
    // _src/general/*.*
    // -------------------------------------------------------------------------
    gulp.watch(
        `${config.paths.general.src}**/*`,
        { cwd: config.paths.base.src },
        clear,
        general,
    );

    // -------------------------------------------------------------------------
    // Images
    // _src/images/**/*.*
    // -------------------------------------------------------------------------
    gulp.watch(
        `${config.paths.base.src + config.paths.images.src}**/*`,
        gulp.series(
            clear,
            images,
            manifest,
        ),
    );

    // -------------------------------------------------------------------------
    // Fonts
    // _src/fonts/**/*.*
    // -------------------------------------------------------------------------
    gulp.watch(
        `${config.paths.base.src + config.paths.fonts.src}**/*`,
        gulp.series(
            clear,
            fonts,
            manifest,
        ),
    );

    // -------------------------------------------------------------------------
    // SVGs
    // _src/images/**/*.svg
    // -------------------------------------------------------------------------
    gulp.watch(
        `${config.paths.base.src + config.paths.images.src}**/*.svg`,
        gulp.series(
            clear,
            svg,
            manifest,
        ),
    );

    // -------------------------------------------------------------------------
    // PHP Files
    // All php files excluding node_modues and vendor
    // -------------------------------------------------------------------------
    gulp.watch(
        config.paths.php,
        gulp.series(
            clear,
            gulp.parallel(
                wordpressStylesheet,
                () => {
                    browsersync.reload();
                },
            ),
        ),
    );

    // -------------------------------------------------------------------------
    // theme.json
    // _src/config/theme.json
    // -------------------------------------------------------------------------
    gulp.watch(
        `${config.paths.base.src}config/theme.json`,
        gulp.series(
            clear,
            themeJson,
        ),
    );

    // -------------------------------------------------------------------------
    // Color
    // _src/general/colors.json
    // -------------------------------------------------------------------------
    gulp.watch(
        `${config.paths.base.src}general/colors.json`,
        gulp.series(
            clear,
            colors,
        ),
    );

    // -------------------------------------------------------------------------
    // Components PHP
    // -------------------------------------------------------------------------
    gulp.watch(
        [
            `${config.paths.base.src + config.paths.components.src}**/*`,
            '!**/*.scss',
            '!**/*.js',
        ],
        gulp.series(
            clear,
            componentsGeneral,
            manifest,
        ),
    );

    // -------------------------------------------------------------------------
    // Components Styles
    // -------------------------------------------------------------------------
    gulp.watch(
        [
            `${config.paths.base.src + config.paths.components.src}**/*.scss`,
            `!${config.paths.base.src}${config.paths.components.src}*.scss`,
        ],
        gulp.series(
            clear,
            componentsStylesCore,
            gulp.parallel(
                stylesDev,
                componentsStylesAncillaryDev,
            ),
            gulp.parallel(
                manifest,
                stylesLint,
                componentsStylesLint,
            ),
        ),
    );

    // -------------------------------------------------------------------------
    // Components Scripts
    // -------------------------------------------------------------------------
    gulp.watch(
        [
            `${config.paths.base.src + config.paths.components.src}**/*.js`,
            `!${config.paths.base.src}${config.paths.components.src}*.js`,
        ],
        gulp.series(
            clear,
            componentsScriptsCore,
            gulp.parallel(
                scriptsDev,
                componentsScriptsAncillaryDev,
            ),
            manifest,
        ),
    );

    cb();
}
