/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import gulp from 'gulp';
import sizereport from 'gulp-sizereport';
import config from '../config/config.js';

export default function report() {
    return gulp.src([
        `${config.paths.base.dest}**/*`,
        '!**/group_*.json',
        '!**/*.php',
    ])
        .pipe(sizereport({
            gzip: true,
        }));
}
