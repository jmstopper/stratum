/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import config from '../config/config.js';
import { taskStyles, taskStylesLint, taskStylesFix } from './task-styles.js';

const src = config.paths.base.src + config.paths.styles.src + config.paths.styles.entry;
const dest = config.paths.base.dest + config.paths.styles.dest;

function styles() {
    return taskStyles(src, dest);
}

function stylesDev() {
    return taskStyles(src, dest, false);
}

function stylesLint() {
    return taskStylesLint(`${config.paths.base.src + config.paths.styles.src}**/*.scss`);
}

function stylesFix() {
    return taskStylesFix(
        `${config.paths.base.src + config.paths.styles.src}**/*.scss`,
        config.paths.base.src + config.paths.styles.src,
    );
}

export {
    styles,
    stylesDev,
    stylesLint,
    stylesFix,
};
