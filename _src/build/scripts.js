/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import config from '../config/config.js';
import taskScripts from './task-scripts.js';

const src = config.paths.base.src + config.paths.scripts.src + config.paths.scripts.entry;
const dest = config.paths.base.dest + config.paths.scripts.dest;

function scripts() {
    return taskScripts(src, dest);
}

function scriptsDev() {
    return taskScripts(src, dest, false);
}

export {
    scripts,
    scriptsDev,
};
