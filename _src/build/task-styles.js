/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import gulp from 'gulp';
import plumber from 'gulp-plumber';
import browsersync from 'browser-sync';
import * as dartSass from 'sass';
import gulpSass from 'gulp-sass';
import postcss from 'gulp-postcss';
import scssParser from 'postcss-scss';

import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import stylelint from 'stylelint';
import sourcemaps from 'gulp-sourcemaps';
import sortMediaQueries from 'postcss-sort-media-queries';
import gulpif from 'gulp-if';
import rename from 'gulp-rename';
import hash from 'gulp-hash-filename';
import config from '../config/config.js';
import rewriteComponents from './rewrite-components.js';

const sass = gulpSass(dartSass);

function taskStyles(src, dest, production = true) {
    return gulp.src(src)
        // If we are in dev add error handling to not crash gulp
        .pipe(gulpif(production !== true, plumber({
            errorHandler(err) {
                console.log(err.message);
                this.emit('end');
            },
        })))

        // If we are in dev, start source maps
        .pipe(gulpif(production !== true, sourcemaps.init()))
        .pipe(sass({
            includePaths: config.paths.includePaths,
        }))

        .pipe(postcss([
            autoprefixer(),
            sortMediaQueries(),
        ]))

        // If we are in production minify the code
        .pipe(gulpif(production === true, postcss([
            cssnano(),
        ])))

        // If we are in production rename the file to bust cache
        .pipe(gulpif(production === true, hash({
            format: '{name}.{hash}{ext}',
        })))

        // If we are in dev, output the source maps
        .pipe(gulpif(production !== true, sourcemaps.write('./')))
        .pipe(rename((mypath) => {
            // eslint-disable-next-line no-param-reassign
            mypath.dirname = rewriteComponents(mypath.dirname);
        }))
        .pipe(gulp.dest(dest))
        .pipe(browsersync.stream());
}

function taskStylesLint(src) {
    return gulp.src(src)
        .pipe(plumber())
        .pipe(postcss([
            stylelint(config.plugins.stylelint),
        ], {
            syntax: scssParser,
        }));
}

function taskStylesFix(src, dest) {
    config.plugins.stylelint.fix = true;

    return gulp.src(src)
        .pipe(plumber())
        .pipe(postcss([
            stylelint(config.plugins.stylelint),
        ], {
            syntax: scssParser,
        }))
        .pipe(gulp.dest(dest));
}

export {
    taskStyles,
    taskStylesLint,
    taskStylesFix,
};
