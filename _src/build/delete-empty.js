/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import fs from 'fs';
import path from 'path';
import config from '../config/config.js';

function deleteEmptyRecursive(folder) {
    const isDir = fs.statSync(folder).isDirectory();

    if (!isDir) {
        return;
    }

    let files = fs.readdirSync(folder);

    if (files.length > 0) {
        files.forEach((file) => {
            const fullPath = path.join(folder, file);

            deleteEmptyRecursive(fullPath);
        });

        // re-evaluate files; after deleting subfolder
        // we may have parent folder empty now
        files = fs.readdirSync(folder);
    }

    if (files.length === 0) {
        fs.rmdirSync(folder);
    }
}

export default function deleteEmpty(cb) {
    deleteEmptyRecursive(config.paths.base.dest + config.paths.components.dest);
    cb();
}
