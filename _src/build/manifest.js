/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
import fs from 'fs';
import path from 'path';
import config from '../config/config.js';

function* readAllFiles(dir) {
    const files = fs.readdirSync(dir, { withFileTypes: true });

    for (const file of files) {
        if (file.isDirectory()) {
            yield* readAllFiles(path.join(dir, file.name));
        } else {
            yield path.join(dir, file.name);
        }
    }
}

function stripChunkHash(name) {
    // if (name.includes('gutenberg') || name.endsWith('.asset.php')) {
    //     const parts = name.split('.');

    //     if (parts.length >= 4) {
    //         parts.splice(1, 1);
    //         return parts.join('.');
    //     }
    // } else

    if (name.endsWith('.css') || name.endsWith('.js') || name.endsWith('.mjs')) {
        const parts = name.split('.');

        if (parts.length > 2) {
            parts.splice(parts.length - 2, 1);
            return parts.join('.');
        }
    }

    return name;
}

export default function manifest(cb) {
    const fileStructure = {};

    for (const file of readAllFiles(config.paths.base.dest)) {
        const withoutAssets = file.slice(7);
        const withoutChunkHash = stripChunkHash(withoutAssets);

        if (withoutAssets !== withoutChunkHash) {
            fileStructure[withoutChunkHash] = withoutAssets;
        }
    }

    fs.writeFile(
        config.paths.base.dest + config.paths.manifest, // File to write to
        JSON.stringify(fileStructure, '', 4), // Contents of file
        {}, // Options for the file
        (err) => { // Callback
            if (err) {
                throw err;
            }
        },
    );

    cb();
}
