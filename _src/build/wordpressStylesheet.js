import { writeFile, readFileSync } from 'fs';

export default function wordpressStylesheet(cb) {
    const wordpress = JSON.parse(readFileSync('./stratum.json')).theme;
    const newline = '\r\n'; // New line code

    // Add a timestamp to the wordpress information
    wordpress.Version = Math.floor(Date.now() / 1000);

    let content = `/*${newline}`; // Open the comments

    // Loop through each of the wordpress keys and add them to the string
    for (const index in wordpress) {
        if (Object.prototype.hasOwnProperty.call(wordpress, index)) {
            content += `${index}: ${wordpress[index]}${newline}`;
        }
    }

    content += '*/'; // Close the comments

    writeFile(
        './style.css', // File to write to
        content, // Contents of file
        {}, // Options for the file
        (err) => { // Callback
            if (err) {
                throw err;
            }
        },
    );

    cb();
}
