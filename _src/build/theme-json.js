import { writeFile, readFileSync } from 'fs';

export default function themeJson(cb) {
    const theme = JSON.parse(readFileSync('./theme.json'));
    const { colors } = JSON.parse(readFileSync('./stratum.json'));

    theme.settings.color.palette = [];

    // Push the mapped colors in to the theme.json file
    for (const color of colors) {
        if (color.secondary !== undefined) {
            theme.settings.color.palette.push({
                name: color.name,
                slug: color.slug,
                color: color.color,
            });
        }
    }

    writeFile(
        './theme.json', // File to write to
        JSON.stringify(theme, null, 4), // Contents of file
        {}, // Options for the file
        (err) => { // Callback
            if (err) {
                throw err;
            }
        },
    );

    cb();
}
