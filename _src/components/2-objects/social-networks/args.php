<?php

add_filter('stratum/render/assets/components/social-networks', function ($args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'networks' => get_field('social_networks', 'option'),
    ]);

    if (!is_array($args['networks'])) {
        $args['networks'] = [];
    }

    $args['networks'] = array_map(function ($network) {
        $network['title'] = sprintf(__('%1$s %2$s account', 'stratum-text-domain'), get_bloginfo('name'), ucfirst($network['social_network']));
        $network['wrapper']['aria-label'] = sprintf(__('Visit %1$s on %2$s', 'stratum-text-domain'), get_bloginfo('name'), ucfirst($network['social_network']));

        return $network;
    }, $args['networks']);

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge([
        'social-networks',
    ], $args['classes']);

    return $args;
});
