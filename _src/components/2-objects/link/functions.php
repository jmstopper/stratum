<?php

namespace Stratum;

function link(mixed $args, $options = []): string
{
    return \Stratum\Component::render('link', $args, $options);
}
