<?php

add_filter('stratum/render/assets/components/link', function ($args, $options = []): array {

    if ($args instanceof \Stratum\Page\AbstractPage) {
        $args = [
            'url' => $args->url(),
            'content' => $args->title(),
        ];
    } elseif ($args instanceof \WP_Post) {
        $args = [
            'url' => get_permalink($args),
            'content' => get_the_title($args),
        ];
    } elseif ($args instanceof \WP_Term) {
        $args = [
            'url' => get_term_link($args),
            'content' => $args->name,
        ];
    }

    $args = wp_parse_args($args, [
        'el' => 'a',
        'url' => '',
        'content' => '',
        'target' => '',
        'classes' => [],
    ]);

    if (!empty($args['title'])) {
        $args['content'] = $args['title'];
        unset($args['title']);
    }

    if (!empty($args['url'])) {
        $args['wrapper']['href'] = $args['url'];
    }

    if (!empty($args['target'])) {
        $args['wrapper']['target'] = $args['target'];
    }

    if (!empty($args['wrapper']['target']) && $args['wrapper']['target'] === '_blank') {
        $args['wrapper']['rel'] = 'noopener noreferrer';
    }

    if (!empty($args['wrapper']['href'])) {
        if ($args['wrapper']['href'] === '#0') {
            $args['el'] = 'span';
            unset($args['wrapper']['href']);
        } elseif (\Stratum\isFileURL($args['wrapper']['href'])) {
            $args['wrapper']['download'] = true;
        } elseif (str_starts_with($args['wrapper']['href'], 'mailto:')) {
            unset($args['wrapper']['rel']);
            unset($args['wrapper']['target']);
        }
    } else {
        $args['el'] = 'span';
    }

    return $args;
}, 10, 2);
