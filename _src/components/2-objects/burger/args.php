<?php

add_filter('stratum/render/assets/components/burger', function (array $args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'content' => '',
    ]);

    $args['classes'] = array_merge([
        'burger',
    ], $args['classes']);

    return $args;
});
