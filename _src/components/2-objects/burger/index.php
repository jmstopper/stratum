<?php
/** @var mixed $args */
?><button <?= \Stratum\attributes($args); ?>>
    <span class="burger__line burger__line--1"></span>
    <span class="burger__line burger__line--2"></span>
    <?php if (!empty($args['content'])) {
        ?><span class="screen-reader-text"><?=
            wp_kses_post($args['content']);
?></span><?php
    } ?>
</button>
