<?php

// Disable the default gravity theme
// https://docs.gravityforms.com/gform_disable_form_theme_css/
add_filter('gform_disable_form_theme_css', '__return_true');

// Focus the confirmation after a submission
add_filter('gform_confirmation_anchor', '__return_true');

// Progress should be at 0 on first page of form
add_filter('gform_progressbar_start_at_zero', '__return_true');

// Move gravity forms inline js and css to footer
// Does not apply when using ajax
// https://docs.gravityforms.com/gform_init_scripts_footer/
add_filter('gform_init_scripts_footer', '__return_true');

// Make the gravity form button display on all tinymce editors
add_filter('gform_display_add_form_button', '__return_true');

// Disable gravity forms tabindex output
add_filter('gform_tabindex', '__return_false');

add_filter('gform_disable_view_counter', '__return_true');

/**
 * Gravity forms loves to add lots of rubbish CSS, lets disable it as we
 * are dealing with it in the theme
 * @return void
 */
add_action('gform_enqueue_scripts', function () {
    // Make sure were not on a gravity admin page
    // if (is_admin() && isset($_GET['gf_page'])) {
    //     return;
    // }

    wp_deregister_style('gform_basic');
    wp_script_add_data('gform_gravityforms', 'group', 1);
    wp_script_add_data('gform_json', 'group', 1);
}, 11);


add_filter('gform_pre_render', function (array $form): array {

    // Render line breaks in the form description
    $form['description'] = wpautop($form['description']);

    // Adds Field types as a class to the container
    foreach ($form['fields'] as $f => $field) {
        $form['fields'][$f]['cssClass'] .= ' gfield-type-' . $field['type'] . ' ';

        if ($field['type'] === 'html') {
            // Render line breaks in html fields
            $form['fields'][$f]['content'] = wpautop($form['fields'][$f]['content']);
            $form['fields'][$f]['cssClass'] .= ' nflm ';
        } elseif ($field['type'] === 'section') {
            $form['fields'][$f]['cssClass'] .= ' nflm ';
        }
    }

    return $form;
});

/**
 * This hack searches the output for <style></style> and removes it. We do this
 * because gravity loves to write super specific styling that we cant over-right
 * @param string $form_string
 * @return string
 */
add_filter('gformGetFormFilter', function (string $formString): string {
    return preg_replace(
        '/<\s*style.+?<\s*\/\s*style.*?>/si',
        ' ',
        $formString
    );
}, 10);

add_filter('gform_form_settings_initial_values', function ($initialValues) {
    $initialValues['labelPlacement'] = 'top_label';
    $initialValues['descriptionPlacement'] = 'above';
    $initialValues['subLabelPlacement'] = 'above';
    $initialValues['validationSummary'] = true;
    $initialValues['enableAnimation'] = false;
    $initialValues['enableHoneypot'] = true;

    return $initialValues;
}, 10, 1);

add_filter('gform_next_button', '\Stratum\GravityForms\submitToButton', 10, 2);
add_filter('gform_previous_button', '\Stratum\GravityForms\submitToButton', 10, 2);
add_filter('gform_submit_button', '\Stratum\GravityForms\submitToButton', 10, 2);

add_filter('stratum/assets/styles', function (array $assets): array {
    if (\Stratum\hasBlock('gravityforms/form')) {
        \Stratum\Component::loadAssets('gravity-forms');
    }

    return $assets;
});

add_filter('gform_get_form_filter', function ($formString) {
    return str_replace('class="gfield_required"', 'class="gfield_required" aria-hidden="true"', $formString);
}, 10);
