const items = document.querySelectorAll('.appear');

const active = (entries) => {
    entries.forEach((entry) => {
        if (entry.isIntersecting) {
            entry.target.classList.add('is-inview');
        } else {
            entry.target.classList.remove('is-inview');
        }
    });
};
const io = new IntersectionObserver(active);

items.forEach((item) => {
    io.observe(item);
});
