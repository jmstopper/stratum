<?php

add_filter('stratum/render/assets/components/tag-and-heading', function ($args, $options = []): array {

    $defaults = [
        'heading' => '',
        'tag' => '',
        'headingEl' => 'p',
        'tagEl' => 'p',
        'headingClasses' => [],
        'headingOnlyClasses' => [],
        'tag_is_semantic_heading' => false,
        '_bypass_heading_generation' => true,
        '_bypass_id' => true,
    ];

    foreach ($args as $key => $value) {
        if (!array_key_exists($key, $defaults)) {
            unset($args[$key]);
        }
    }

    $args['classes'] = [];
    $args = wp_parse_args($args, $defaults);

    $args['classes'] = array_merge([
        'tag-and-heading'
    ], $args['classes']);

    $args['headingClasses'] = array_merge([
        'tag-and-heading__heading'
    ], $args['headingClasses']);

    if (!empty($args['tag']) && $args['tag_is_semantic_heading'] === true) {
        $args['tagEl'] = !empty($args['headingEl']) ? $args['headingEl'] : 'p';
        $args['headingEl'] = 'p';
    }

    return $args;
}, 10, 2);
