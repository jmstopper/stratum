<?php if (!empty($args['tag'])) { ?>
    <div <?= \Stratum\attributes($args); ?>>
        <?php if (!empty($args['tag']) && $args['tag_is_semantic_heading'] === true) { ?>
            <<?= $args['tagEl']; ?> class="tag-and-heading__tag">
                <?= wp_kses_post($args['tag']); ?>
            </<?= $args['tagEl']; ?>>
        <?php } ?>

        <?php if (!empty($args['heading'])) { ?>
            <<?= $args['headingEl']; ?> class="<?= implode(' ', $args['headingClasses']); ?>">
                <?= wp_kses_post($args['heading']); ?>
            </<?= $args['headingEl']; ?>>
        <?php } ?>

        <?php if (!empty($args['tag']) && $args['tag_is_semantic_heading'] === false) { ?>
            <<?= $args['tagEl']; ?> class="tag-and-heading__tag">
                <?= wp_kses_post($args['tag']); ?>
            </<?= $args['tagEl']; ?>>
        <?php } ?>
    </div>
<?php } elseif (!empty($args['heading'])) { ?>
    <<?= $args['headingEl']; ?> class="<?= implode(' ', $args['headingOnlyClasses']); ?>">
        <?= wp_kses_post($args['heading']); ?>
    </<?= $args['headingEl']; ?>>
<?php }
