<?php

add_filter('stratum/render/assets/components/main', function ($args): array {

    if (stratum()->mainRendered) {
        throw new Exception('Stratum: Main tag already rendered');
    }

    // If we are lazy we just pass the content through as the args.
    // Lets alias the args to the content and life goes on
    if (!is_array($args)) {
        $args = [
            'content' => $args
        ];
    }

    $args = wp_parse_args($args, [
        'id' => 'content',
        'classes' => [],
        'content' => '',
    ]);

    $args['classes'] = array_merge([
        'main',
        // 'nflm'
    ], $args['classes']);

    if (is_singular()) {
        $lastBlock = stratum()->page->blocks()->last();

        if ($lastBlock instanceof \Stratum\Block) {
            if ($color = $lastBlock->color()) {
                $args['classes'][] = 'last-block-has-background';
                $args['classes'][] = 'last-block-has-' . $color . '-background-color';
            }

            if ($align = $lastBlock->align()) {
                $args['classes'][] = 'last-block-align' . $align;
            }
        }
    }

    // Tell stratum that the main tag has been rendered so we dont output it
    // twice. If we do this component will throw an exception
    stratum()->mainRendered = true;

    return $args;
});
