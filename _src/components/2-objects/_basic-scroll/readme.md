https://github.com/electerious/basicScroll

npm install basicscroll

Add the basic-scroll class to the elements you would like to fade in.
For other animations, you will need to setup new properties and new instances.
