import * as basicScroll from 'basicscroll';

const items = document.querySelectorAll('.basic-scroll');

items.forEach((item) => {
    const instance = basicScroll.create({
        elem: item,
        direct: true,
        from: 'top-bottom',
        to: 'top-middle',
        props: {
            '--opacity': {
                from: 0.01,
                to: 0.99,
            },
        },
    });

    instance.start();
});
