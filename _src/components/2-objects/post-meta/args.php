<?php

add_filter('stratum/render/assets/components/post-meta', function ($args): ?array {

    if ($args instanceof \Stratum\Page\Post) {
        $args = $args->get();
    }

    if ($args instanceof WP_Post) {
        $myPost = $args;

        $args = [
            'items' => []
        ];

        if (get_post_type($myPost) === 'post') {
            $author = get_userdata((int) $myPost->post_author)->display_name;
            $terms = get_the_terms($myPost, 'category');
            $published = get_the_date('', $myPost);

            if (!empty($terms)) {
                $args['items'][] = [
                    'type' => 'terms',
                    'content' => \Stratum\Component::render('link-list', [
                        'links' => $terms
                    ])
                ];
            }

            if (!empty($author)) {
                $markup = '<dl>';
                $markup .= '<dt class="screen-reader-text">'
                    . __('Author', 'stratum-text-domain')
                    . '</dt><dd>'
                    . sprintf(__('By % s'), $author)
                    . '</dd>';
                $markup .= '<dt class="screen-reader-text">'
                    . __('Published', 'stratum-text-domain')
                    . '</dt><dd>'
                    . $published
                    . '</dd>';
                $markup .= '</dl>';
                $args['items'][] = [
                    'type' => 'author',
                    'content' => $markup,
                ];
            } else {
                $args['items'][] = [
                    'type' => 'author',
                    'content' => '<p>' . $published . '</p>'
                ];
            }
        }
    }

    $args = wp_parse_args($args, [
        'classes' => [],
        'items' => [],
    ]);

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge([
        'post-meta',
    ], $args['classes']);

    return !empty($args['items']) ? $args : null;
});
