<?php
/** @var mixed $args */
?><div <?= \Stratum\attributes($args); ?>>
    <?php foreach ($args['items'] as $key => $item) { ?>
        <div class="post-meta__item nflm is-<?= $item['type']; ?>">
            <?= wp_kses_post($item['content']); ?>
        </div>
    <?php } ?>
</div>
