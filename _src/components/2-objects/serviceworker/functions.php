<?php

namespace Stratum\WordPress;

add_action('wp_login', function () {
    $source = \Stratum\Asset::path('components/serviceworker/serviceworker.js');
    $dest = ABSPATH . 'serviceworker.js';

    copy($source, $dest);
});

add_action('wp_footer', function () {
    if (!is_user_logged_in()) {
        echo \Stratum\Component::render('serviceworker');
    }
}, 999);
