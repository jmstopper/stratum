<?php

add_filter('stratum/render/assets/components/article', function ($args): array {

    // If we are lazy we just pass the content through as the args.
    // Lets alias the args to the content and life goes on
    if (!is_array($args)) {
        $args = [
            'content' => $args,
        ];
    }

    $args = wp_parse_args($args, [
        'el' => 'article',
        'classes' => [],
        'id' => 'post-' . get_the_ID(),
        'content' => '',
    ]);

    if (is_singular('page')) {
        $args['el'] = 'div';
    }

    $args['classes'] = array_merge([
        'article', 'nflm'
    ], get_post_class($args['classes']));

    return $args;
});
