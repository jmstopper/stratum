<?php

add_filter('stratum/render/assets/components/responsive-embed', function ($args): array {
    $args = wp_parse_args($args, [
        'classes' => [],
        'embed' => '',
        'ratio' => 'is-16-9',
    ]);

    $args['classes'] = array_merge([
        'responsive-embed', $args['ratio'],
    ], $args['classes']);

    return $args;
});
