import TabsAutomatic from './scripts/instance.js';

document.addEventListener('DOMContentLoaded', () => {
    const instances = document.querySelectorAll('[role=tablist].automatic');

    if (instances) {
        instances.forEach((instance) => {
            new TabsAutomatic(instance);
        });
    }
});
