<?php

add_filter('stratum/render/assets/components/social-icons', function ($args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'networks' => []
    ]);

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge($args['classes'], [
        'social-icons',
    ]);

    // Map the networks to links
    $args['links'] = array_map(function ($network) {
        $network['wrapper']['title'] = $network['title'];
        $network['title'] = \Stratum\svg('networks/' . $network['social_network'], [
            'wrapper' => [
                'width' => 32,
                'height' => 32
            ]
        ]);
        $network['classes'][] = 'round-button';

        return $network;
    }, $args['networks']);

    // unset the networks
    unset($args['networks']);

    return $args;
});
