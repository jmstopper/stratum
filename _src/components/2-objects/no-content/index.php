<?php

echo \Stratum\Component::render('wysiwyg', [
    'heading' => stratum()->page->title(),
    'content' =>
    wpautop(__('It appears you have found a dead end', 'stratum-text-domain'))
]);
