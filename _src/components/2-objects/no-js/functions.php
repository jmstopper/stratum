<?php

add_action('wp_head', function () {
    echo \Stratum\Component::render('no-js');
}, 5);

add_filter('stratum/html-attributes', function (array $attributes) {
    $attributes['class'][] = 'no-js';

    return $attributes;
});
