<?php
/** @var mixed $args */
?><<?= $args['element']; ?> <?= \Stratum\attributes($args); ?>>
    <div class="c-card__outer">

        <?php if (!empty($args['heading'])) { ?>
            <<?= $args['headingEl']; ?> class="c-card__heading h3"><?=
                $args['heading'];
            ?></<?= $args['headingEl']; ?>>
        <?php } ?>

        <?php if (!empty($args['meta'])) { ?>
            <div class="c-card__meta nflm">
                <?= wp_kses_post($args['meta']); ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['content'])) { ?>
            <div class="c-card__content nflm">
                <?= wp_kses_post($args['content']); ?>
            </div>
        <?php } ?>

    </div>

    <?php if (!empty($args['media'])) { ?>
        <div class="c-card__media objfit hideable-media">
            <?= wp_kses_post($args['media']); ?>
        </div>
    <?php } ?>
</<?= $args['element']; ?>>
