<?php

add_filter('stratum/render/assets/components/card', function ($args, $options): array {

    if ($args instanceof \WP_Post) {
        $sPost = \Stratum\Page\Post::seed($args);

        $args = [
            'element' => 'article',
            'heading' => \Stratum\link($sPost),
            'content' => $sPost->excerpt(),
            'image' => $sPost->hasImage() ? ['id' => $sPost->imageID()] : '',
            'classes' => ['is-' . $sPost->type(), 'has-link'],
            'meta' => \Stratum\Component::render('post-meta', $sPost),
        ];
    }

    $args = wp_parse_args($args, [
        'classes' => [],
        'element' => 'div',
        'headingEl' => 'div',
        'heading' => '',
        'meta' => '',
        'content' => '',
        'media' => '',
    ]);

    $args['classes'] = array_merge([
        'c-card',
    ], $args['classes']);

    if (!empty($args['image']['id'])) {
        $args['media'] = wp_get_attachment_image($args['image']['id'], 'medium_large', false, [
            'sizes' => '(max-width: 1023px) 266px, 380px'
        ]);
        unset($args['image']);
    }

    if (!str_contains($args['heading'], 'href="') && str_contains($args['content'], 'href=')) {
        $args['classes'][] = 'has-content-link';
    }

    return $args;
}, 10, 2);
