<?php

add_filter('stratum/render/assets/components/social-share', function ($args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'title' => '',
        'url' => '',
        'networks' => [],
    ]);

    // Twitter
    $args['networks'][] = [
        'social_network' => 'twitter',
        'url' => 'https://twitter.com/share?' . http_build_query([
            'url' => $args['url'],
        ]),
    ];

    // Facebook
    $args['networks'][] = [
        'social_network' => 'facebook',
        'url' => 'https://www.facebook.com/sharer/sharer.php?' . http_build_query([
            'u' => $args['url'],
        ]),
    ];

    // Linkedin
    $args['networks'][] = [
        'social_network' => 'linkedin',
        'url' => 'https://www.linkedin.com/sharing/share-offsite/?' . http_build_query([
            'url' => $args['url'],
        ]),
    ];

    // Add default properties to all networks
    $args['networks'] = array_map(function ($network) {
        $network['title'] = sprintf(__('Share on %s', 'stratum-text-domain'), $network['social_network']);
        $network['target'] = '_blank';

        return $network;
    }, $args['networks']);

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge([
        'social-share',
    ], $args['classes']);

    return $args;
});
