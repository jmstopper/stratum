<?php

add_filter('stratum/render/assets/components/search-form', function ($args): array {

    $args = wp_parse_args($args, [
        'form' => get_search_form([
            'echo' => false,
        ]),
    ]);

    return $args;
});
