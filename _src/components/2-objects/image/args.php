<?php

add_filter('stratum/render/assets/components/image', function ($args): array {
    $args = wp_parse_args($args, [
        'file' => '',
        'classes' => [],
        'wrapper' => [],
        'loading' => 'lazy',
        'alt' => '',
    ]);

    // Drop the src tag in
    $args['wrapper']['src'] = \Stratum\Asset::url('images/' . $args['file']);
    unset($args['file']);

    if (!empty($args['alt'])) {
        $args['wrapper']['alt'] = $args['alt'];
        unset($args['alt']);
    } else {
        // https://validator.w3.org/ says to not have the role and have an empty
        // alt tag.
        // $args['wrapper']['role'] = 'presentation';
        $args['wrapper']['alt'] = '';
        unset($args['alt']);
    }

    if ($args['loading'] !== false) {
        $args['wrapper']['loading'] = $args['loading'];
    } else {
        unset($args['loading']);
    }

    if (!empty($args['width'])) {
        $args['wrapper']['width'] = $args['width'];
        unset($args['width']);
    }

    if (!empty($args['height'])) {
        $args['wrapper']['height'] = $args['height'];
        unset($args['height']);
    }

    return $args;
});
