<?php

namespace Stratum;

function image(string $name, array $args = []): string
{
    return \Stratum\Component::render('image', wp_parse_args($args, [
        'file' => $name,
    ]));
}
