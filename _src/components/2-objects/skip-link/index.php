<?php

/** @var mixed $args */
?><?= \Stratum\link([
    'url' => $args['target'],
    'title' => $args['title'],
    'classes' => [
        'screen-reader-text'
    ]
]);
