<?php

add_filter('stratum/render/assets/components/accordion', function (array $args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'items' => [],
        'name' => \Stratum\uniqueID('accordion'),
    ]);

    $args['classes'] = array_merge([
        'accordion',
    ], $args['classes']);

    $args['items'] = array_map(function ($item) use ($args) {
        if ($item instanceof WP_Post) {
            $item = [
                'heading' => get_the_title($item),
                'content' => apply_filters('s_content', get_the_content(null, false, $item))
            ];
        }

        $item['wrapper']['name'] = $args['name'];

        return $item;
    }, $args['items']);

    return $args;
});

add_filter('stratum/render/assets/components/accordion/item', function (array $args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'heading' => '',
        'content' => '',
        '_bypass_id' => true,
        '_bypass_heading_generation' => true,
    ]);

    $args['classes'] = array_merge([
        'accordion__item', 'nflm'
    ], $args['classes']);

    return $args;
});
