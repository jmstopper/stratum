<?php

/** @var mixed $args */
?><details <?= \Stratum\attributes($args); ?>>
    <summary class="accordion__item-heading">
        <?= wp_kses_post($args['heading']); ?>
    </summary>
    <div class="accordion__item-content nflm">
        <?= wp_kses_post($args['content']); ?>
    </div>
</details>
