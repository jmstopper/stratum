<?php

/** @var mixed $args */
?><div <?= \Stratum\attributes($args); ?>>
    <?php foreach ($args['items'] as $key => $item) {
        echo \Stratum\Component::render('accordion/item', $item);
    } ?>
</div>