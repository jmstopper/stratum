<?php

add_filter('stratum/render/assets/components/table', function ($args, $options, $adjunct): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'label' => '',
        'rows' => [],
        'sortable' => false,
        'tableClasses' => []
    ]);

    $args['wrapper']['role'] = 'region';
    $args['wrapper']['tabindex'] = 0;

    if (!empty($args['label'])) {
        $args['wrapper']['aria-label'] = $args['label'];
    }

    if ($args['sortable'] === true) {
        $args['tableClasses'][] = 'sortable';
    }

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge([
        'table', 'nflm', 'scrollable-table'
    ], $args['classes']);

    return $args;
}, 10, 3);
