<?php
/** @var mixed $args */
?><div <?= \Stratum\attributes($args); ?>>
    <table class="<?= implode(' ', $args['tableClasses']); ?>">
        <thead><tr>
            <?php foreach ($args['rows'][0] as $column) {
                $thClasses = [];

                if ($column['sortable'] === true && $column['type'] === 'number') {
                    $thClasses[] = 'num';
                }
                ?><th class="<?= implode(' ', $thClasses); ?>">
                    <?php if ($column['sortable'] === true) { ?>
                        <button>
                    <?php } ?>
                        <?= $column['name']; ?>
                    <?php if ($column['sortable'] === true) { ?>
                        </button>
                    <?php } ?>
                </th>
            <?php } ?>
        </tr></thead>
        <tbody>
            <?php foreach ($args['rows'] as $row) { ?>
                <tr>
                    <?php foreach ($row as $column) {
                        $tag = 'td';

                        if (!empty($column['value'])) {
                            $tag .= ' data-value="' . esc_attr($column['value']) . '"';
                        }
                        ?><<?= $tag; ?>>
                            <?= $column['label']; ?>
                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
