<?php

/** @var mixed $args */
?><<?= $args['el']; ?> <?= \Stratum\attributes($args); ?>>

    <?php if (!empty($args['heading'])) { ?>
        <<?= $args['headingEl']; ?> class="<?= implode(' ', $args['headingClasses']); ?>"><?= $args['heading']; ?></<?= $args['headingEl']; ?>>
    <?php } ?>

    <?php if (count($args['links']) > 1) { ?>
        <<?= $args['type']; ?> class="link-list__items">
            <?php foreach ($args['links'] as $link) {
                ?><li><?= is_string($link) ? $link : \Stratum\link($link); ?></li><?php
            } ?>
        </<?= $args['type']; ?>>
    <?php } else {
        echo is_string($args['links'][0])
            ? $args['links'][0]
            : \Stratum\link($args['links'][0]);
    } ?>
</<?= $args['el']; ?>>
