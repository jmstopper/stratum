<?php

add_filter('stratum/render/assets/components/link-list', function (array $args): ?array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'links' => [],
        'type' => 'ul',
        'el' => 'div',
        'buttons' => false,
        'headingClasses' => [],
    ]);

    // Early return in case there are no links
    if (empty($args['links'])) {
        return null;
    }

    $args['classes'] = array_merge([
        'link-list', 'nflm'
    ], $args['classes']);

    $args['headingClasses'] = array_merge([
        'link-list__heading'
    ], $args['headingClasses']);

    // ACF repeaters store the link array data on a sub array, so if thats true
    // lets move it to the root
    $args['links'] = array_map(
        fn ($link) => is_array($link) && !empty($link['link']) ? $link['link'] : $link,
        $args['links']
    );

    // If any of the links are empty lets delete them
    $args['links'] = array_filter(
        $args['links'],
        fn ($link) => !empty($link)
    );

    // Late return incase all the links were filtered out
    if (empty($args['links'])) {
        return null;
    }

    if ($args['buttons'] === true) {
        $args['links'] = array_map(function ($link) {
            if (!array_key_exists('classes', $link) || !is_array($link['classes'])) {
                $link['classes'] = [];
            }

            $link['classes'][] = 'btn';

            return $link;
        }, $args['links']);
    }

    $args['links'] = array_values($args['links']);

    return $args;
});
