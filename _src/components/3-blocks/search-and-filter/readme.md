Place this where you want to use this component

global $wp_query;

$content .= \Stratum\Component::render('search-and-filter', [
    'query' => $wp_query,
]);
