<?php

namespace Stratum\SearchAndFilter;

function processTax(array $tax): array
{
    $query = [
        'relation' => 'AND'
    ];

    foreach ($tax as $taxonomy => $terms) {
        $query[] = [
            'taxonomy' => $taxonomy,
            'field' => 'slug',
            'terms' => $terms,
        ];
    }

    return $query;
}
