<?php

add_filter('acf/load_field/name=sf_post_type', function ($field) {

    $postTypes = get_post_types([
        'public'   => true,
        // '_builtin' => false
    ], 'objects');

    $blacklist = apply_filters('stratum/search-and-filter/post-type-blacklist', ['attachment']);

    if (count($blacklist) > 0) {
        $postTypes = array_filter(
            $postTypes,
            fn ($postType) => !in_array($postType->name, $blacklist)
        );
    }

    $field['choices'] = [];

    foreach ($postTypes as $postType) {
        $field['choices'][$postType->name] = $postType->label;
    }

    return $field;
});
