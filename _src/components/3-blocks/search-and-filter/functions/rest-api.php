<?php

namespace Stratum\SearchAndFilter;

add_action('rest_api_init', function () {
    register_rest_route('stratum/v1', '/search-and-filter/', [
        'methods' => 'GET',
        'callback' => '\Stratum\SearchAndFilter\restHandler',
        'permission_callback' => '__return_true',
    ]);
});

function restHandler($request)
{
    $params = $request->get_params();

    // -------------------------------------------------------------------------
    // Pull the config - decode an associative array instead of an object
    // as thats what the partials expect
    // -------------------------------------------------------------------------
    $config = json_decode($params['config'], true);

    // -------------------------------------------------------------------------
    // Default args for rendering partials
    // -------------------------------------------------------------------------
    $args = [
        'config' => $config,
        'id' => $config['id'],
        'url' => $params['url'],
        'component' => $params['component'],
        'components' => [
            'card-grid',
        ],
    ];

    // -------------------------------------------------------------------------
    // Remove the params that are needed for getting to this point and not
    // needed for the query. The only params left should be from the form data
    // -------------------------------------------------------------------------
    unset($params['action']);
    unset($params['url']);
    unset($params['config']);

    // Run a wp query against the args
    $args['query'] = new \WP_Query(
        \Stratum\SearchAndFilter\args($params)
    );

    // Build the response
    $response = [
        'partials' => [
            'message' => \Stratum\SearchAndFilter\message($args),
            'results' => \Stratum\Component::render('search-and-filter/partials/results', $args),
            'controls' => \Stratum\Component::render('search-and-filter/partials/controls', $args),
            'pagination' => \Stratum\Component::render('search-and-filter/partials/pagination', $args),
        ],
        'numbers' => [
            'results' => $args['query']->found_posts,
            'pages' => $args['query']->max_num_pages,
            'currentPage' => $args['query']->query_vars['paged'],
            'perPage' => $args['query']->post_count,
        ],
    ];

    if (array_key_exists('debug', $config) && $config['debug']) {
        $response['debug'] = [
            'rest_params' => $request->get_params(),
            'WP_Query' => $args['query'],
        ];
    }

    // Ship it
    return new \WP_REST_Response($response, 200);
}
