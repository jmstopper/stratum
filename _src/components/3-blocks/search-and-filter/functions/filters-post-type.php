<?php

namespace Stratum\SearchAndFilter;

function postTypeFilter(array $args): array
{
    $whitelist = $args['config']['post_type'];
    $blacklist = \apply_filters('stratum/search-and-filter/post-type-blacklist', ['attachment']);

    $postTypes = \get_post_types([
        'public' => true,
    ], 'objects');

    if (count($blacklist) > 0) {
        $postTypes = \array_filter(
            $postTypes,
            fn ($postType) => !in_array($postType->name, $blacklist)
        );
    }

    if (count($whitelist) > 0) {
        $postTypes = \array_filter(
            $postTypes,
            fn ($postType) => \in_array($postType->name, $whitelist)
        );
    }

    $args['filters']['post_type'] = [
        'name' => '_pt[]',
        'classes' => ['search-and-filter__group'],
        'label' => __('Type', 'stratum-text-domain'),
        'type' => 'checkbox',
        'options' => \array_map(function ($postType) use ($args) {
            return [
                'label' => $postType->label,
                'value' => $postType->name,
                'checked' => \Stratum\SearchAndFilter\postTypeActive($postType->name, $args),
                'taxonomies' => \get_object_taxonomies($postType->name),
            ];
        }, $postTypes),
    ];

    return $args;
}

function postTypeActive(string $postType, array $args): bool
{
    return \in_array($postType, $args['config']['post_type']) ||
        (
            \is_array($args['query']->query_vars['post_type']) &&
            \in_array(
                $postType,
                $args['query']->query_vars['post_type']
            )
        );
}

function activePostTypes($postTypes): array
{
    $postTypes = \array_filter($postTypes['options'], function ($postType) {
        return $postType['checked'] === true;
    });

    return \array_values(\array_map(function ($postType) {
        return $postType['value'];
    }, $postTypes));
}
