<?php

namespace Stratum\SearchAndFilter;

function filters($args)
{
    $args = \Stratum\SearchAndFilter\postTypeFilter($args);
    $args = \Stratum\SearchAndFilter\taxonomyFilter($args);
    $args = \Stratum\SearchAndFilter\removeRedundantFilters($args);
    $args = \Stratum\SearchAndFilter\hideSingleFilters($args);

    return $args['filters'];
}

function hideSingleFilters($args)
{
    $activeTaxonomies = activeTaxonomies($args['filters']);

    if (!empty($args['config']['post_type'])) {
        $args['filters']['post_type']['wrapper']['hidden'] = true;
    }

    if (!empty($args['config']['taxonomy'])) {
        foreach ($args['config']['taxonomy'] as $slug => $terms) {
            $args['filters'][$slug]['wrapper']['hidden'] = true;
        }
    }

    if (!empty($activeTaxonomies)) {
        $postTypeOptions = array_filter($args['filters']['post_type']['options'], function ($postType) use ($activeTaxonomies) {
            return count(array_intersect($postType['taxonomies'], $activeTaxonomies)) > 0;
        });

        if (count($postTypeOptions) < 2) {
            $args['filters']['post_type']['wrapper']['hidden'] = true;
        }
    }

    return $args;
}

function removeRedundantFilters(array $args): array
{
    $activePostTypes = activePostTypes($args['filters']['post_type']);

    if (!empty($activePostTypes)) {
        foreach ($args['filters'] as $key => $filter) {
            if ($key === 'post_type') {
                continue;
            }

            if (count(array_intersect($activePostTypes, $filter['post_types'])) === 0) {
                unset($args['filters'][$key]);
            }
        }
    }

    return $args;
}
