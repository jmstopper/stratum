<?php

namespace Stratum\SearchAndFilter;

function message(array $args)
{
    $query = $args['query'];

    $pages = $query->max_num_pages;
    $page = array_key_exists('paged', $query->query_vars) && $query->query_vars['paged'] !== 0
        ? $query->query_vars['paged']
        : 1;

    $message = __('No results found', 'stratum-text-domain');

    if ($query->found_posts > 0) {
        $message =  sprintf(
            __('%d results found. Showing page %s of %s.', 'stratum-text-domain'),
            $query->found_posts,
            $page,
            $pages
        );
    }

    return $message;
}
