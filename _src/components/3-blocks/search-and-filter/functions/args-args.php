<?php

namespace Stratum\SearchAndFilter;

function args(array $params): array
{
    if (!empty($params['_pt'])) {
        $params['post_type'] = $params['_pt'];
        unset($params['_pt']);
    }

    if (!empty($params['_s'])) {
        $params['s'] = $params['_s'];
        unset($params['_s']);
    }

    if (!empty($params['_tax'])) {
        $params['tax_query'] = processTax($params['_tax']);
        unset($params['_tax']);
    }

    if (!empty($params['_paged'])) {
        $params['paged'] = $params['_paged'];
        unset($params['_paged']);
    }

    if (!empty($params['_order'])) {
        $params['orderby'] = processOrder($params['_order']);

        unset($params['_order']);
    }

    return wp_parse_args($params, [
        'search-and-filter-query' => true,
        // ACF adds acf-disabled to search queries which results in the result
        // order changing due to the broken query. We have two choices. Remove
        // acf-disabled from the query or add it to this custom query.
        'post_status' => 'publish',
    ]);
}
