<?php

namespace Stratum\SearchAndFilter;

function processOrder(string $orderString): array
{
    $order = str_starts_with($orderString, 'asc') ? 'ASC' : 'DESC';
    $orderby = substr($orderString, strlen($order) + 1);

    return [
        $orderby => $order
    ];
}
