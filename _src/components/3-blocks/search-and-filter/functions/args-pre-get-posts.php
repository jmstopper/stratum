<?php

namespace Stratum\SearchAndFilter;

add_action('pre_get_posts', function ($query) {
    // Dont want to do anything in the admin area
    // https://developer.wordpress.org/reference/functions/is_archive/
    if (!is_admin() && $query->is_archive() && $query->is_main_query()) {
        // Need to rewrite this comment....
        // If we use the default ?s we run in to a conflict when the component
        // is used on pages and archives if the link is shared. The query string
        // on the link can cause wordpress to resolve the main query incorrectly
        if (!empty($_REQUEST['_s'])) {
            $query->set('s', $_REQUEST['_s']);
        }

        // WordPress does not have a query_var for post type so lets add one
        if (!empty($_REQUEST['_pt'])) {
            $query->set('post_type', $_REQUEST['_pt']);
        }

        if (!empty($_REQUEST['_paged'])) {
            $query->set('paged', $_REQUEST['_paged']);
        }

        // WordPress supports an order query by default but it doesnt support
        // the structure we have so lets modify
        if (!empty($_REQUEST['_order']) && str_contains($_REQUEST['_order'], '-')) {
            $query->set('orderby', processOrder($_REQUEST['_order']));
            $query->set('order', null);
        }

        // When not filtering with AJAX, we need to check for the tax param and
        // add the taxonomy query as necessary
        if (!empty($_REQUEST['_tax'])) {
            $query->set('tax_query', processTax($_REQUEST['_tax']));
        }
    }
});
