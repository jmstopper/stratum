<?php

namespace Stratum\SearchAndFilter;

function taxonomyFilter(array $args): array
{
    foreach ($args['filters']['post_type']['options'] as $postType) {
        $taxonomies = get_object_taxonomies($postType['value'], 'objects');

        foreach ($taxonomies as $taxonomy) {
            if (empty($args['filters'][$taxonomy->name])) {
                $terms = get_terms([
                    'taxonomy' => $taxonomy->name,
                ]);

                if (!empty($terms)) {
                    $args['filters'][$taxonomy->name] = [
                        'name' => '_tax[' . $taxonomy->name . '][]',
                        'classes' => ['search-and-filter__group'],
                        'label' => $taxonomy->label,
                        'type' => 'checkbox',
                        'post_types' => [$postType['value']],
                        'options' => array_map(function ($term) use ($args) {
                            return [
                                'label' => $term->name,
                                'value' => $term->slug,
                                'checked' => \Stratum\SearchAndFilter\termActive($term, $args),
                            ];
                        }, $terms),
                    ];
                }
            } else {
                $args['filters'][$taxonomy->name]['post_types'][] = $postType['value'];
            }
        }
    }

    return $args;
}

function termActive($term, $args): bool
{
    return !empty($args['query']->tax_query->queried_terms[$term->taxonomy]['terms'])
        && in_array($term->slug, $args['query']->tax_query->queried_terms[$term->taxonomy]['terms']);

    // // Is there a taxonomy query to bother checking
    // if (!empty($args['query']->query_vars['tax_query']) && is_array($args['query']->query_vars['tax_query'])) {
    //     foreach ($args['query']->query_vars['tax_query'] as $taxQuery) {
    //         // The tax_query can include a string so lets check for the property
    //         // we need.
    //         if (!empty($taxQuery['taxonomy'])) {
    //             // Does the taxonomy match
    //             if ($taxQuery['taxonomy'] === $term->taxonomy) {
    //                 // Have we queried the term
    //                 if (\in_array($term->slug, $taxQuery['terms'])) {
    //                     return true;
    //                 }
    //             }
    //         }
    //     }
    // }

    // return false;
}

function activeTaxonomies($taxonomies)
{
    unset($taxonomies['post_type']);

    $activeTaxonomies = array_filter($taxonomies, function ($taxonomy) {
        return count(array_filter($taxonomy['options'], function ($term) {
            return $term['checked'];
        })) > 0;
    });

    return array_keys($activeTaxonomies);
}
