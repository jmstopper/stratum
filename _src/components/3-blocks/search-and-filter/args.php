<?php

add_filter('stratum/render/assets/components/search-and-filter', function ($args): array {
    $args = wp_parse_args($args, [
        'classes' => [],
        'heading' => '',
        'content' => '',
        'query' => null,
        'config' => [
            'debug' => \Stratum\isDebug(),
            'live_filtering' => false,
            'is_search' => is_search(),
            'post_type' => [],
            'taxonomy' => [],
            'request_on_init' => is_singular(),
        ],
        'url' => \Stratum\currentURL(), // Used for pagination
        'id' => \Stratum\uniqueID('search-and-filter'),
        'message' => '',
        'component' => 'card-grid',
        'components' => \Stratum\Components::supportPosts(),
        // ---------------------------------------------------------------------
        // Fields from ACF
        // ---------------------------------------------------------------------
        'lock_post_type' => false
    ]);

    // -------------------------------------------------------------------------
    // If there is no WP_Query to work with, lets make one
    // -------------------------------------------------------------------------
    if (!$args['query'] instanceof \WP_Query) {
        $queryArgs = [];

        if (!empty($args['sf_post_type'])) {
            $queryArgs['post_type'] = $args['sf_post_type'];

            if ($args['lock_post_type']) {
                $args['config']['post_type'] = $args['sf_post_type'];
            }
        }

        // Build the WP_Query
        $args['query'] = new \WP_Query($queryArgs);
    }

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge([
        'search-and-filter', 'block'
    ], $args['classes']);

    // -------------------------------------------------------------------------
    // Attempt to introduce logic for archive page
    // -------------------------------------------------------------------------
    if (stratum()->page instanceof \Stratum\Page\PostType) {
        $args['config']['post_type'][] = stratum()->page->type();
    } elseif (stratum()->page instanceof \Stratum\Page\Term) {
        $args['config']['taxonomy'][stratum()->page->taxonomy()] = [stratum()->page->id()];
    }

    // -------------------------------------------------------------------------
    // Pull the ID in to the config data. Simpler javascript code
    // -------------------------------------------------------------------------
    $args['config']['id'] = $args['id'];
    // -------------------------------------------------------------------------
    // Push the config on to the wrapper as a data attribute
    // -------------------------------------------------------------------------
    $args['wrapper']['data-config'] = $args['config'];
    $args['message'] = \Stratum\SearchAndFilter\message($args);

    return $args;
});
