<?php

/** @var mixed $args */

echo \Stratum\Component::render($args['component'], [
    'items' => $args['query']->posts,
]);
