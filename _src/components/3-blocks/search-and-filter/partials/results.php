<?= !empty($args['query']->posts)
    ? \Stratum\Component::render('search-and-filter/partials/loop', $args)
    : \Stratum\Component::render('search-and-filter/partials/nothing'); ?>
