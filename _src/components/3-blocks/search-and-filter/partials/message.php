<?php

/** @var mixed $args */

$classes = ['search-and-filter__message'];

if ((int) $args['query']->found_posts === 0) {
    $classes[] = 'screen-reader-text';
}

$attributes = [
    'classes' => $classes,
    'wrapper' => [
        'tabindex' => '-1',
        'aria-live' => 'polite',
        'aria-atomic' => 'true',
    ],
];

?><div <?= \Stratum\attributes($attributes); ?>><?= $args['message']; ?></div>