<?php

/** @var mixed $args */

$searchID = $args['id'] . '-' . 'search';
$value = $args['query']->query_vars['s'] ?? '';
$name = $args['config']['is_search'] === true ? 's' : '_s';

?><div class="search-and-filter__group">
    <label for="<?= $searchID; ?>"><?= __('Search', 'stratum-text-domain'); ?></label>
    <input id="<?= $searchID; ?>" type="text" name="<?= $name; ?>" value="<?= esc_attr($value); ?>">
</div>