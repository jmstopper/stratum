<?php

/** @var mixed $args */

$paged = $args['query']->query_vars['paged'] !== 0
    ? $args['query']->query_vars['paged']
    : 1;

$type = $args['config']['debug']
    ? 'text'
    : 'hidden';

?>
<div class="search-and-filter__footer">
    <input name="_paged" type="<?= $type; ?>" value="<?= esc_attr($paged); ?>" class="search-and-filter__paged">
    <input class="search-and-filter__submit" type="submit">
</div>