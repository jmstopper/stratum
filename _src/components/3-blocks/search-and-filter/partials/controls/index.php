<?php

/** @var mixed $args */

echo \Stratum\Component::render('search-and-filter/partials/controls/search', $args);
echo \Stratum\Component::render('search-and-filter/partials/controls/filters', $args);
echo \Stratum\Component::render('search-and-filter/partials/controls/sort', $args);
echo \Stratum\Component::render('search-and-filter/partials/controls/presentation', $args);
echo \Stratum\Component::render('search-and-filter/partials/controls/submit', $args);
