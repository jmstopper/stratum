<?php

/** @var mixed $args */

$orderID = $args['id'] . '-' . 'order';
$selected = 'desc-date';

if (!empty($args['query']->query_vars['orderby'])) {
    $by = key($args['query']->query_vars['orderby']);
    $direction = $args['query']->query_vars['orderby'][$by];
    $selected = strtolower($direction . '-' . $by);
}

$options = [
    [
        'title' => __('Title ascending', 'stratum-text-domain'),
        'value' => 'asc-title',
    ],
    [
        'title' => __('Title descending', 'stratum-text-domain'),
        'value' => 'desc-title',
    ],
    [
        'title' => __('Oldest first', 'stratum-text-domain'),
        'value' => 'asc-date',
    ],
    [
        'title' => __('Newest first', 'stratum-text-domain'),
        'value' => 'desc-date',
    ],
];

?><div class="search-and-filter__sort">
    <label for="<?= $orderID; ?>">
        <?= __('Sort by', 'stratum-text-domain'); ?>
    </label>
    <select name="_order" id="<?= $orderID; ?>">
        <?php foreach ($options as $option) { ?>
            <option value="<?= $option['value'] ?>" <?= $selected === $option['value'] ? 'selected' : ''; ?>>
                <?= $option['title']; ?>
            </option>
        <?php } ?>
    </select>
</div>