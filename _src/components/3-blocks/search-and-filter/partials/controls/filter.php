<?php

/** @var mixed $args */

?><fieldset <?= \Stratum\attributes($args); ?>>
    <legend class="screen-reader-text"><?= $args['label']; ?></legend>

    <?php foreach ($args['options'] as $key => $option) {
        $id = $args['baseid'] . '-' . $args['name'] . '-' . $option['value'];

        ?><div class="search-and-filter__input">
            <input type="<?= esc_attr($args['type']); ?>"
                id="<?= esc_attr($id); ?>"
                name="<?= $args['name']; ?>"
                value="<?= esc_attr($option['value']); ?>"
                <?php if ($option['checked'] === true) {
                    echo "checked";
                } ?>
            >
            <label for="<?= esc_attr($id); ?>"><?= esc_html($option['label']); ?></label>
        </div>
    <?php } ?>
</fieldset>
