<?php

/** @var mixed $args */

$filters = \Stratum\SearchAndFilter\filters($args);

echo \Stratum\Component::render('accordion', [
    'items' => array_map(function ($filter) use ($args) {
        if ($args['config']['debug']) {
            $filter['wrapper']['open'] = true;
        }

        return [
            'classes' => $filter['classes'],
            'wrapper' => $filter['wrapper'],
            'heading' => $filter['label'],
            'content' => \Stratum\Component::render('search-and-filter/partials/controls/filter', $filter, [
                'baseid' => $args['id']
            ]),
        ];
    }, $filters),
]);
