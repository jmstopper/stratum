<?php if (!empty($args['heading']) || !empty($args['content'])) { ?>
    <div class="search-and-filter__header nflm">
        <?= \Stratum\Component::render('tag-and-heading', $args, [
            'headingClasses' => ['h2'],
            'headingOnlyClasses' => ['search-and-filter__heading', 'h2'],
        ]); ?>

        <?php if (!empty($args['content'])) { ?>
            <div class="search-and-filter__content nflm">
                <?= wp_kses_post($args['content']); ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>
