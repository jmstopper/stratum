<?php

/** @var mixed $args */

$totalPages = $args['query']->max_num_pages;

if ($totalPages > 1) {
    // For some reason the paged var is stored as a string
    $currentPage = (int) $args['query']->query_vars['paged'] > 0
        ? (int) $args['query']->query_vars['paged']
        : 1;

    $i = 1;
    $links = [];

    $url = parse_url($args['url']);
    $query = [];

    if (!empty($url['query'])) {
        parse_str($url['query'], $query);
    }

    while ($i <= $totalPages) {
        $query['_paged'] = $i;
        $url['query'] = http_build_query($query);

        $link = [
            'title' => $i,
            'url' => \Stratum\unparseURL($url),
        ];

        if ($i !== $currentPage) {
            $link['title'] = '<span class="screen-reader-text">' . __('Page', 'stratum-text-domain') . ' </span>' . $i;
        } else {
            $link['wrapper']['aria-current'] = 'page';
        }

        $links[] = $link;

        $i++;
    }

    echo \Stratum\Component::render('link-list', [
        'links' => $links
    ]);
}
