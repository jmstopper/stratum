import SearchAndFilter from './scripts/instance.js';

document.addEventListener('DOMContentLoaded', () => {
    const instances = document.querySelectorAll('.search-and-filter');

    if (instances) {
        instances.forEach((instance) => {
            new SearchAndFilter(instance);
        });
    }
});
