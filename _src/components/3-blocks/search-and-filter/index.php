<?php

/** @var mixed $args */
?><div <?= \Stratum\attributes($args); ?>>
    <div class="search-and-filter__outer block__outer">
        <?= \Stratum\Component::render('search-and-filter/partials/header', $args); ?>

        <form class="search-and-filter__form" method="GET">

            <div class="search-and-filter__controls">
                <?= \Stratum\Component::render('search-and-filter/partials/controls', $args); ?>
            </div>

            <div class="search-and-filter__items-wrap nflm">
                <?= \Stratum\Component::render('search-and-filter/partials/message', $args); ?>

                <div class="search-and-filter__results nflm">
                    <?= \Stratum\Component::render('search-and-filter/partials/results', $args); ?>
                </div>

                <nav aria-label="<?= __('Pagination', 'stratum-text-domain'); ?>" class="search-and-filter__pagination">
                    <?= \Stratum\Component::render('search-and-filter/partials/pagination', $args); ?>
                </nav>
            </div>
        </form>
    </div>
</div>