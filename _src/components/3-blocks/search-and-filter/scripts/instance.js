// Inspiration for the behaviour demonstrated below
// - https://www.gov.uk/search
// - https://www.scottohara.me/blog/2022/02/05/dynamic-results.html
export default class SearchAndFilter {
    constructor(element) {
        this.wrapper = element;
        this.config = JSON.parse(this.wrapper.dataset.config);
        this.state = {};
        this.previousSearch = '';

        // elements
        this.form = this.wrapper.querySelector('form');
        this.submit = this.form.querySelector('.search-and-filter__submit');

        // Slots for dynamic content to be dropped in to or events to be attached
        this.results = this.wrapper.querySelector('.search-and-filter__results');
        this.controls = this.wrapper.querySelector('.search-and-filter__controls');
        this.pagination = this.wrapper.querySelector('.search-and-filter__pagination');
        this.message = this.wrapper.querySelector('.search-and-filter__message');

        this.instances = document.querySelectorAll('.search-and-filter').length;

        // Start it up
        this.init();
    }

    init() {
        this.log('init', this);

        // Pull the current search query into the previous search so we can
        // check if we need to reset the pagination
        this.setPreviousSearch();

        // Add all the event handlers
        this.addEventHandlers();

        if (this.config.request_on_init) {
            this.log('render on init');
            this.search();
        }
    }

    addEventHandlers() {
        // If live filtering is enabled we can:
        // - hide the submit button
        // - delegate change event listeners to the controls
        if (this.config.live_filtering) {
            // Hide the submit button as the results change when an input is updated
            this.submit.style.display = 'none';

            // Add event handlers to the inputs
            this.controls.addEventListener('change', () => {
                this.search();
            });
        }

        // Add handlers for pagination
        this.pagination.addEventListener('click', (event) => {
            if (event.target.tagName === 'A') {
                event.preventDefault();

                const url = new URL(event.target.href);

                // Since the paged element is replaced in the controls we need
                // to query it each time. Performance overhead but oh well.
                this.setPaged(url.searchParams.get('_paged'));

                // Run the search update
                this.search();

                // We only scroll in to view here because if we put it in search
                // it would occur after any filtering and we only want it to
                // occur when we are all the way down at the pagination
                this.message.scrollIntoView();
            }
        });

        // Intercept the form submit and run a search
        this.form.addEventListener('submit', (event) => {
            event.preventDefault();

            // If the previous search doesnt match the current search because
            // the filters have changed then reset the paged value
            if (this.previousSearch !== this.getQueryWithoutPagination()) {
                this.setPaged(1);
            }

            this.search();
        });

        // Focus the live region when the form is submitted using the enter key.
        // Not a core part of accessibility but a nice enhancement.
        // https://www.scottohara.me/blog/2022/02/05/dynamic-results.html#wait-what-happens-if-i-press-enter
        this.form.addEventListener('keyup', (event) => {
            if (event.key === 'Enter' && event.target.classList.contains('search-and-filter__submit')) {
                this.message.focus();
            }
        });

        // If the user is currently focused on the live region (most likely
        // after manually submitting the form) and then press escape, send
        // focus back to the first input in the form. Not a core part of
        // accessibility but a nice enhancement to usability.
        this.message.addEventListener('keyup', (event) => {
            if (event.key === 'Escape') {
                this.inputs[0].focus();
            }
        });
    }

    search() {
        this.submit.setAttribute('disabled', true); // prevent double submissions
        this.setPreviousSearch();
        this.updateURL();
        this.request();
    }

    updateURL() {
        // We cant handle updating the url of the page if there is more than one
        // instance of search-and-filter on the page.
        if (this.instances === 1) {
            const formData = new FormData(this.form);

            // Convert it to a url
            const searchparameters = new URLSearchParams(formData).toString();

            this.log('update url', searchparameters);

            // Update the url and search history
            window.history.replaceState({}, document.title, `?${searchparameters}`);
        }
    }

    async request() {
        const formData = new FormData(this.form);

        formData.append('config', JSON.stringify(this.config));
        formData.append('action', 'search_and_filter');
        formData.append('url', window.location);

        const queryString = new URLSearchParams(formData).toString();
        const response = await fetch(`/wp-json/stratum/v1/search-and-filter?${queryString}`);

        this.log('request string', queryString);

        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }

        this.state = await response.json();

        this.render();
    }

    setPaged(value) {
        this.controls.querySelector('.search-and-filter__paged').value = value;
    }

    setPreviousSearch() {
        this.previousSearch = this.getQueryWithoutPagination();
    }

    getQueryWithoutPagination() {
        const formData = new FormData(this.form);
        formData.delete('_paged');
        return new URLSearchParams(formData).toString();
    }

    render() {
        this.log('render', this.state);
        this.controls.innerHTML = this.state.partials.controls;
        this.results.innerHTML = this.state.partials.results;
        this.message.innerHTML = this.state.partials.message;
        this.pagination.innerHTML = this.state.partials.pagination;

        if (this.state.numbers.results > 0) {
            this.message.classList.remove('screen-reader-text');
        } else {
            this.message.classList.add('screen-reader-text');
        }

        // Because the submit button was replaced in the partials,
        // lets query it again so we have a reference
        this.submit = this.form.querySelector('.search-and-filter__submit');
    }

    log(action, content = '') {
        if (this.config.debug) {
            if (content !== '') {
                console.log(`S&F: ${action}`, content);
            } else {
                console.log(`S&F: ${action}`);
            }
        }
    }
}
