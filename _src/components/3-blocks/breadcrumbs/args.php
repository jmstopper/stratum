<?php

add_filter('stratum/render/assets/components/breadcrumbs', function ($args) {

    if (!\Stratum\Breadcrumbs\enabled()) {
        return null;
    }

    $args = wp_parse_args($args, [
        'classes' => [],
        'el' => 'nav',
        'type' => 'ol',
        'links' => \Stratum\Plugin\YoastSEO::breadcrumbsAsArray(),
        'wrapper' => [
            'aria-label' => __('Breadcrumbs', 'stratum-text-domain')
        ]
    ]);

    $args['classes'] = array_merge([
        'breadcrumbs', 'nflm', 'alignnormal'
    ], $args['classes']);

    return $args;
});
