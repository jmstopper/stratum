<?php

namespace Stratum\Breadcrumbs;

// -----------------------------------------------------------------------------
// Tips and tricks for modifying breadrumbs
// https://gist.github.com/amboutwe/ea0791e184668a5c7bd7bbe357c598e9
// -----------------------------------------------------------------------------

add_theme_support('yoast-seo-breadcrumbs');

// -----------------------------------------------------------------------------
// Allows for replacing the value of one of the breadcrumbs
// -----------------------------------------------------------------------------
// add_filter('wpseo_breadcrumb_single_link_info', function ($link_info, $index, $crumbs) {

//     if ($index !== array_key_last($crumbs)) {
//         return $link_info;
//     }

//     if (!empty($link_info['id']) && $client = get_field('client', $link_info['id'])) {
//         $link_info['text'] = $client;
//     }

//     return $link_info;
// }, 10, 3);

// -----------------------------------------------------------------------------
// Change the element seperating the breadcrumbs
// This overwrites the value set in WordPress
// -----------------------------------------------------------------------------
// add_filter('wpseo_breadcrumb_separator', function ($seperator) {
//     return '';
// });

// -----------------------------------------------------------------------------
// Add a class to the body if the page has breadcrumbs showing
// -----------------------------------------------------------------------------
add_filter('body_class', function ($classes) {
    if (\Stratum\Breadcrumbs\enabled()) {
        $classes[] = 'has-breadcrumbs';
    }

    return $classes;
});

// -----------------------------------------------------------------------------
// Helper function for determining if breadcrumbs are shown. Modify as needed
// -----------------------------------------------------------------------------
function enabled(): bool
{
    // Are we on the page post type and is this a top level page
    return is_front_page() || is_post_type_archive() || is_home() || is_404() || (get_post_type() === 'page' && !has_post_parent())
        ? false
        : true;
}
