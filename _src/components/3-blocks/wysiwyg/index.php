<?php
/** @var mixed $args */
?><div <?= \Stratum\attributes($args); ?>>
    <div class="block__outer wysiwyg__outer nflm">
        <?= \Stratum\Component::render('tag-and-heading', $args, [
            '+headingClasses' => ['h2'],
            'headingOnlyClasses' => ['wysiwyg__heading'],
        ]); ?>

        <?php if (!empty($args['content'])) { ?>
            <div class="wysiwyg__content nflm">
                <?= $args['content']; ?>
            </div>
        <?php } ?>
    </div>
</div>
