<?php

add_filter('stratum/render/assets/components/wysiwyg', function ($args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'tag' => '',
        'heading' => '',
        'content' => '<InnerBlocks />',
        'tag_is_semantic_heading' => false,
    ]);

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge([
        'wysiwyg', 'block'
    ], $args['classes']);

    return $args;
});
