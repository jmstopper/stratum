<?php
/** @var mixed $args */
?><div <?= \Stratum\attributes($args); ?>>

    <button
        class="accessibility-controls__button js-button"
        id="accessibility-controls-button"
        type="button"
        aria-controls="accessibility-controls"
        aria-expanded="false"
        aria-label="<?= __('Open accessibility menu', 'stratum-text-domain'); ?>"
        data-label-opened="<?= __('Close accessibility menu', 'stratum-text-domain'); ?>"
        data-label-closed="<?= __('Open accessibility menu', 'stratum-text-domain'); ?>"
    ><span class="hidden"><?= __('Accessibility menu', 'stratum-text-domain'); ?></span><?= \Stratum\svg('person', ['width' => 30, 'height' => 36]); ?></button>

    <fieldset
        id="accessibility-controls"
        class="accessibility-controls__group nflm has-background has-beige-background-color"
        disabled>
        <legend class="accessibility-controls__heading h4"><?= __('Accessibility Controls', 'stratum-text-domain'); ?></legend>
        <div class="accessibility-controls__content nflm">
            <p>
                The following controls allow you to change parts of the site to
                make it more accessible to people of all abilities.
            </p>
        </div>
        <div class="accessibility-controls__controls nflm">
            <?php foreach ($args['items'] as $key => $item) { ?>
                <div class="accessibility-controls__control">
                    <label for="accessibility-controls-<?= $key; ?>"><?= $item; ?></label>
                    <input id="accessibility-controls-<?= $key; ?>" type="checkbox" data-class="is-<?= $key; ?>">
                </div>
            <?php } ?>
        </div>
    </fieldset>
</div>
