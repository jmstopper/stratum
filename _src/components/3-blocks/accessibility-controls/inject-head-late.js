import Cookies from 'js-cookie';

function disableStylesheets() {
    for (const stylesheet of document.styleSheets) {
        stylesheet.disabled = true;
    }
}

let classes = Cookies.get('accessibility-controls');

if (classes !== undefined) {
    classes = JSON.parse(classes);

    for (const className of classes) {
        document.documentElement.classList.add(className);

        if (className === 'is-disable-css') {
            disableStylesheets();

            setTimeout(() => {
                disableStylesheets();
            }, 500);
        }
    }
}
