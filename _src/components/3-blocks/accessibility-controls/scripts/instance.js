import * as FocusTrap from 'focus-trap'; // ESM
import Cookies from 'js-cookie';

const cookieName = 'accessibility-controls';

export default class AccessibilityControls {
    constructor(element) {
        this.wrapper = element;
        this.html = document.documentElement;
        this.button = this.wrapper.querySelector('.js-button');
        this.fieldset = this.wrapper.querySelector('fieldset');
        this.controls = this.wrapper.querySelectorAll('input');
        this.focusTrap = FocusTrap.createFocusTrap(this.wrapper, {
            clickOutsideDeactivates: true,
            onDeactivate: () => this.closeMenu(),
        });
        this.init();
    }

    init() {
        this.state = Cookies.get(cookieName);

        if (this.state === undefined) {
            this.state = [];
        } else {
            this.state = JSON.parse(this.state);

            this.state.forEach((className) => {
                this.wrapper.querySelector(`[data-class="${className}"]`).checked = true;
            });
        }

        this.button.addEventListener('click', (event) => {
            event.preventDefault();
            this.toggleMenu();
        });

        this.controls.forEach((control) => {
            control.addEventListener('click', () => {
                this.toggleControl(control);
            });
        });

        document.addEventListener('keyup', (event) => {
            if (event.key === 'Escape') {
                // Is the focus within the navigation still?
                if (this.wrapper.contains(event.target)) {
                    // Set focus to the closest toggle
                    this.button.focus();
                    this.closeMenu();
                }
            }
        });
    }

    toggleMenu() {
        if (this.button.getAttribute('aria-expanded') === 'false') {
            this.openMenu();
        } else {
            this.closeMenu();
        }
    }

    openMenu() {
        this.wrapper.classList.add('is-open');
        this.button.setAttribute('aria-expanded', 'true');
        this.button.setAttribute('aria-label', this.button.dataset.labelOpened);
        this.fieldset.disabled = false;
        this.focusTrap.activate();
    }

    closeMenu() {
        this.wrapper.classList.remove('is-open');
        this.button.setAttribute('aria-expanded', 'false');
        this.button.setAttribute('aria-label', this.button.dataset.labelClosed);
        this.fieldset.disabled = true;
        this.focusTrap.deactivate();
    }

    toggleControl(control) {
        this.html.classList.toggle(control.dataset.class);

        const index = this.state.indexOf(control.dataset.class);

        if (index === -1) {
            this.state.push(control.dataset.class);

            if (control.dataset.class === 'is-disable-css') {
                AccessibilityControls.disableCSS();
            }
        } else {
            this.state.splice(index, 1);

            if (control.dataset.class === 'is-disable-css') {
                AccessibilityControls.enableCSS();
            }
        }

        Cookies.set(cookieName, JSON.stringify(this.state), { expires: 365 });
    }

    static enableCSS() {
        for (let i = 0; i < document.styleSheets.length; i += 1) {
            document.styleSheets.item(i).disabled = false;
        }
    }

    static disableCSS() {
        for (let i = 0; i < document.styleSheets.length; i += 1) {
            document.styleSheets.item(i).disabled = true;
        }
    }
}
