<?php

add_filter('stratum/render/assets/components/accessibility-controls', function ($args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'heading' => '',
        'content' => '<InnerBlocks />',
        'items' => [
            'dyslexic-friendly' => __('Dyslexic friendly mode', 'stratum-text-domain'),
            'line-height' => __('Increased line height', 'stratum-text-domain'),
            'no-animation' => __('No animation', 'stratum-text-domain'),
            'start-text-align' => __('Left align text', 'stratum-text-domain'),
            'desaturate' => __('Desaturated colors', 'stratum-text-domain'),
            'low-data' => __('Low Data mode', 'stratum-text-domain'),
            'disable-css' => __('Disable CSS', 'stratum-text-domain'),
            // 'high-contrast' => __('High Contrast', 'stratum-text-domain'),
            // 'dark-mode' => __('Dark mode', 'stratum-text-domain'),
        ],
    ]);

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge([
        'accessibility-controls',
    ], $args['classes']);

    return $args;
});
