import AccessibilityControls from './scripts/instance.js';

document.addEventListener('DOMContentLoaded', () => {
    const instances = document.querySelectorAll('.accessibility-controls');

    if (instances) {
        instances.forEach((instance) => {
            new AccessibilityControls(instance);
        });
    }
});
