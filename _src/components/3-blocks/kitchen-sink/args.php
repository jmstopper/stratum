<?php

namespace Stratum\KitchenSink;

add_filter('stratum/render/assets/components/kitchen-sink', function ($args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
    ]);

    $args['classes'] = array_merge($args['classes'], [
        'kitchen-sink'
    ]);

    // -------------------------------------------------------------------------
    // Generate the components
    // -------------------------------------------------------------------------
    $components = \Stratum\Components::get();
    $colors = \Stratum\Colors::secondary();

    // Add the basic text based elements
    $componentsToRender = [
        [
            'component' => 'kitchen-sink/partials/elements',
            'json' => json_decode(json_encode([
                'title' => 'Elements',
            ]), false),
            'datasets' => [[]],
        ]
    ];

    foreach ($components as $component) {
        $json = \Stratum\Component::json($component);

        if (!empty($json->stratum->render) && $json->stratum->render === true &&  !empty($json->stratum->fields)) {
            // Load the CSS and Javascript required for the block
            \Stratum\Component::loadAssets($component);

            $variations = [];
            $componentArgs = resolveArguments($component);

            if (!property_exists($json->stratum, 'renderDefault') || $json->stratum->renderDefault === true) {
                $variations[] = $componentArgs;

                // ---------------------------------------------------------------------
                // If the type is an array we need to loop each of the possible values
                // for type and create a version of it
                // ---------------------------------------------------------------------
                foreach ($json->stratum->fields as $name => $field) {
                    if (!isset($field->_render) || $field->_render == true) {
                        if (is_array($field->type)) {
                            foreach ($field->type as $key => $type) {
                                // We already have this variation
                                if ($key === 0) {
                                    continue;
                                }

                                // Build the new variation
                                $variation = $componentArgs;
                                $variation[$name] = resolveArgument($type, $field);
                                $variations[] = $variation;
                            }
                        } elseif ($field->type === 'bool') {
                            $variation = $componentArgs;
                            $variation[$name] = true;
                            $variations[] = $variation;
                        }
                    }
                }

                // ---------------------------------------------------------------------
                // Text alignment
                // ---------------------------------------------------------------------
                if (!empty($json->supports->align_text) && $json->supports->align_text === true) {
                    // Make a new copy
                    $variation = $componentArgs;
                    $variation['classes'][] = 'has-text-align-center';
                    $variations[] = $variation;

                    // Make a new copy
                    $variation = $componentArgs;
                    $variation['classes'][] = 'has-text-align-right';
                    $variations[] = $variation;
                }

                // ---------------------------------------------------------------------
                // Block alignment
                // ---------------------------------------------------------------------
                if (!empty($json->supports->align)) {
                    if (is_bool($json->supports->align) && $json->supports->align === true) {
                        foreach (['wide', 'full', 'left', 'right'] as $align) {
                            // Make a new copy
                            $variation = $componentArgs;
                            $variation['classes'][] = 'align' . $align;
                            $variations[] = $variation;
                        }
                    } elseif (is_array($json->supports->align)) {
                        foreach ($json->supports->align as $align) {
                            // Make a new copy
                            $variation = $componentArgs;
                            $variation['classes'][] = 'align' . $align;
                            $variations[] = $variation;
                        }
                    } elseif (is_string($json->supports->align)) {
                        // Make a new copy
                        $variation = $componentArgs;
                        $variation['classes'][] = 'align' . $json->supports->align;
                        $variations[] = $variation;
                    }
                }

                // ---------------------------------------------------------------------
                // Background
                // ---------------------------------------------------------------------
                if (!empty($json->supports->color->background)) {
                    foreach ($colors as $color) {
                        $variation = $componentArgs;
                        $variation['classes'][] = 'has-' . $color['background'] . '-background-color';
                        $variations[] = $variation;
                    }
                }
            }

            // ---------------------------------------------------------------------
            // Custom variations
            // ---------------------------------------------------------------------
            if (!empty($json->stratum->variations)) {
                foreach ($json->stratum->variations as $componentVariation) {
                    $variation = $componentArgs;

                    foreach ($componentVariation as $argument => $type) {
                        if ($argument === "classes") {
                            $variation[$argument] = $type;
                        } else {
                            $variation[$argument] = resolveArgument($type);
                        }
                    }

                    $variations[] = $variation;
                }
            }

            $componentsToRender[] = [
                'component' => $component,
                'json' => $json,
                'datasets' => $variations,
            ];
        }
    }

    $args['components'] = $componentsToRender;

    return $args;
});
