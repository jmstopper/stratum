import KitchenSink from './scripts/instance.js';

document.addEventListener('DOMContentLoaded', () => {
    const instances = document.querySelectorAll('.kitchen-sink');

    if (instances) {
        instances.forEach((instance) => {
            new KitchenSink(instance);
        });
    }
});
