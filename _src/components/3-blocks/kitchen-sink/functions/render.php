<?php

namespace Stratum\KitchenSink;

function render($component): string
{
    \Stratum\Component::loadAssets($component);
    return \Stratum\Component::render($component, resolveArguments($component));
}
