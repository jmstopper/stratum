<?php

namespace Stratum\KitchenSink;

function resolveArguments($component): array
{
    $json = \Stratum\Component::json($component);
    $data = [
        "classes" => [],
    ];

    if (!empty($json->stratum->fields)) {
        foreach ($json->stratum->fields as $name => $field) {
            if (!isset($field->_render) || $field->_render == true) {
                $type = is_array($field->type) ? $field->type[0] : $field->type;
                $data[$name] = resolveArgument($type, $field);

                if (!empty($field->quantity)) {
                    $instance = $data[$name];
                    $data[$name] = [];

                    $i = 0;

                    while ($i < $field->quantity) {
                        $data[$name][] = $instance;
                        $i++;
                    }
                }
            }
        }
    }

    return $data;
}
