<?php

namespace Stratum\KitchenSink;

function resolveArgument($type, $field = [])
{
    $dummyData = \Stratum\Asset::require('components/kitchen-sink/partials/data.php');

    if (array_key_exists($type, $dummyData)) {
        return is_array($dummyData[$type])
            ? $dummyData[$type][0]
            : $dummyData[$type];
    }

    $components = \Stratum\Components::get();

    if (in_array($type, $components)) {
        return !empty($field->data) && $field->data === 'raw'
            ? resolveArguments($type)
            : render($type);
    }

    echo "<pre>";
    echo "Error: " . $type;
    exit;
}
