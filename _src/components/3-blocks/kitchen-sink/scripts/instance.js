export default class KitchenSink {
    constructor(element) {
        this.wrapper = element;
        this.controls = this.wrapper.querySelectorAll('.js-kitchen-sink-controls [type="radio"]');
        this.components = this.wrapper.querySelectorAll('.kitchen-sink__component');

        this.init();
    }

    init() {
        this.controls.forEach((control) => {
            control.addEventListener('change', () => {
                this.deactivateAll();
                this.wrapper.querySelector(`.is-${control.dataset.component}`).classList.add('is-active');
            });
        });
    }

    deactivateAll() {
        this.components.forEach((component) => {
            component.classList.remove('is-active');
        });
    }
}
