<?php
/** @var mixed $args */
?><div <?= \Stratum\attributes($args); ?>>
    <?= \Stratum\Component::render('kitchen-sink/partials/controls', $args); ?>

    <div class="kitchen-sink__components"><?php
    foreach ($args['components'] as $key => $component) {
        $component['active'] = $key === 0;
        echo \Stratum\Component::render('kitchen-sink/partials/component', $component);
    }
    ?></div>
</div>
