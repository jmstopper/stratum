<?php

return [
    "text" => "lorem ipsum dolar sit amet",
    "wysiwyg" => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet, purus sed mattis maximus, sem orci varius ex, ut mollis magna libero vitae nulla.</p><p>Sed vestibulum faucibus lacus, eget imperdiet purus posuere at. Maecenas mollis porta pharetra.</p><p>Nullam vel purus non lacus consectetur eleifend nec in libero. Nam non vestibulum arcu, vel egestas urna. Pellentesque scelerisque suscipit placerat.</p><p>Cras dolor mauris, varius sit amet erat vitae, iaculis mollis urna. Aenean fringilla nec tellus et fringilla.",
    "image" => \Stratum\image('screenshot.png'),
    "embed" => '<iframe width="560" height="315" src="https://www.youtube.com/embed/5GJWxDKyk3A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
    "embedurl" => "https://www.youtube.com/embed/5GJWxDKyk3A",
    "bool" => [false, true]
];
