<?php
/** @var mixed $args */
?><div class="kitchen-sink__controls nflm js-kitchen-sink-controls">
    <fieldset>
        <?php foreach ($args['components'] as $key => $component) {
            $id = 'kitchen-sink-control-' . sanitize_title($component['component']);

            ?><div>
                <input
                    name="kitchen-sink-control"
                    type="radio"
                    id="<?= esc_attr($id); ?>"
                    data-component="<?= esc_attr(sanitize_title($component['component'])); ?>"
                    <?= $key === 0 ? 'checked' : ''; ?>
                >
                <label for="<?= esc_attr($id); ?>"><?= $component['json']->title; ?></label>
            </div>
        <?php } ?>
    </fieldset>
</div>
