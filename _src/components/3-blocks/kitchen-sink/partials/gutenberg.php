<!-- wp:heading -->
<h2>Heading Block (H2)</h2>
<!-- /wp:heading -->

<!-- wp:heading -->
<h3>You are looking at one. (H3)</h3>
<!-- /wp:heading -->

<!-- wp:heading -->
<h2>Subhead Block</h2>
<!-- /wp:heading -->

<!-- wp:subhead -->
<p class="wp-block-subhead">This is a Subhead</p>
<!-- /wp:subhead -->

<!-- wp:heading -->
<h2>Paragraph Block</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>This is a paragraph block. Professionally productize highly efficient results with world-class core competencies. Objectively matrix leveraged architectures vis-a-vis error-free applications. Completely maximize customized portals via fully researched metrics. Enthusiastically generate premier action items through web-enabled e-markets. Efficiently parallel task holistic intellectual capital and client-centric markets.<br /><br /></p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Image Block (Standard
    <g class="gr_ gr_3 gr-alert gr_spell gr_inline_cards gr_run_anim ContextualSpelling ins-del multiReplace" id="3" data-gr-id="3">width</g>)
</h2>
<!-- /wp:heading -->

<!-- wp:image {"id":1755} -->
<figure class="wp-block-image"><img src="https://d.pr/i/8pTmgY+" alt="" class="wp-image-1755" /></figure>
<!-- /wp:image -->

<!-- wp:heading -->
<h2>Image Block (Wide
    <g class="gr_ gr_3 gr-alert gr_spell gr_inline_cards gr_run_anim ContextualSpelling ins-del multiReplace" id="3" data-gr-id="3">width</g>)
</h2>
<!-- /wp:heading -->

<!-- wp:image {"id":1755,"align":"wide"} -->
<figure class="wp-block-image alignwide"><img src="https://d.pr/i/8pTmgY+" alt="" class="wp-image-1755" /></figure>
<!-- /wp:image -->

<!-- wp:heading -->
<h2>Image Block (Full width)</h2>
<!-- /wp:heading -->

<!-- wp:image {"id":1755,"align":"full"} -->
<figure class="wp-block-image alignfull"><img src="https://d.pr/i/8pTmgY+" alt="" class="wp-image-1755" /></figure>
<!-- /wp:image -->

<!-- wp:heading -->
<h2>Cover Image Block</h2>
<!-- /wp:heading -->

<!-- wp:cover-image {"url":"https://d.pr/i/8pTmgY+","align":"full","id":1755} -->
<div class="wp-block-cover-image has-background-dim alignfull" style="background-image:url(https://d.pr/i/8pTmgY+)">
    <p class="wp-block-cover-image-text">This block can be used for Hero section.<br />Full width has been applied.</p>
</div>
<!-- /wp:cover-image -->

<!-- wp:heading -->
<h2>Gallery Block</h2>
<!-- /wp:heading -->

<!-- wp:gallery -->
<ul class="wp-block-gallery alignnone columns-3 is-cropped">
    <li class="blocks-gallery-item">
        <figure><img src="https://d.pr/i/zd7Ehu+" alt="" data-id="1706" />
            <figcaption>Stool</figcaption>
        </figure>
    </li>
    <li class="blocks-gallery-item">
        <figure><img src="https://d.pr/i/jXLtzZ+" alt="" data-id="1705" />
            <figcaption>Box</figcaption>
        </figure>
    </li>
    <li class="blocks-gallery-item">
        <figure><img src="https://d.pr/i/VhP9ud+" alt="" data-id="1704" />
            <figcaption>Mug</figcaption>
        </figure>
    </li>
    <li class="blocks-gallery-item">
        <figure><img src="https://d.pr/i/HB0qXg+" alt="" data-id="1703" />
            <figcaption>Clock</figcaption>
        </figure>
    </li>
    <li class="blocks-gallery-item">
        <figure><img src="https://d.pr/i/5cHfcd+" alt="" data-id="1699" />
            <figcaption>Telephone</figcaption>
        </figure>
    </li>
</ul>
<!-- /wp:gallery -->

<!-- wp:heading -->
<h2>List Block</h2>
<!-- /wp:heading -->

<!-- wp:list -->
<ul>
    <li>List item 1</li>
    <li>List item 2</li>
    <li>List item 3</li>
    <li>List item 4</li>
</ul>
<!-- /wp:list -->

<!-- wp:heading {"className":"has-top-margin"} -->
<h2 class="has-top-margin">Columns Block</h2>
<!-- /wp:heading -->

<!-- wp:columns -->
<div class="wp-block-columns has-2-columns">
    <!-- wp:image {"id":1701,"layout":"column-1"} -->
    <figure class="wp-block-image layout-column-1"><img src="https://d.pr/i/fW6V3V+" alt="" class="wp-image-1701" /></figure>
    <!-- /wp:image -->

    <!-- wp:paragraph {"layout":"column-2"} -->
    <p class="layout-column-2">Phosfluorescently morph intuitive relationships rather than customer directed human capital. Dynamically customize turnkey information whereas orthogonal processes. Assertively deliver superior leadership skills whereas holistic outsourcing. Enthusiastically iterate enabled best practices vis-a-vis 24/365 communities.</p>
    <!-- /wp:paragraph -->
</div>
<!-- /wp:columns -->

<!-- wp:heading -->
<h2>Text Column Block</h2>
<!-- /wp:heading -->

<!-- wp:text-columns -->
<div class="wp-block-text-columns alignundefined columns-2">
    <div class="wp-block-column">
        <p>This is left text column.<br /><br />Appropriately initiate pandemic data via economically sound scenarios. Credibly foster inexpensive methodologies via cross-media leadership. Dramatically monetize seamless infomediaries via principle-centered functionalities. Seamlessly morph cross functional outsourcing via team building meta-services. Intrinsicly embrace installed base products for enabled testing procedures.<br /></p>
    </div>
    <div class="wp-block-column">
        <p>This is right text column.<br /><br />Completely disintermediate user friendly content through covalent core competencies. Enthusiastically seize intermandated alignments for resource sucking paradigms. Intrinsicly engineer sustainable initiatives through resource maximizing benefits. Efficiently fashion top-line vortals through distributed interfaces. Intrinsicly engage performance based mindshare with best-of-breed outsourcing.<br /></p>
    </div>
</div>
<!-- /wp:text-columns -->

<!-- wp:heading -->
<h2>Button Block</h2>
<!-- /wp:heading -->

<!-- wp:button -->
<div class="wp-block-button alignnone"><a class="wp-block-button__link" href="#">Get Started</a></div>
<!-- /wp:button -->

<!-- wp:heading -->
<h2>Quote Block</h2>
<!-- /wp:heading -->

<!-- wp:quote -->
<blockquote class="wp-block-quote">
    <p><em>Take up one idea, make that one idea your life. Think of it, dream of it, Live on that idea let the brain, muscles, nerves, every part of your body be full of that idea, and just leave every other idea alone. This is the way to success.</em></p><cite>Swami Vivekananda</cite>
</blockquote>
<!-- /wp:quote -->

<!-- wp:heading -->
<h2>Quote Block (another style)</h2>
<!-- /wp:heading -->

<!-- wp:quote {"style":2} -->
<blockquote class="wp-block-quote is-large">
    <p><em>Take up one idea, make that one idea your life. Think of it, <g class="gr_ gr_10 gr-alert gr_gramm gr_inline_cards gr_run_anim Grammar only-ins doubleReplace replaceWithoutSep" id="10" data-gr-id="10">dream</g> of it, Live on that idea let the brain, muscles, nerves, every part of your body be full of that idea, and just leave every other idea alone. This is the way to success.</em></p><cite>Swami Vivekananda</cite>
</blockquote>
<!-- /wp:quote -->

<!-- wp:heading -->
<h2>Code Block</h2>
<!-- /wp:heading -->

<!-- wp:code -->
<pre class="wp-block-code"><code>@media only screen and (min-width: 960px) {

	body .alignfull {
		width: auto;
		max-width: 1000%;

		margin-inline: calc(50% - 50vw);
	}

	body .alignwide {
		width: auto;
		max-width: 1000%;
		margin-inline: calc(25% - 25vw);
	}

	.alignwide img,
	.alignfull img {
		display: block;
		margin: 0 auto;
	}
}</code></pre>
<!-- /wp:code -->

<!-- wp:heading -->
<h2>Audio Block</h2>
<!-- /wp:heading -->

<!-- wp:audio -->
<figure class="wp-block-audio"><audio controls src="https://freemusicarchive.org/file/music/WFMU/Broke_For_Free/Directionless_EP/Broke_For_Free_-_01_-_Night_Owl.mp3"></audio></figure>
<!-- /wp:audio -->

<!-- wp:heading -->
<h2>Video Block</h2>
<!-- /wp:heading -->

<!-- wp:video -->
<figure class="wp-block-video"><video controls src="https://archive.org/download/SlowMotionFlame/slomoflame_512kb.mp4"></video></figure>
<!-- /wp:video -->

<!-- wp:heading {"className":""} -->
<h2>Custom HTML Block</h2>
<!-- /wp:heading -->

<!-- wp:html -->
<strong>This is a HTML block.</strong>
<!-- /wp:html -->

<!-- wp:heading {"className":"has-top-margin"} -->
<h2 class="has-top-margin">Preformatted Block</h2>
<!-- /wp:heading -->

<!-- wp:preformatted -->
<pre class="wp-block-preformatted">This is some preformatted text. Preformatted text keeps your s p a c e s, tabs and<br/>linebreaks as they are.</pre>
<!-- /wp:preformatted -->

<!-- wp:heading -->
<h2>Verse Block</h2>
<!-- /wp:heading -->

<!-- wp:verse -->
<pre class="wp-block-verse">Write poetry and other literary expressions honoring all spaces and line-breaks.</pre>
<!-- /wp:verse -->

<!-- wp:heading -->
<h2>Table Block</h2>
<!-- /wp:heading -->

<!-- wp:table -->
<table class="wp-block-table">
    <tbody>
        <tr style="height:45px">
            <td style="height:45px">Row 1 Column 1</td>
            <td style="height:45px">Row 1 Column 2</td>
        </tr>
        <tr style="height:45px">
            <td style="height:45px">Row 2 Column 1</td>
            <td style="height:45px">Row 2 Column 2</td>
        </tr>
        <tr style="height:45px">
            <td style="height:45px">Row 3 Column 1</td>
            <td style="height:45px">Row 3 Column 2</td>
        </tr>
    </tbody>
</table>
<!-- /wp:table -->

<!-- wp:heading -->
<h2>Separator Block</h2>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator" />
<!-- /wp:separator -->

<!-- wp:heading {"className":"has-top-margin"} -->
<h2 class="has-top-margin">Spacer Block</h2>
<!-- /wp:heading -->

<!-- wp:spacer -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading -->
<h2>Categories Block</h2>
<!-- /wp:heading -->

<!-- wp:categories /-->

<!-- wp:heading -->
<h2>Latest Posts Block</h2>
<!-- /wp:heading -->

<!-- wp:latest-posts /-->

<!-- wp:heading -->
<h2>Twitter Embed Block</h2>
<!-- /wp:heading -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/MKBHD/status/993606431126548481","type":"rich","providerNameSlug":"twitter"} -->
<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
    https://twitter.com/MKBHD/status/993606431126548481
</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading -->
<h2>YouTube Embed Block</h2>
<!-- /wp:heading -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=V-Ma40uyo-I","type":"video","providerNameSlug":"youtube"} -->
<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube">
    https://www.youtube.com/watch?v=V-Ma40uyo-I
</figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:heading -->
<h2>Facebook Embed Block</h2>
<!-- /wp:heading -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/groups/macpowerusers/","providerNameSlug":"embed-handler"} -->
<figure class="wp-block-embed-facebook wp-block-embed is-provider-embed-handler">
    https://www.facebook.com/groups/macpowerusers/
</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:heading -->
<h2>WordPress Embed Block</h2>
<!-- /wp:heading -->

<!-- wp:core-embed/wordpress {"url":"https://gutensample.genesiswp.club/contact/","type":"wp-embed","providerNameSlug":"genesis-framework"} -->
<figure class="wp-block-embed-wordpress wp-block-embed is-type-wp-embed is-provider-genesis-framework">
    https://gutensample.genesiswp.club/contact/
</figure>
<!-- /wp:core-embed/wordpress -->

<!-- wp:heading -->
<h2>Vimeo Embed Block</h2>
<!-- /wp:heading -->

<!-- wp:core-embed/vimeo {"url":"https://vimeo.com/267362241","type":"video","providerNameSlug":"vimeo"} -->
<figure class="wp-block-embed-vimeo wp-block-embed is-type-video is-provider-vimeo">
    https://vimeo.com/267362241
</figure>
<!-- /wp:core-embed/vimeo -->