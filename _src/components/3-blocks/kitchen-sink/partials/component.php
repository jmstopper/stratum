<?php
/** @var mixed $args */
?><?php

$classes = [
    'kitchen-sink__component',
    'is-' . sanitize_title($args['component']),
    'nflm',
];

if ($args['active'] === true) {
    $classes[] = 'is-active';
}

?><div class="<?= esc_attr(implode(' ', $classes)); ?>">
    <?php foreach ($args['datasets'] as $data) {
        echo \Stratum\Component::render($args['component'], $data);
    } ?>
</div>
