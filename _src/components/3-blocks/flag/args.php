<?php

add_filter('stratum/render/assets/components/flag', function ($args): array {
    if ($args instanceof \WP_Post) {
        $myPost = \Stratum\Page\Post::seed($args);

        $args = [
            'heading' => \Stratum\link($args),
            'content' => $myPost->excerpt(),
            'image' => $myPost->hasImage() ? ['id' => $myPost->imageID()] : '',
        ];
    }

    $args = wp_parse_args($args, [
        'classes' => [],
        'heading' => '',
        'content' => '<InnerBlocks />',
        'media' => '',
        'reverse' => false,
        'embed_title' => '',
    ]);

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge([
        'flag', 'block'
    ], $args['classes']);

    // -------------------------------------------------------------------------
    // An image from wordpress
    // -------------------------------------------------------------------------
    if (!empty($args['image']['id'])) {
        $args['media'] = wp_get_attachment_image($args['image']['id'], 'large');
        unset($args['image']);
    }

    return $args;
});
