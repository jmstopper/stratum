<?php
/** @var mixed $args */
?><div <?= \Stratum\attributes($args); ?>>
    <div class="flag__outer block__outer">

        <div class="flag__inner nflm">
            <?= \Stratum\Component::render('tag-and-heading', $args, [
                '+headingClasses' => ['h2'],
                'headingOnlyClasses' => ['flag__heading', 'h2'],
            ]); ?>

            <?php if (!empty($args['content'])) { ?>
                <div class="flag__content nflm">
                    <?= $args['content']; ?>
                </div>
            <?php } ?>
        </div>

        <?php if (!empty($args['media'])) {
            $mediaClasses = ['flag__media', 'nflm'];

            if (str_starts_with($args['media'], '<img')) {
                $mediaClasses[] = 'objfit';
            }

            ?><div class="<?= esc_attr(implode(' ', $mediaClasses)) ?>">
                <?= $args['media']; ?>
            </div>
        <?php } ?>
    </div>
</div>
