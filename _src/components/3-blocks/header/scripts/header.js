// Follows
// https://www.w3.org/TR/wai-aria-practices-1.1/examples/disclosure/disclosure-navigation.html

// -----------------------------------------------------------------------------
// Import all the libraries we need
// -----------------------------------------------------------------------------
import * as FocusTrap from 'focus-trap'; // ESM
import siblings from '../../../0-framework/scripts/siblings.js';

// -----------------------------------------------------------------------------
// Define all the constants we will be using in this file
// -----------------------------------------------------------------------------
const dropdownSelector = '.menu-item-has-children';

export default class Header {
    constructor(element) {
        // ---------------------------------------------------------------------
        // Select the elements we need to work with
        // ---------------------------------------------------------------------
        this.header = element;
        this.navigation = this.header.querySelector('.header__navigation');
        this.dropdowns = this.header.querySelectorAll(dropdownSelector);
        this.burger = this.header.querySelector('.js-header-burger');

        // ---------------------------------------------------------------------
        // Start it up
        // ---------------------------------------------------------------------
        this.init();
    }

    init() {
        this.focusTrap = FocusTrap.createFocusTrap(this.navigation, {
            clickOutsideDeactivates: true,
            onDeactivate: () => this.closeBurgerMenu(),
        });

        // ---------------------------------------------------------------------
        // Handle opening and closing the submenus
        // ---------------------------------------------------------------------
        this.dropdowns.forEach((dropdown) => {
            const button = dropdown.querySelector('button');
            const submenu = dropdown.querySelector('.sub-menu-outer');

            // Lets detect if there actually is a sub menu. If WordPress generates
            // a single layer only menu it will still have the class but not the html
            if (submenu !== null) {
                button.addEventListener('click', () => {
                    Header.toggleDropdown(dropdown);
                });
            }
        });

        // -----------------------------------------------------------------------------
        // Make the burger work
        // -----------------------------------------------------------------------------
        this.burger.addEventListener('click', (event) => {
            event.preventDefault();
            this.toggleBurgerMenu();
        });

        // -----------------------------------------------------------------------------
        // Handle the key presses for the menu
        // -----------------------------------------------------------------------------
        document.addEventListener('keyup', (event) => {
            if (event.key === 'Tab') {
                if (!this.navigation.contains(event.target)) {
                    this.dropdowns.forEach((dropdown) => {
                        Header.closeDropdown(dropdown);
                    });
                }
            }

            if (event.key === 'Escape') {
                // Is the focus within the navigation still?
                if (this.navigation.contains(event.target)) {
                    // Set focus to the closest toggle
                    event.target.closest(dropdownSelector).querySelector('button').focus();

                    // Close the dropdowns
                    this.dropdowns.forEach((dropdown) => {
                        this.closeDropdown(dropdown);
                    });
                }
            }
        });
    }

    toggleBurgerMenu() {
        if (this.burger.getAttribute('aria-expanded') === 'false') {
            this.openBurgerMenu();
        } else {
            this.closeBurgerMenu();
        }
    }

    openBurgerMenu() {
        this.header.classList.add('is-burger-open');
        this.burger.setAttribute('aria-expanded', 'true');
        this.burger.setAttribute('aria-label', this.burger.dataset.labelOpened);
        this.focusTrap.activate();
    }

    closeBurgerMenu() {
        this.header.classList.remove('is-burger-open');
        this.burger.setAttribute('aria-expanded', 'false');
        this.burger.setAttribute('aria-label', this.burger.dataset.labelClosed);
        this.focusTrap.deactivate();
    }

    // -----------------------------------------------------------------------------
    // Open or close the submenu based on its current state
    // -----------------------------------------------------------------------------
    static toggleDropdown(dropdown) {
        const toggle = Header.getDropdownToggle(dropdown);

        if (toggle.getAttribute('aria-expanded') === 'false') {
            Header.openDropdown(dropdown);
        } else {
            Header.closeDropdown(dropdown);
        }
    }

    // -----------------------------------------------------------------------------
    // Open a specific dropdown
    // -----------------------------------------------------------------------------
    static openDropdown(dropdown) {
        const toggle = Header.getDropdownToggle(dropdown);
        toggle.setAttribute('aria-expanded', 'true');

        // Close the other menus
        Header.closeSiblings(dropdown);
    }

    static closeSiblings(dropdown) {
        const siblingsEls = siblings(dropdown);
        const siblingDropdowns = siblingsEls.filter((sibling) => sibling.classList.contains('menu-item-has-children'));

        siblingDropdowns.forEach((sibling) => {
            Header.closeDropdown(sibling);
        });
    }

    // -----------------------------------------------------------------------------
    // Close a dropdown
    // -----------------------------------------------------------------------------
    static closeDropdown(dropdown) {
        const toggle = Header.getDropdownToggle(dropdown);
        toggle.setAttribute('aria-expanded', 'false');
    }

    static getDropdownToggle(dropdown) {
        return dropdown.querySelector('button');
    }
}
