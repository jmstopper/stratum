import Header from './scripts/header.js';

document.addEventListener('DOMContentLoaded', () => {
    const instances = document.querySelectorAll('.header');

    if (instances) {
        instances.forEach((instance) => {
            new Header(instance);
        });
    }
});
