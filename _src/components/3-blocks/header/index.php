<?php
/** @var mixed $args */
?><header <?= \Stratum\attributes($args); ?>>
    <?= \Stratum\Component::render('skip-link', [
        'target' => '#content',
        'title' => __('Skip to content', 'stratum-text-domain'),
    ]); ?>

    <?= \Stratum\Component::render('skip-link', [
        'target' => '#accessibility-controls-button',
        'title' => __('Skip to accessibility controls', 'stratum-text-domain'),
    ]); ?>

    <div class="header__outer block__outer">
        <?= \Stratum\Component::render('header/partials/logo'); ?>

        <?php if (has_nav_menu('header')) { ?>
            <nav class="header__navigation">
                <?= \Stratum\Component::render('header/partials/burger'); ?>

                <div class="header__navigation-outer" id="header-navigation">
                    <div class="header__navigation-inner">
                        <?= \Stratum\Component::render('header/partials/menu'); ?>
                    </div>
                </div>
            </nav>
        <?php } ?>

        <?= \Stratum\Component::render('accessibility-controls'); ?>
    </div>
</header>
