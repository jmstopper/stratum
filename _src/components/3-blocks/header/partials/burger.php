<?= \Stratum\Component::render('burger', [
    'content' => 'Main menu',
    'classes' => [
        'header__burger',
        'js-header-burger',
    ],
    'wrapper' => [
        'type' => 'button',
        'aria-controls' => 'header-navigation',
        'aria-expanded' => 'false',
        'aria-label' => __('Open main menu', 'stratum-text-domain'),
        'data-label-opened' => __('Close main menu', 'stratum-text-domain'),
        'data-label-closed' => __('Open main menu', 'stratum-text-domain'),
    ],
]);
