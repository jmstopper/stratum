<?php

$args = [
    'url' => get_home_url(),
    'wrapper' => [
        'title' => get_bloginfo('name'),
    ],
    'title' => \Stratum\svg('logo', [
        'width' => 180,
        'height' => 46,
    ]),
    'classes' => ['header__logo'],
];

if (is_front_page()) {
    $args['wrapper']['aria-current'] = 'page';
}

echo \Stratum\link($args);
