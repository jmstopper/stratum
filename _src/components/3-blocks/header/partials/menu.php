<?= wp_nav_menu([
    'theme_location' => 'header',
    'depth' => 0,
    'container' => '',
    'container_class' => '',
    'menu_class' => 'header__menu',
    'fallback_cb' => false,
    'echo' => false,
    'walker' => new \Stratum\Header\MenuWalker(),
]);
