<?php

namespace Stratum\Header;

// add_action('init', function(){
//     echo "<pre>";
//     var_dump(\Stratum\Component::directory('header'));
//     exit;
// }, 11);

// Add this code in if there is a fixed header for some pages
// add_filter('body_class', function ($classes) {
//     if (is_singular()) {
//         $blocks = parse_blocks(get_the_content());

//         // Is the wysiwyg check needed for all sites?
//         if (!empty($blocks[0]['attrs']['backgroundColor'])) {
//             $classes[] = 'is-fixed-header';
//         }
//     }

//     return $classes;
// });

// -----------------------------------------------------------------------------
// Add a div around the sub-menu so we can have full width backgrounds and
// containers for the sub-menu
// -----------------------------------------------------------------------------
class MenuWalker extends \Walker_Nav_Menu
{
    private $curItem;

    function start_lvl(&$output, $depth = 0, $args = null)
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div id='header-sub-menu-" . $this->curItem->object_id . "' class='sub-menu-outer'><div class='sub-menu-inner'><ul class='sub-menu'>\n";
    }

    function end_lvl(&$output, $depth = 0, $args = null)
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div></div>\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = null, $id = 0)
    {
        $this->curItem = $item;

        $output .= parent::start_el($output, $item, $depth, $args, $id);
    }
}

add_filter('wp_nav_menu_objects', function ($items, $args) {
    if ($args->theme_location === 'header') {
        $pageForPosts = get_option('page_for_posts');

        foreach ($items as &$item) {
            if (get_field('button', $item) && !in_array('menu-item-has-children', $item->classes)) {
                $item->classes[] = 'has-button';
            }

            if (get_field('content', $item)) {
                $item->classes[] = 'has-content';
            }

            if (($item->type === 'post_type_archive') && is_singular($item->object)) {
                $item->classes[] = 'current-menu-item';
            } elseif ($item->object_id == $pageForPosts && is_singular('post')) {
                $item->classes[] = 'current-menu-item';
            }
        }
    }

    // return
    return $items;
}, 10, 2);

add_filter('walker_nav_menu_start_el', function ($output, $item, $depth, $args) {
    if ($args->theme_location === 'header') {
        if (in_array('menu-item-has-children', $item->classes)) {
            $output = '<button class="link" aria-controls="header-sub-menu-' . $item->object_id . '" aria-expanded="false">' . $item->title . '</button>';
        } elseif (get_field('button', $item)) {
            $processor = new \WP_HTML_Tag_Processor($output);
            $processor->next_tag('a');
            $processor->add_class('btn');
            $output = $processor->get_updated_html();
        }

        if ($content = get_field('content', $item)) {
            $output = '<div class="menu-item-content nflm">' . $content . '</div>' . $output;
        }
    }

    return $output;
}, 10, 4);
