<?php

add_filter('stratum/render/assets/components/header', function ($args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'wrapper' => [
            'role' => 'banner'
        ]
    ]);

    $args['classes'] = array_merge($args['classes'], [
        'header', 'alignfull', 'block'
    ]);

    // if (is_singular()) {
    //     $blocks = parse_blocks(get_the_content());

    //     if (!empty($blocks[0]) && $blocks['0']['blockName'] === 'acf/wysiwyg' && str_contains($blocks[0]['attrs']['wpClassName'], 'has-background')) {
    //         $args['classes'][] = 'is-absolute';
    //     }
    // }

    return $args;
});
