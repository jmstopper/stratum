<?php

add_filter('stratum/render/assets/components/footer', function (?array $args): array {

    $args = wp_parse_args($args, [
        'heading' => get_field('footer_heading', 'option'),
        'content' => get_field('footer_content', 'option'),
        'summary' => get_field('footer_summary', 'option'),
        'details' => get_field('footer_details', 'option'),
        'classes' => [],
        'hidden' => get_field('hide_footer'),
        'wrapper' => [
            'role' => 'contentinfo'
        ]
    ]);

    $args['classes'] = array_merge($args['classes'], [
        'footer', 'block', 'has-black-background-color', 'alignfull'
    ]);

    if ($args['hidden']) {
        $args['classes'][] = 'is-footer-hidden';
    }

    return $args;
});
