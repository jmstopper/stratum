<?php
/** @var mixed $args */
?><footer <?= \Stratum\attributes($args); ?>>
    <div class="footer__outer block__outer">
        <?= \Stratum\link([
            'url' => get_home_url(),
            'wrapper' => [
                'title' => get_bloginfo('name'),
            ],
            'title' => \Stratum\svg('logo.svg', [
                'width' => '120',
                'height' => 31
            ]),
            'classes' => ['footer__logo'],
        ]); ?>

        <?php if (has_nav_menu('footer')) { ?>
            <nav aria-label="<?= esc_attr__('Footer navigation', 'stratum-text-domain'); ?>" class="footer__navigation">
                <?php wp_nav_menu([
                    'theme_location' => 'footer',
                    'depth' => 0,
                    'container' => '',
                    'container_class' => '',
                    'menu_class' => 'footer__menu nflm',
                    'fallback_cb' => false,
                ]); ?>
            </nav>
        <?php } ?>

        <?= \Stratum\Component::render('social-networks'); ?>

        <?php if (!empty($args['address'])) { ?>
            <div class="footer__address nflm">
                <?= $args['address']; ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['copyright'])) { ?>
            <div class="footer__copyright nflm">
                <?= $args['copyright']; ?>
            </div>
        <?php } ?>
    </div>
</footer>
