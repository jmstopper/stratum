<?php

add_filter('stratum/render/assets/components/card-grid', function (array $args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'items' => [],
        'tag' => '',
        'heading' => '',
        'content' => '<InnerBlocks />',
        'scroll' => false,
    ]);

    $args['classes'] = array_merge($args['classes'], [
        'card-grid', 'block'
    ]);

    if ($args['scroll'] === true) {
        $args['classes'][] = 'is-scroll';
    }

    return $args;
});
