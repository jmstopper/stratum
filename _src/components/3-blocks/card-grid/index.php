<?php

/** @var mixed $args */

?><div <?= \Stratum\attributes($args); ?>>
    <div class="card-grid__outer block__outer">

        <?php if (!empty($args['heading']) || !empty($args['content'])) { ?>
            <div class="block__header card-grid__header has-text-align-center nflm">
                <?= \Stratum\Component::render('tag-and-heading', $args, [
                    '+headingClasses' => ['h2'],
                    'headingOnlyClasses' => ['flag__heading', 'h2'],
                ]); ?>

                <?php if (!empty($args['content'])) { ?>
                    <div class="card-grid__content nflm">
                        <?= wp_kses_post($args['content']); ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['items'])) { ?>
            <div class="card-grid__grid">
                <?php foreach ($args['items'] as $key => $item) {
                    $options = [];
                    if (!empty($args['_index']) && $args['_index'] >= 2) {
                        $options['_loading'] = false;
                    }

                    if (!empty($args['heading'])) {
                        $options['headingEl'] = $args['secondaryHeadingEl'];
                    } elseif (is_post_type_archive('post') || is_home()) {
                        $options['headingEl'] = 'h2';
                    }

                    echo \Stratum\Component::render('card', $item, $options);
                } ?>
            </div>
        <?php } ?>
    </div>
</div>
