<?php

add_filter('login_headerurl', function (string $url): string {
    return get_bloginfo('url');
});

add_filter('login_headertext', function (string $text): string {
    return get_bloginfo('name');
});

add_action('login_enqueue_scripts', function (): void {
    wp_dequeue_style('login');

    wp_enqueue_style(
        'stratum-style-login',
        \Stratum\Asset::url('components/wp-login/login.css'),
        ['dashicons'],
        null
    );
});

// add_filter('login_site_html_link', function(string $link) {
//     return '';
// });
