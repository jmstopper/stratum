<?php

add_filter('stratum/render/output/assets/components/pagination/archive', function ($content) {

    $processor = new \WP_HTML_Tag_Processor($content);
    $processor->next_tag('ul');
    $processor->remove_class('page-numbers');
    $processor->add_class('navigation__list');

    return $processor->get_updated_html();
});

// https://developer.wordpress.org/reference/functions/_navigation_markup/
add_filter('navigation_markup_template', function () {
    return '<nav class="navigation %1$s block alignnormal nflm" aria-label="%4$s">%3$s</nav>';
});
