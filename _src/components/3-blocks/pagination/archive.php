<?php

// https://developer.wordpress.org/reference/functions/the_posts_pagination/
// https://developer.wordpress.org/reference/functions/get_the_posts_pagination/
echo get_the_posts_pagination([
    'type' => 'list',
    'class' => '',
    'aria_label' => 'Pagination',
    'prev_text' => '<span class="screen-reader-text"> ' . __('Previous page', 'stratum-text-domain') . '</span>',
    'next_text' => '<span class="screen-reader-text"> ' . __('Next page', 'stratum-text-domain') . '</span>',
    'before_page_number' => '<span class="screen-reader-text">' . __('Page', 'stratum-text-domain') . ' </span>',
]);
