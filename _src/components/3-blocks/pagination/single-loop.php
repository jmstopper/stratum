<?php

$postType = get_post_type_object(get_post_type());

$posts = get_posts([
    'posts_per_page' => -1,
    'order' => 'ASC',
    'orderby' => 'menu_order',
    'post_type' => get_post_type(),
    'fields' => 'ids',
]);

$currentPostID = get_the_id();
$prevPost = false;
$nextPost = false;

foreach ($posts as $key => $id) {
    if ($currentPostID === $id) {
        $nextPost = !empty($posts[$key + 1]) ? $posts[$key + 1] : $posts[0];
        $prevPost = !empty($posts[$key - 1]) ? $posts[$key - 1] : $posts[array_key_last($posts)];
        break;
    }
}

?><nav class="navigation post-navigation" role="navigation" aria-label="<?= $postType->label; ?>">
    <h2 class="screen-reader-text"><?= sprintf(__('%s navigation', 'stratum-text-domain'), $postType->labels->singular_name); ?></h2>
    <div class="nav-links">
        <?php if ($prevPost) { ?>
            <div class="nav-previous">
                <a href="<?= get_the_permalink($prevPost); ?>" rel="<?= __('prev', 'stratum-text-domain') ?>">
                    <?= sprintf(__('Previous %s', 'stratum-text-domain'), $postType->labels->singular_name); ?>
                </a>
            </div>
        <?php } ?>
        <?php if ($nextPost) { ?>
            <div class="nav-next">
                <a href="<?= get_the_permalink($nextPost); ?>" rel="<?= __('next', 'stratum-text-domain') ?>">
                    <?= sprintf(__('Next %s', 'stratum-text-domain'), $postType->labels->singular_name); ?>
                </a>
            </div>
        <?php } ?>
    </div>
</nav>
