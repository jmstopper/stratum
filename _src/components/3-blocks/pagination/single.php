<?php

$postType = get_post_type_object(get_post_type());

the_post_navigation([
    'prev_text' => __('Previous ' . $postType->labels->singular_name, 'stratum-text-domain'),
    'next_text' => __('Next ' . $postType->labels->singular_name, 'stratum-text-domain'),
]);
