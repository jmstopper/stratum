<?php

add_filter('stratum/render/assets/components/query-loop', function ($args, $options, $adjunct): array {

    $args = wp_parse_args($args, [
        'component_to_render' => 'card-grid',
        'args' => [
            'query' => stratum()->query(),
            'items' => stratum()->query()->posts,
        ],
    ]);

    return $args;
}, 10, 3);
