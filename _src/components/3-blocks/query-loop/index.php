<?php

/** @var mixed $args */

echo \Stratum\Component::render(
    'main',
    \Stratum\Component::render($args['component_to_render'], $args['args'])
);

if ($args['component_to_render'] !== 'search-and-filter') {
    echo \Stratum\Component::render('pagination/archive');
}
