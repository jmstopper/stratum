<?php

namespace Stratum\QueryLoop;

add_filter('acf/load_field/name=component_to_render', function ($field) {
    $field['choices'] = [];
    $components = \Stratum\Components::supportPosts();

    foreach ($components as $component) {
        $field['choices'][$component] = \Stratum\Component::name($component);
    }

    return $field;
});
