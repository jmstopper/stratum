<?php

add_filter('stratum/render/assets/components/terms', function ($args): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'links' => [],
        'id' => 'filters'
    ]);

    $queriedObject = get_queried_object();

    if (empty($args['post_type'])) {
        if ($queriedObject instanceof WP_Post_Type) {
            $args['post_type'] = $queriedObject->name;
        } elseif (get_option('page_for_posts') == $queriedObject->ID) {
            $args['post_type'] = 'post';
        }
    }

    if (empty($args['taxonomy']) && $queriedObject instanceof WP_Term) {
        $args['taxonomy'] = $queriedObject->taxonomy;

        $taxonomy = get_taxonomy($args['taxonomy']);

        if (!empty($taxonomy->object_type[0])) {
            $args['post_type'] = $taxonomy->object_type[0];
        }
    }

    // if (empty($args['post_type']) && !empty($args['taxonomy'])) {
    //     if ($args['taxonomy'] === 'category') {
    //         $args['post_type'] = 'post';
    //     }
    // }

    if (empty($args['taxonomy']) && !empty($args['post_type'])) {
        $taxonomies = get_object_taxonomies($args['post_type']);

        if (!empty($taxonomies[0])) {
            $args['taxonomy'] = $taxonomies[0];
        }
    }

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge($args['classes'], [
        'terms', 'block',
    ]);

    $args['links'] = get_terms([
        'taxonomy' => $args['taxonomy'],
    ]);

    if (!is_array($args['links'])) {
        $args['links'] = [];
    }

    if (!empty($args['links'])) {
        $args['links'] = array_map(function ($item) use ($queriedObject) {
            $classes = [];

            if (!empty($queriedObject->term_id) && $queriedObject->term_id === $item->term_id) {
                $classes[] = 'is-active';
            }

            return [
                'url' => get_term_link($item) . '#filters',
                'title' => $item->name,
                'classes' => $classes,
            ];
        }, $args['links']);

        if (!empty($args['post_type'])) {
            $allClasses = [];

            if (
                empty(array_filter($args['links'], function ($item) {
                    return in_array('is-active', $item['classes']);
                }))
            ) {
                $allClasses[] = 'is-active';
            }

            $args['links'] = array_merge([[
                'url' => get_post_type_archive_link($args['post_type']) . '#filters',
                'title' => __('All', 'stratum-text-domain'),
                'classes' => $allClasses,
            ]], $args['links']);
        }
    }

    return $args;
});
