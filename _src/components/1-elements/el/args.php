<?php

add_filter('stratum/render/assets/components/el', function ($args): ?array {
    if (empty($args['el'])) {
        return null;
    }

    $args = wp_parse_args($args, [
        'classes' => [],
        'content' => '',
        'void' => \Stratum\El\voidElement($args['el']),
    ]);

    return $args;
});
