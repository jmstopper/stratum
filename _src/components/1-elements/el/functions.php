<?php

namespace Stratum\El;

function voidElement(string $el): bool
{
    $voidElements = [
        'area', 'base', 'br', 'col','embed', 'hr', 'img', 'input', 'link',
        'meta', 'param', 'source', 'track', 'wbr'
    ];

    return in_array($el, $voidElements);
}
