<?php
/** @var mixed $args */

// Alias the element as its easier to read
$el = $args['el'];

// Output the opening tag
?><<?= esc_attr($el); ?> <?= \Stratum\attributes($args); ?>><?php

if (!$args['void']) {
    // Output the content if there is any
    echo $args['content'];

    // Output the closing tag
    ?></<?= esc_attr($el); ?>><?php
}
