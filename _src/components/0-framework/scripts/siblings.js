export default function siblings(e) {
    // for collecting siblings
    const siblingsEl = [];
    // if no parent, return no sibling
    if (!e.parentNode) {
        return siblingsEl;
    }
    // first child of the parent node
    let sibling = e.parentNode.firstChild;

    // collecting siblings
    while (sibling) {
        if (sibling.nodeType === 1 && sibling !== e) {
            siblingsEl.push(sibling);
        }
        sibling = sibling.nextSibling;
    }
    return siblingsEl;
}
