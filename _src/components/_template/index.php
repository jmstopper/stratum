<?php
/** @var mixed $args */
?><div <?= \Stratum\attributes($args); ?>>
    <div class="{{template-slug}}__outer block__outer">

        <?php if (!empty($args['heading']) || !empty($args['content'])) { ?>
            <div class="{{template-slug}}__header nflm">
                <?= \Stratum\Component::render('tag-and-heading', $args, [
                    'headingClasses' => ['h2'],
                    'headingOnlyClasses' => ['{{template-slug}}__heading', 'h2'],
                ]); ?>

                <?php if (!empty($args['content'])) { ?>
                    <div class="{{template-slug}}__content nflm">
                        <?= wp_kses_post($args['content']); ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['items'])) { ?>
            <div class="{{template-slug}}__items">
                <?php foreach ($args['items'] as $key => $item) {
                    $item['headingEl'] = !empty($args['secondaryHeadingEl']) ? $args['secondaryHeadingEl'] : 'div';

                    ?><div class="{{template-slug}}__item nflm">
                        <?php if (!empty($item['heading'])) { ?>
                            <<?= esc_attr($item['headingEl']); ?> class="{{template-slug}}__item-heading h3">
                                <?= wp_kses_post($item['heading']); ?>
                            </<?= esc_attr($item['headingEl']); ?>>
                        <?php } ?>

                        <?php if (!empty($item['content'])) { ?>
                            <div class="{{template-slug}}__item-content nflm">
                                <?= wp_kses_post($item['content']); ?>
                            </div>
                        <?php } ?>

                        <?php if (!empty($item['media'])) { ?>
                            <div class="{{template-slug}}__item-media objfit">
                                <?= wp_kses_post($item['media']); ?>
                            </div>
                        <?php } ?>
                    </div><?php
                } ?>
            </div>
        <?php } ?>

        <?php if (!empty($args['media'])) { ?>
            <div class="{{template-slug}}__media objfit">
                <?= wp_kses_post($args['media']); ?>
            </div>
        <?php } ?>
    </div>
</div>
