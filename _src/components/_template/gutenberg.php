<?php

add_action('init', function () {

    // -------------------------------------------------------------------------
    // Pull the dependencies of the gutenberg block
    // -------------------------------------------------------------------------
    $gutenberg = require \Stratum\Asset::path('{{template-slug}}/gutenberg.asset.php');

    // -------------------------------------------------------------------------
    // Register the admin area script that registers the Gutenberg block
    // -------------------------------------------------------------------------
    wp_register_script(
        '{{template-slug}}-editor',
        \Stratum\Asset::url('components/{{template-slug}}/gutenberg.js'),
        $gutenberg['dependencies'],
        $gutenberg['version'],
    );

    // -------------------------------------------------------------------------
    // Register the Gutenberg block
    // -------------------------------------------------------------------------
    register_block_type('{{template-slug}}', [
        'apiVersion' => 2,
        'editor_script' => '{{template-slug}}-editor',
        // 'editor_style' => '{{template-slug}}-editor',
        // javascript is generally loaded with the main theme or admin js
        // 'script' => '{{template-slug}}',
        // styles are generally loaded with the main theme or admin css
        // 'style' => '{{template-slug}}',
    ]);
});
