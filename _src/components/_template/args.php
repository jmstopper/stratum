<?php

add_filter('stratum/render/assets/components/{{template-slug}}', function ($args, $options, $adjunct): array {

    $args = wp_parse_args($args, [
        'classes' => [],
        'heading' => '',
        'content' => '<InnerBlocks />',
        'items' => [],
    ]);

    // -------------------------------------------------------------------------
    // Add any required classes
    // -------------------------------------------------------------------------
    $args['classes'] = array_merge([
        '{{template-slug}}', 'block',
    ], $args['classes']);

    // -------------------------------------------------------------------------
    // If we need to loop items
    // -------------------------------------------------------------------------
    $args['items'] = array_map(function ($item) {
        if ($item instanceof WP_Post) {

            $myPost = \Stratum\Page\Post::seed($item);
            // Now access stuff through the \Stratum\Page\Post interface
        }

        return $item;
    }, $args['items']);

    // -------------------------------------------------------------------------
    // An image from wordpress
    // -------------------------------------------------------------------------
    if (!empty($args['image']['id'])) {
        $args['media'] = wp_get_attachment_image($args['image']['id'], 'large');
        unset($args['image']);
    }

    // -------------------------------------------------------------------------
    // If we need to add extra classes
    // -------------------------------------------------------------------------
    // if(!empty($args['xxx'])){
    //     $args['classes'][] = '{{template-slug}}--' . $args['xxx'];
    // }

    return $args;
}, 10, 3);
