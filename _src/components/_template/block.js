import {{template-instance}} from './scripts/instance.js'

document.addEventListener("DOMContentLoaded", () => {
    const instances = document.querySelectorAll('.{{template-slug}}');

    if (instances) {
        instances.forEach((instance) => {
            new {{template-instance}(instance);
        });
    }
});
