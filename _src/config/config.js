import { readFileSync } from 'fs';
import plugins from './plugins.js';
import paths from './paths.js';

const stratum = JSON.parse(readFileSync('./stratum.json'));

export default {
    paths,
    plugins,
    stratum,
};
