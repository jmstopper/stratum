<?php

function applyConfiguration()
{
    $configuration = json_decode(file_get_contents('_src/config/config.json'), true);

    updateWordPressJSON($configuration);
    updateDotEnv($configuration);
    updateSiteManifest($configuration);
    updateTextDomain($configuration);
}

// -----------------------------------------------------------------------------
// Write the config file
// -----------------------------------------------------------------------------
function writeConfiguration($configuration)
{
    output("Starting: writeConfiguration");

    file_put_contents('_src/config/config.json', json_encode($configuration, JSON_PRETTY_PRINT));

    output("Complete: writeConfiguration");
}

// -----------------------------------------------------------------------------
// Function that change files on disc
// -----------------------------------------------------------------------------
function updateWordPressJSON($configuration)
{
    output("Starting: updateWordPressJSON");

    $content = file_get_contents('_src/config/templates/wordpress.json');

    $content = str_replace('{{theme-name}}', $configuration['themeName'], $content);
    $content = str_replace('{{theme-url}}', $configuration['themeURL'], $content);
    $content = str_replace('{{theme-description}}', $configuration['themeDescription'], $content);
    $content = str_replace('{{theme-author}}', $configuration['author'], $content);
    $content = str_replace('{{theme-author-url}}', $configuration['authorURL'], $content);
    $content = str_replace('{{theme-domain}}', $configuration['textDomain'], $content);
    $content = str_replace('{{theme-tags}}', $configuration['themeTags'], $content);

    file_put_contents('_src/config/wordpress.json', $content);

    output("Complete: updateWordPressJSON");
}

function updateDotEnv($configuration)
{
    output("Starting: updateDotEnv");

    $content = file_get_contents('_src/config/templates/.env.js');

    $content = str_replace('{{BROWSERSYNC_PROXY}}', $configuration['localURL'], $content);

    file_put_contents('.env.js', $content);

    output("Complete: updateDotEnv");
}

function updateSiteManifest($configuration)
{
    output("Starting: updateSiteManifest");

    $content = file_get_contents('_src/config/templates/site.webmanifest');

    $content = str_replace('{{name}}', $configuration['themeName'], $content);
    $content = str_replace('{{short-name}}', $configuration['shortName'], $content);
    $content = str_replace('{{primary-color}}', $configuration['primaryColor'], $content);

    file_put_contents('_src/general/site.webmanifest', $content);

    output("Complete: updateSiteManifest");
}

function updateTextDomain($configuration)
{
    output("Starting: updateTextDomain");

    $files = getPHPFiles();

    foreach ($files as $key => $file) {
        if (str_contains($file, './_src/config') !== true) {
            file_put_contents($file, str_replace(
                $configuration['previousTextDomain'],
                $configuration['textDomain'],
                file_get_contents($file)
            ));
        }
    }

    // Update the configuration file with the new text domain information
    $configuration['previousTextDomain'] = $configuration['textDomain'];
    writeConfiguration($configuration);

    output("Complete: updateTextDomain");
}


// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// Helpers from here on down
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
function continueCheck()
{
    return request("Continue?", FILTER_VALIDATE_BOOLEAN);
}


function section($heading, $content = '')
{
    clear();
    lines();
    output($heading);
    lines();

    if (!empty($content)) {
        output($content);
        newline();
    }
}

function lines()
{
    output("------------------------------------------------------------------------------------");
}

function output($string)
{
    echo $string;
    newline();
}

function clear()
{
    system('clear');
}

function newline()
{
    echo "\n";
}

function request(string $label, $format = '', $required = true)
{

    $value = '';

    if ($required === true) {
        while ($value === '') {
            $value = validate(trim(readline($label . ": ")), $format);
        }
    } else {
        $value = validate(trim(readline($label . ": ")), $format);
    }

    return $value;
}

function validate($value, $format = '')
{

    // -------------------------------------------------------------------------
    // Handle a boolean edge case
    // -------------------------------------------------------------------------
    if ($format === FILTER_VALIDATE_BOOLEAN) {
        // early exit for empty strings because filter_var treats an empty
        // string as false. We dont want this behavior as it can lead to mistakes
        if ($value === '') {
            return '';
        }

        // FILTER_NULL_ON_FAILURE means we will only look for valid falsey values
        if ($truthy  = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) !== null) {
            // So once we know its truthy, we need to use the filter_var return
            // value as it will be a true or a false. boolval will convert non
            // truthy values to true.
            return filter_var($value, FILTER_VALIDATE_BOOLEAN);
        }

        return '';
    } elseif ($format === FILTER_VALIDATE_URL) {
        if (filter_var($value, FILTER_VALIDATE_URL) === false) {
            return '';
        }
    }

    // -------------------------------------------------------------------------
    // Return to the prompt
    // -------------------------------------------------------------------------
    return $value;
}

function quit()
{
    newline();
    lines();
    output("Nothing was changed");
    lines();
    newline();
    exit;
}

function getPHPFiles()
{
    $files = [];

    $it = new RecursiveDirectoryIterator('./');

    foreach (new RecursiveIteratorIterator($it) as $file) {
        if (pathinfo($file, PATHINFO_EXTENSION) === 'php') {
            $pathName = $file->getPathname();

            if (!str_ends_with($pathName, '_src/config/configure.php') && !str_starts_with($pathName, './vendor') && !str_starts_with($pathName, './node_modules')) {
                $files[] = $pathName;
            }
        }
    }

    return $files;
}

function yesNo(bool $value)
{
    return $value === true ? 'Yes' : 'No';
}
