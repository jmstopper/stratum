<?php

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// Stratum configuration script
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Check if we are in CLI mode
// -----------------------------------------------------------------------------
if (php_sapi_name() !== 'cli') {
    exit;
}

// -----------------------------------------------------------------------------
// helpers
// -----------------------------------------------------------------------------
require 'helpers/helpers.php';

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// If we have made it this far, lets get started.
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
$configuration = [
    'previousTextDomain' => 'stratum-text-domain',
];

// -----------------------------------------------------------------------------
// Show introduction
// -----------------------------------------------------------------------------
section(
    "Stratum setup",
    "This process will take you through configuring Stratum with all the defaults for your new theme"
);

// Do we want to continue?
if (continueCheck() !== true) {
    quit();
}

// -----------------------------------------------------------------------------
// style.css requirements
// -----------------------------------------------------------------------------
section("Theme configuration");

$configuration['themeName'] = request('Theme name');
$configuration['shortName'] = request('Theme short name');
$configuration['themeURL'] = request('Theme URL', FILTER_VALIDATE_URL);
$configuration['themeDescription'] = request("Theme description");
$configuration['themeTags'] = request("Theme tags");
$configuration['textDomain'] = request('Text domain') . '-td';
$configuration['author'] = request('Author');
$configuration['authorURL'] = request('Author URL', FILTER_VALIDATE_URL);
$configuration['primaryColor'] = request('Theme primary color');

newline();
// -----------------------------------------------------------------------------
// .env requirements
// -----------------------------------------------------------------------------
section(
    ".env Configuration",
    "The information provided here will be used to generate .env"
);

$configuration['localURL'] = request('Local development URL', FILTER_VALIDATE_URL);

// -----------------------------------------------------------------------------
// Confirm the information provided to us
// -----------------------------------------------------------------------------
section("Confirm");

output("Theme Name: " . $configuration['themeName']);
output("Theme URI: " . $configuration['themeURL']);
output("Description: " . $configuration['themeDescription']);
output("Author: " . $configuration['author']);
output("Author URI: " . $configuration['authorURL']);
output("Text Domain: " . $configuration['textDomain']);
output("Tags: " . $configuration['themeTags']);

newline();
output("Local URL: " . $configuration['localURL']);

newline();
lines();

// Do we want to continue?
continueCheck();

// -----------------------------------------------------------------------------
// Begin the changes to files on disc
// -----------------------------------------------------------------------------
section('Building');

writeConfiguration($configuration);
applyConfiguration();

lines();
newline();
output('Done');
