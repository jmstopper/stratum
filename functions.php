<?php

// Composer
require __DIR__ . '/vendor/autoload.php';

// Start it all up
\Stratum\Stratum::init();
