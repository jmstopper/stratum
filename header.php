<!doctype html>
<html <?= \Stratum\htmlAttributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <?= \Stratum\Component::render('wp-head'); ?>
    <meta name="format-detection" content="telephone=no">
    <link rel="manifest" href="<?= \Stratum\Asset::url('general/site.webmanifest'); ?>">
</head>

<body <?= \Stratum\bodyAttributes(); ?>>
    <?php wp_body_open(); ?>
    <div class="site-wrapper nflm">
        <?= \Stratum\Component::render('header'); ?>
        <?= \Stratum\Component::render('breadcrumbs'); ?>
        <div class="site-wrapper__outer nflm">

            <?= \Stratum\Template::render([
                'suffix' => 'before'
            ]);
